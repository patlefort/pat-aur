use str
use path

use ./utils u

fn parse-source {|source|
	fn parse {
		var filename

		{
			var index = (str:index $source '::')
			if (>= $index 0) {
				set filename = $source[0..$index]
				set source = $source[(+ $index 2)..]
			}
		}

		{
			var url-sign-pos = (str:index $source '://')
			if (>= $url-sign-pos 0) {
				var type
				var protocol = $source[0..$url-sign-pos]

				{
					var index = (str:index $protocol '+')
					if (>= $index 0) {
						set type = $protocol[0..$index]
						set protocol = $protocol[(+ $index 1)..]
						set source = $source[(+ $index 1)..]
					} else {
						set type = 'url'
					}
				}

				put [
					&type= $type
					&protocol= $protocol
					&url= $source
					&filename= $filename
				]

				return
			}
		}

		{
			var lp-sign-pos = (str:index $source 'lp:')
			if (>= $lp-sign-pos 0) {
				var type
				var protocol = $source[0..$lp-sign-pos]

				{
					var index = (str:index $protocol '+')
					if (>= $index 0) {
						set type = $protocol[0..$index]
						set protocol = $protocol[(+ $index 1)..]
						set source = $source[(+ $index 1)..]
					} else {
						set type = 'lp'
					}
				}

				put [
					&type= $type
					&protocol= $protocol
					&url= $source
					&filename= $filename
				]

				return
			}
		}

		put [
			&type= 'local'
			&protocol= 'file'
			&url= $source
			&filename= $filename
		]
	}

	var parts = (parse)

	if (eq $parts['filename'] $nil) {
		if (==s $parts['type'] 'local') {
			set parts['filename'] = (path:base $parts['url'])
		} elif (has-value ['bzr' 'fossil' 'git' 'hg' 'svn'] $parts['type']) {
			var filename = $parts['url']
			set filename = (u:trim-right-of-last $filename '#')
			set filename = (u:trim-right-of-last $filename '?')
			set filename = (str:trim-suffix $filename '/')
			set filename = (u:trim-left-of-last $filename '/')

			if (==s $parts['type'] 'bzr') {
				set filename = (str:trim-prefix $filename 'lp:')
			} elif (==s $parts['type'] 'fossil') {
				set filename = $filename'.fossil'
			} elif (==s $parts['type'] 'git') {
				set filename = (str:trim-suffix $filename '.git')
			}

			set parts['filename'] = $filename
		} else {
			set parts['filename'] = (u:trim-left-of-last $parts['url'] '/')
		}
	}

	put $parts
}

var valid-types = ['bzr' 'fossil' 'git' 'hg' 'local' 'lp' 'svn' 'url']
var download-tools = ['bzr' 'fossil' 'git' 'hg' 'lp' 'svn' 'url']

var default-dl-agents = [
	&file= [/usr/bin/curl -qg -R [e -z [f]] -C - -s -S -o [o] [i]]
	&ftp= [/usr/bin/curl -qg -R [e -z [f]] -L -C - --ftp-pasv --retry 3 --retry-delay 3 --write-out '%{exit_code}\n' -s -S -f -o [o] [i]]
	&http= [/usr/bin/curl -qgb '' -R [e -z [f]] -L -C - --retry 3 --retry-delay 3 --write-out '%{http_code}\n' --etag-compare [f '.~etag'] --etag-save [f '.~etag'] -s -S -f -o [o] [i]]
	&https= [/usr/bin/curl -qgb '' -R [e -z [f]] -L -C - --retry 3 --retry-delay 3 --write-out '%{http_code}\n' --etag-compare [f '.~etag'] --etag-save [f '.~etag'] -s -S -f -o [o] [i]]
	&rsync= [/usr/bin/rsync --no-motd -z [i] [f]]
	&scp= [/usr/bin/scp -C [i] [o]]
]

fn ctor-downloader {|dl-agents tools-path &maybe-skip= {|_| put $false }|

	fn get-dl-agent {|protocol dest filename url &exists=$false|
		fn substitute-list {|list str|
			{
				put $str
				drop 1 $list
			} | str:join ''
		}

		fn parse-args {
			each {|a|
				if (==s (kind-of $a) 'list') {
					if (==s $a[0] 'o') {
						substitute-list $a $dest/$filename'.~part'
					} elif (==s $a[0] 'f') {
						substitute-list $a $dest/$filename
					} elif (==s $a[0] 'i') {
						substitute-list $a $url
					} elif (==s $a[0] 'e') {
						if $exists {
							drop 1 $a | parse-args
						}
					} else {
						fail 'Invalid download agent format, input and output file should be specified with [o] and [i].'
					}
				} else {
					put $a
				}
			}
		}

		all (u:default-conf-value $dl-agents $protocol $default-dl-agents[$protocol]) | parse-args
	}

	put [
		&download-cmd-parsed-url= {|_ parts destination &exists=$false &can-skip=$false|
			if (not (has-value $valid-types $parts['type'])) {
				fail 'Invalid source url type `'$parts['type']'`'
			}

			if ($maybe-skip $parts) {
				#u:echo-info 'Skipped '$url
				return
			}

			if (!=s $parts['type'] 'local') {
				if (==s $parts['type'] 'url') {
					if (!=s $parts['protocol'] 'local') {
						put $tools-path/'download.url' ^
							(u:select $can-skip '1' '0') ^
							$destination/$parts['filename'] ^
							(get-dl-agent &exists=$exists $parts['protocol'] $destination $parts['filename'] $parts['url'])
					}
				} else {
					put $tools-path/'download.'$parts['type'] $destination/$parts['filename'] $parts['url'] (u:select $can-skip '1' '0')
				}
			}
		}
		&download-cmd= {|this url destination &exists=$false &can-skip=$false|
			$this[download-cmd-parsed-url] (parse-source $url) $destination &can-skip=$can-skip &exists=$exists
		}
	]
}
