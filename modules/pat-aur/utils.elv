use str
use path
use os
use re
use flag
use math
use runtime

fn capture-return {|fc @args|
	$fc $@args
}

fn ignore-errors {|fc @args|
	try { $fc $@args } catch e { }
}

fn discard {
	all | { }
}

fn silence {|fc|
	$fc 1>$os:dev-null 2>$os:dev-null
}

fn silence-output {|fc|
	$fc 1>$os:dev-null
}

fn silence-errors {|fc|
	$fc 2>$os:dev-null
}

fn empty {|cont|
	== (count $cont) 0
}

fn not-empty {|cont|
	> (count $cont) 0
}

fn identity {|x|
	put $x
}

fn log {|fc|
	$fc 1>&3 2>&4
}

fn log-errors {|fc|
	$fc 2>&4
}

fn log-redirect-to-error {|fc|
	$fc 3>&4
}

fn log-context {|fc &silent=$false|
	if $silent {
		capture-return $fc >$os:dev-null 3>$os:dev-null 4>&2
	} else {
		capture-return $fc 3>&1 4>&2
	}
}

fn env-or-default {|env-var default|
	if (has-env $env-var) {
		get-env $env-var
	} else {
		put $default
	}
}

fn string-is-true {|str|
	has-value ['1' 'yes' 'on' 'true'] $str
}

fn string-is-false {|str|
	has-value ['0' 'no' 'off' 'false'] $str
}

fn has-env-and-value {|env-var value|
	and (has-env $env-var) (==s (get-env $env-var) $value)
}

fn env-is-true {|env-var|
	and (has-env $env-var) (string-is-true (get-env $env-var))
}

fn env-is-false {|env-var|
	or (not (has-env $env-var)) (string-is-false (get-env $env-var))
}

fn env-is-not-empty {|env-var|
	and (has-env $env-var) (not-empty (get-env $env-var))
}

fn detect-tty-style-support {
	var supported = $false

	ignore-errors {
		set supported = (not-empty (silence-errors { e:tput bold } | slurp))
	}

	put $supported
}

var use-styles = (detect-tty-style-support)
var use-colors

fn set-use-colors {|val|
	set use-colors = $val

	if $val {
		unset-env 'NO_COLOR'
	} else {
		set E:NO_COLOR = '1'
	}
}

set-use-colors (and $use-styles (not (env-is-not-empty 'NO_COLOR')))

fn maybe-styled {|str @args|
	if $use-styles {
		styled $str $@args
	} else {
		print $str
	}
}

fn execute {|cmd @args|
	set cmd = (external $cmd)

	$cmd $@args
}

fn space-pad-str {|str length|
	printf '%-'$length's' $str
}

fn slice {|size fc|
	var s = [(take $size)]
	while (not-empty $s) {
		$fc $s
		set s = [(take $size)]
	}
}

fn all-of {|@args|
	for a $args {
		if (==s (kind-of $a) 'list') {
			all $a
		} else {
			put $a
		}
	}
}

fn find-continuous {|@vals &op={|_ _| put $false }|
	var res = $vals[0]

	for v $vals[1..] {
		if ($op $v $res) {
			set res = $v
		}
	}

	put $res
}

fn find-stop {|@vals &op={|_| put $false }|
	for v $vals[1..] {
		if ($op $v) {
			put $v
			break
		}
	}
}

fn put-keys {|map @keys|
	for k $keys {
		put $map[$k]
	}
}

fn select-keys {|map @keys|
	for k $keys {
		put [$k $map[$k]]
	} | make-map
}

fn column {|list key|
	for e $list {
		put $e[$key]
	}
}

fn make-set {
	each {|v|
		put [$v $nil]
	} | make-map
}

fn concat-sets {|@sets|
	for s $sets {
		keys $s | each {|v| put [$v $nil] }
	} | make-map
}

fn append-set {|s @values|
	for v $values {
		set s[$v] = $nil
	}
	put $s
}

fn uniq-key {|key|
	var mapped = [&]

	each {|entry|
		var kv = ($key $entry)
		if (not (has-key $mapped $kv)) {
			put $entry
			set mapped[$kv] = $nil
		}
	}
}

fn substr {|str &size=$nil &start=0|
	if (eq $size $nil) {
		set size = (count $str)
	}

	if (>= $start (count $str)) {
		put ''
	} else {
		put $str[$start..(+ $start (math:min $size (- (count $str) $start)))]
	}
}

fn str-slice {|str size fc|
	var i = 0
	while (< $i (count $str)) {
		$fc (substr $str &start=$i &size=$size)
		set i = (+ $i $size)
	}
}

fn print-header { maybe-styled ' >>' bold magenta }

fn echo-title {|@str|
	log { echo (print-header) (maybe-styled (print $@str) bold cyan) }
}

fn echo-error {|@str|
	log { echo (print-header) (maybe-styled 'Error: '(print $@str) bold red) >&2 }
}

fn echo-warning {|@str|
	log { echo (print-header) (maybe-styled 'Warning: '(print $@str) bold yellow) >&2 }
}

fn echo-info {|@str|
	log { echo (print-header) (maybe-styled (print $@str) blue) }
}

fn section {|@title fc|
	echo-title $@title
	log { echo (print-header) (maybe-styled "============================================================================" cyan) }
	defer {
		log { echo (print-header) (maybe-styled "============================================================================" cyan) }
	}
	$fc
}

fn echo-to-file-context {|filename fc|
	$fc 1>$filename
}

fn padded-output {|&pad= ''|
	var first = $true

	each {|line|
		if $first {
			echo $pad
			set first = $false
		}

		echo $line
	}
}

fn format-time {|time-in-seconds|
	set time-in-seconds = (exact-num (math:round $time-in-seconds))
	var seconds = (% $time-in-seconds 60)
	var minutes = (% (math:floor (/ $time-in-seconds 60)) 60)
	var hours = (math:floor (/ $time-in-seconds 3600))

	put (if (> $hours 0) { put $hours'h ' } else { put '' })(if (or (> $minutes 0) (> $hours 0)) { put $minutes'm ' } else { put '' })$seconds's'
}

fn fail-if {|cond message|
	and $cond (fail $message) | discard
}

fn fail-if-nil {|value message|
	fail-if (eq $value $nil) $message
}

fn empty-str-if-nil {|v|
	if (eq $v $nil) { put '' } else { put $v }
}

fn nil-if-empty-str {|v|
	if (eq $v '') { put $nil } else { put $v }
}

fn select {|condition vtrue vfalse|
	if $condition { put $vtrue } else { put $vfalse }
}

fn default-if-nil {|value default|
	if (eq $value $nil) {
		put $default
	} else {
		put $value
	}
}

fn map-loop {|map fc|
	keys $map | each {|k|
		$fc $k $map[$k]
	}
}

fn p-map-loop {|map fc &num-workers=(num +inf)|
	keys $map | peach &num-workers=$num-workers {|k|
		$fc $k $map[$k]
	}
}

fn pairs {|map|
	keys $map | each {|k| put [$k $map[$k]] }
}

fn reverse-loop {|list fc|
	range (count $list) 0 | each {|i| $fc $list[(- $i 1)] }
}

fn p-reverse-loop {|list fc &num-workers=(num +inf)|
	range (count $list) 0 | peach &num-workers=$num-workers {|i| $fc $list[(- $i 1)] }
}

fn flag-if-not-nil {|flag value|
	if (not-eq $value $nil) {
		put '--'$flag'='$value
	}
}

fn flatten {|@array|
	for v $array {
		if (==s (kind-of $v) 'list') {
			flatten $@v
		} else {
			put $v
		}
	}
}

fn append-to-list {|list @items|
	if (eq $list $nil) {
		put [$@items]
	} else {
		conj $list $@items
	}
}

fn init-key {|map key value|
	if (not (has-key $map $key)) {
		set map[$key] = $value
	}

	put $map
}

fn with-key {|map key fc &or-else=$nop~|
	if (has-key $map $key) {
		$fc $map[$key]
	} else { $or-else }
}

fn with-keys {|map keys fc &or-else=$nop~|
	var cur = $map

	for k $keys {
		if (has-key $cur $k) {
			set cur = $cur[$k]
		} else {
			$or-else
			return
		}
	}

	$fc $cur
}

fn has-key-and-value {|map key value|
	and (has-key $map $key) (eq $map[$key] $value)
}

fn has-keys-and-value {|map keys value|
	with-keys $map $keys {|v| eq $v $value } &or-else= { put $false }
}

fn has-key-and-not-nil {|map key|
	and (has-key $map $key) (not-eq $map[$key] $nil)
}

fn has-key-and-not-empty {|map key|
	and (has-key $map $key) (not-empty $map[$key])
}

fn index-of {|list value|
	var index = 0
	for i $list {
		if (eq $i $value) { put $index; return }
		set index = (+ $index 1)
	}

	put $nil
}

fn print-to-value {|@values &sep=' '|
	print &sep=$sep $@values | slurp
}

fn nil-or-empty {|cont|
	or (eq $cont $nil) (empty $cont)
}

fn not-nil-or-empty {|cont|
	not (nil-or-empty $cont)
}

fn remove-empty {
	put {|x|
		if (not (empty $x)) { put $x }
	}
}

fn filter-out {|values &proj=$identity~ &on-filtered={|_|}|
	put {|x|
		if (not (has-value $values ($proj $x))) { put $x } else { $on-filtered $x }
	}
}

fn filter-in {|values &proj=$identity~ &on-filtered={|_|}|
	put {|x|
		if (has-value $values ($proj $x)) { put $x } else { $on-filtered $x }
	}
}

fn filter {|matcher &proj_in=$identity~ &proj_out=$identity~|
	put {|x|
		if ($matcher ($proj_in $x)) {
			$proj_out $x
		}
	}
}

fn recursive-loop {|list fc after|
	fn step {|cur @rest|
		$fc $cur { if (not (empty $rest)) { step $@rest } else { $after } }
	}

	step $@list
}

fn has-one-of {|list1 list2|
	for x $list1 {
		if (has-value $list2 $x) {
			put $true
			return
		}
	}

	put $false
}

fn escape-cmd {|cmd|
	str:replace '"' '\"' (str:replace '\' '\\' $cmd)
}

fn escape-quoted {|@args|
	for x $args {
		put '"'(escape-cmd $x)'"'
	}
}

fn serialize-for-eval {|fc|
	put $fc['body']
}

fn elvish-cmd {|@args|
	put $runtime:elvish-path -c (print (escape-quoted $@args))
}

fn bash-cmd {|@args|
	put bash -l -c (print (escape-quoted $@args))
}

#fn remove-empty {|list|
#	for x $list {
#		if (not (empty $x)) { put $x }
#	}
#}

fn concat-maps {|@maps|
	for m $maps {
		map-loop $m {|k v|
			put [$k $v]
		}
	} | make-map
}

#fn compact-non-ordered {
#	var added = []
#
#	each {|x|
#		if (not (has-value $added $x)) {
#			set added = (conj $added $x)
#			put $x
#		}
#	}
#}

fn compact-non-ordered {|@lists|
	var added = [&]

	for l $lists {
		for x $l {
			if (not (has-key $added $x)) {
				set added[$x] = $nil
				put $x
			}
		}
	}
}

fn index-loop {|list fc|
	range (count $list) | each $fc
}

fn default-conf-value {|map key default|
	if (has-key $map $key) {
		put $map[$key]
	} else {
		if (==s (kind-of $default) 'fn') {
			$default
		} else {
			put $default
		}
	}
}

fn default-conf-key {|map|
	each {|v|
		put [$v[0] (default-conf-value $map $v[0] $v[1])]
	}
}

fn clean-path {|path|
	os:eval-symlinks (path:abs $path)
}

fn realpath {|path &relative-base=$nil &relative-to=$nil|
	e:realpath -m (flag-if-not-nil 'relative-base' $relative-base) (flag-if-not-nil 'relative-to' $relative-to) $path
}

fn xdg-config-dir { env-or-default 'XDG_CONFIG_HOME' ~/'.config' }
fn xdg-cache-dir { env-or-default 'XDG_CACHE_HOME' ~/'.cache' }
fn xdg-runtime-dir { env-or-default 'XDG_RUNTIME_DIR' '/run' }
fn xdg-data-dir { env-or-default 'XDG_DATA_HOME' ~/'.local/share' }
fn xdg-data-dirs {
	# Non-standard but commonly used even though XDG_DATA_DIRS doesn't always include it.
	put ~/'.local/share'

	if (has-env 'XDG_DATA_DIRS') {
		str:split ':' $E:XDG_DATA_DIRS
	} else {
		put '/usr/local/share' '/usr/share'
	}
}
fn temp-dir {
	if (has-env 'TMPDIR') {
		put $E:TMPDIR
	} else {
		put '/tmp'
	}
}
fn pat-aur-temp-dir {
	put (temp-dir)/'pat-aur-'(e:id -u)
}

fn create-temp-dir {|name &sub-dir=''|
	var dir = (pat-aur-temp-dir)/$sub-dir
	os:mkdir-all $dir
	os:temp-dir &dir=$dir $name
}

fn create-temp-file {|name &sub-dir=''|
	var dir = (pat-aur-temp-dir)/$sub-dir
	os:mkdir-all $dir
	#os:temp-file &dir=$dir $name
	e:mktemp -p $dir $name
}

fn scoped-temp-dir {|name fc &sub-dir=''|
	var dir = (create-temp-dir $name &sub-dir=$sub-dir)
	defer { e:rm -rf $dir }
	$fc $dir
}

fn find-in-data-dirs {|name|
	for path [(xdg-data-dirs)] {
		if (os:exists $path/$name) {
			put $path/$name
			return
		}
	}

	fail $name' not found in XDG data directories.'
}

fn ask {|question default|
	log { print (print-header)' '(maybe-styled $question' ['(select $default 'Y' 'y')'/'(select $default 'n' 'N')'] ' bold) }

	var response = (read-line)
	if (empty $response) {
		put $default
	} else {
		set response = (str:to-lower $response)
		if (==s $response 'y') {
			put $true
		} else {
			put $false
		}
	}
}

fn handle-commands {|commands handlers &default={ fail 'No command given.' } &failure={ fail 'Invalid command.' }|
	if (empty $commands) {
		if (==s (kind-of $default) 'fn') {
			capture-return { $default }
			return
		} else {
			set commands = [$default]
		}
	}

	with-key $handlers $commands[0] &or-else={ capture-return $failure } {|o| capture-return $o $commands[1..] }
}

fn parse-host {|host|
	var split-pos = (str:last-index $host ':')

	if (or (== -1 $split-pos) (== (- (count $host) 1) $split-pos) ) {
		put $host
		put $nil
	} else {
		put $host[..$split-pos]
		put $host[(+ $split-pos 1)..]
	}
}

fn make {|obj|
	var new-obj = [&]

	keys $obj | each {|fname|
		var fc = $obj[$fname]

		var opt-args = [(index-loop $fc['opt-names'] {|i| put '&'$fc['opt-names'][$i]'='(repr $fc['opt-defaults'][$i]) })]
		var opt-args-passthrough = [(for arg $fc['opt-names'] { put '&'$arg'=$'$arg })]
		#log { echo $fname':' $@opt-args }
		#log { echo $@opt-args-passthrough }
		eval &ns=(ns [&new-obj= { put $new-obj } &fc= $fc]) ^
				&on-end={|ns| set new-obj[$fname] = $ns['__new-fc__~'] } ^
			(print 'fn __new-fc__ {|@args' $@opt-args '| $fc' $@opt-args-passthrough '($new-obj) $@args }')
	}

	if (has-key $new-obj 'ctor') {
		try {
			log { $new-obj[ctor] }
		} catch e {
			if (has-key $new-obj 'destroy') {
				$new-obj[destroy]
			}
			fail $e
		}
	}

	put $new-obj
}

fn scoped {|obj fc|
	defer {
		with-key $obj 'destroy' {|d| $d }
	}

	$fc $obj
}

fn scoped-list {|fc|
	var obj-list = []
	defer {
		for o $obj-list {
			if (has-key $o 'destroy') {
				$o[destroy]
			}
		}
	}

	each {|obj|
		set obj-list = (conj $obj-list $obj)
	}

	$fc $obj-list
}

var -scoped-lock-counter = 100
fn scoped-lock {|@args fc &excl=$true &timeout=0|
	recursive-loop $args {|file cont|
		set -scoped-lock-counter = (+ $-scoped-lock-counter 1)
		defer { set -scoped-lock-counter = (- $-scoped-lock-counter 1) }

		{
			e:flock (select $excl '-x' '-s') (if (!= $timeout 0) { print '-w '$timeout }) $-scoped-lock-counter

			$cont
		} $-scoped-lock-counter>$file
	} $fc
}

fn ctor-synchronized {|lock-file data|
	put [
		&lock= {|_ fc &excl=$true &timeout=0|
			scoped-lock &excl=$excl &timeout=$timeout $lock-file { $fc $data }
		}
	]
}

fn echo-exception {|e|
	fn print-exception {|e|
		with-keys $e ['reason' 'type'] {|type|
			if (==s $type 'fail') {
				if (==s (kind-of $e['reason']['content']) 'list') {
					for content $e['reason']['content'] {
						if (==s (kind-of $content) 'exception') {
							print-exception $content
						} else {
							echo-error $content
						}
					}
				} elif (==s (kind-of $e['reason']['content']) 'map') {
					var subtype = (default-conf-value $e['reason']['content'] 'type' $nil)

					echo-error (
						with-key $e['reason']['content'] 'message' {|except-mes|
							put $except-mes
						} &or-else= {
							if (not-eq $subtype $nil) {
								put $subtype
							} else {
								put 'unknown exception.'
							}
						}
					)

					with-key $e['reason']['content'] 'list' {|list|
						for sub-exc $list {
							print-exception $sub-exc
						}
					}
				} else {
					echo-error $e['reason']['content']
				}
			} elif (==s $type 'flow') {
				echo-error 'Flow error: '$e['reason']['name']
			} elif (==s $type 'external-cmd/exited') {
				echo-error 'Command `'$e['reason']['cmd-name']'` exited with code: '$e['reason']['exit-status']
			} elif (==s $type 'external-cmd/signaled') {
				echo-error 'Command `'$e['reason']['cmd-name']'` killed with signal: '$e['reason']['signal-name']
			} elif (==s $type 'pipeline') {
				for sub-exc $e['reason']['exceptions'] {
					if (not $sub-exc) {
						print-exception $sub-exc
					}
				}
			}
		} &or-else= {
			with-key $e 'reason' {|reason|
				echo-error 'Exception thrown: '(pprint $reason)
			} &or-else= {
				echo-error 'Exception thrown:'
				log { pprint $e > &2 }
			}
		}
	}

	print-exception $e
}

fn program-context {|fc &debug-varname='DEBUG' &silent=$false|
	log-context &silent=$silent {
		try {
			capture-return $fc
		} catch e {
			if (env-is-true $debug-varname) {
				pprint $e >&2
				fail $e
			} else {
				echo-exception $e

				if (and (has-key $e 'reason') (has-key $e['reason'] 'type') (==s $e['reason']['type'] 'fail') (==s (kind-of $e['reason']['content']) 'map')) {
					exit (default-conf-value $e['reason']['content'] 'code' 1)
				} elif (and (has-key $e 'reason') (has-key $e['reason'] 'exit-status')) {
					exit $e['reason']['exit-status']
				} else {
					exit 1
				}
			}
		}
	}
}

fn show-flags-help {|flag-specs cmd-specs &size=50|
	var size-extra-lines = (- $size 2)

	fn print-entry {|name desc|
		var slices = [(
			put (substr $name &size=$size)
			str-slice (substr $name &start=$size) $size-extra-lines {|s|
				put '  '$s
			}
		)]

		for s $slices[0..-1] {
			echo '  '$s
		}
		printf "  %-"$size"s %s\n" $slices[-1] $desc
	}

	if (not (empty $flag-specs)) {
		echo 'Switches:'

		for f $flag-specs {
			var flags = [({
				fn value-str {
					with-key $f 'type' {|type|
						if (eq $type 'boolean') {
							put 'yes/no'
						} elif (eq $type 'sep-list') {
							put 'value'(default-conf-value $f 'sep' ',')'...'
						} elif (eq $type 'json') {
							put 'json'
						} else {
							put 'value'
						}
					} &or-else= {
						put 'value'
					}
				}
				fn value-spec {|sign|
					if (has-key-and-value $f 'arg-optional' $true) {
						put '['$sign'<'(value-str)'>]'
					} elif (has-key-and-value $f 'arg-required' $true) {
						put $sign'<'(value-str)'>'
					} else {
						put ''
					}
				}
				with-key $f 'short' {|o|
					var vsp = (value-spec '')
					put '-'$o(if (not-empty $vsp) { put ' '$vsp } else { put '' })
				}
				with-key $f 'long' {|o|
					put (select (has-key $f 'short') '' '    ')'--'$o(value-spec '=')
				}
			})]

			print-entry (str:join ', ' $flags) $f['description']
			#printf "  %-"$size"s %s\n" (str:join ', ' $flags) $f['description']
		}
	}

	if (not (empty $cmd-specs)) {
		echo
		echo 'Commands:'
		for f $cmd-specs {
			print-entry $f['name'] $f['description']
		}
	}

	echo
}

fn cmd-in-dir {|dir @args|
	var cmd = (external (find-in-data-dirs 'pat-aur/scripts/chdir-cmd'))

	$cmd $dir $@args
}

fn makepkg-in-dir {|dir @args|
	cmd-in-dir $dir makepkg $@args
}

fn sign-package {|file key|
	e:gpg --local-user $key --detach-sign --sign $file
}

fn move-to-repo {|file repo-path repo-root &sign=$false &key=$nil|
	set repo-path = (clean-path $repo-path)

	scoped-lock $repo-path'.pataur.lock' {
		var file-base = (path:base $file)
		var new-path = $repo-root/$file-base

		e:mv -f $file $new-path

		try {
			e:repo-add ^
				(if $sign { put '--sign'; if (not-eq $key $nil) { put '--key' $key } }) ^
				(if (or (not $use-styles) (not $use-colors)) { put '--nocolor' }) ^
				-R $repo-path $new-path

			if (os:exists $file'.sig') {
				e:mv -f $file'.sig' $new-path'.sig'
			}
		} catch e {
			ignore-error {
				os:remove $new-path
				os:remove $new-path'.sig'
			}
			fail $e
		}
	}
}

fn init-repo {|dir name &sig-key=$nil|
	os:mkdir-all $dir
	tmp pwd = $dir
	e:tar cf $name'.db.tar.gz' -T /dev/null
	e:tar cf $name'.files.tar.gz' -T /dev/null
	e:ln -sf $name'.db.tar.gz' $name'.db'
	e:ln -sf $name'.files.tar.gz' $name'.files'
	if (not-eq $sig-key $nil) {
		e:gpg --local-user $sig-key --detach-sign --sign $name'.db.tar.gz'
		e:gpg --local-user $sig-key --detach-sign --sign $name'.files.tar.gz'
		e:ln -sf $name'.db.tar.gz.sig' $name'.db.sig'
		e:ln -sf $name'.files.tar.gz.sig' $name'.files.sig'
	}
}

fn sudo-ping {
	fn bg-ping {
		while $true {
			e:sudo -n true
			sleep 10
			if (not ?(e:kill -0 $pid)) {
				break
			}
		} 2>$os:dev-null
	}

	bg-ping &
}

fn get-repo-info {|repo &pm-conf=$nil|
	if (==s (kind-of $repo) 'map') {
		set repo['server'] = 'file://'$repo['root']
		set repo['path'] = (os:eval-symlinks $repo['root']'/'$repo['name']'.db')
		put $repo
	} else {
		var sig-level = []
		var server

		e:pacman-conf (if (not-eq $pm-conf $nil) { put '--config='$pm-conf }) --repo=$repo | each {|x|
			re:find '^SigLevel = (.+)$' $x | each {|m|
				set sig-level = (conj $sig-level $m['groups'][1]['text'])
			}
			re:find '^Server = (.+)$' $x | each {|m|
				set server = $m['groups'][1]['text']
			}
		}

		var res = [
			&name= $repo
			&server= $server
			&sig-level= $sig-level
		]

		if (str:has-prefix $server 'file://') {
			set res['root'] = (str:trim-prefix $server 'file://')
			set res['path'] = (os:eval-symlinks $res['root']'/'$repo'.db')
		}

		put $res
	}
}

fn get-local-repos-info {|&pm-conf=$nil|
	e:pacman-conf (if (not-eq $pm-conf $nil) { put '--config='$pm-conf }) --repo-list | each {|repo|
		var repo-info = (get-repo-info &pm-conf=$pm-conf $repo)
		if (has-key $repo-info 'root') {
			put $repo-info
		}
	}
}

fn get-repo-info-from-path {|path|
	var name = (path:base $path)
	put [
		&root= (path:dir $path)
		&path= (clean-path $path)
		&name= $name[0..(str:last-index $name '.db')]
	]
}

fn to-pacman-repo-config {
	each {|repo|
		var server = (
			if (has-key $repo 'root') {
				put 'file://'$repo['root']
			} else {
				put $repo['server']
			}
		)
		echo '['$repo['name']']'
		echo 'SigLevel = '(str:join ' ' (default-conf-value $repo 'sig-level' ['PackageOptional' 'DatabaseOptional']))
		echo 'Server = '$server
	}
}

var -escape-ninja-rule-codepoints = [
	[(str:to-codepoints 'a') (str:to-codepoints 'z')]
	[(str:to-codepoints 'A') (str:to-codepoints 'Z')]
	[(str:to-codepoints '0') (str:to-codepoints '9')]
	[(str:to-codepoints '_') (str:to-codepoints '_')]
	[(str:to-codepoints '-') (str:to-codepoints '-')]
]
fn escape-ninja-rule {|rule|
	fn valid-cp {|c|
		or (
			for r $-escape-ninja-rule-codepoints {
				and (>= $c $r[0]) (<= $c $r[1])
			}
		)
	}

	str:from-codepoints (str:to-codepoints $rule | each {|c|
		if (valid-cp $c) {
			put $c
		} else {
			str:to-codepoints '_-'(print $c)'-_'
		}
	})

	#re:replace &literal=$true '([^a-zA-Z0-9_-]{1})' '_-_' $rule
}

fn parse-args {|args flag-specs|
	var parsed-flags commands = (flag:parse-getopt $args $flag-specs)

	var flags = [&]
	for spec $flag-specs {
		if (has-key $spec 'default') {
			set flags[$spec['long']] = $spec['default']
		} elif (and (not (has-key-and-value $spec 'arg-required' $true)) (not (has-key-and-value $spec 'arg-optional' $true))) {
			set flags[$spec['long']] = $false
		}
	}

	#pprint $parsed-flags >&2
	for flag $parsed-flags {
		var long-name = $flag['spec']['long']

		if (and (not (has-key-and-value $flag['spec'] 'arg-required' $true)) (not (has-key-and-value $flag['spec'] 'arg-optional' $true))) {
		#if (not (has-key $flag 'arg')) {
			set flags[$long-name] = $true
		} else {
			var type = (default-conf-value $flag['spec'] 'type' $nil)

			if (eq $type 'boolean') {
				set flags[$long-name] = (
					if (empty $flag['arg']) {
						put $true
					} elif (string-is-false $flag['arg']) {
						put $false
					} elif (string-is-true $flag['arg']) {
						put $true
					} else {
						fail 'Invalid value for argument `'$long-name'`.'
					}
				)
			} elif (eq $type 'multi') {
				if (has-key $flags $long-name) {
					set flags[$long-name] = (conj $flags[$long-name] $flag['arg'])
				} else {
					set flags[$long-name] = [$flag['arg']]
				}
			} elif (eq $type 'sep-list') {
				set flags[$long-name] = [(str:split (default-conf-value $flag['spec'] 'sep' ',') $flag['arg'])]
			} elif (eq $type 'json') {
				set flags[$long-name] = (print $flag['arg'] | from-json)
			} else {
				set flags[$long-name] = $flag['arg']
			}
		}
	}

	put $flags
	put $commands
}

fn parse-target-config-flags {|args|
	var flags _ = (parse-args $args [
		[&long=arch &arg-required=$true &default=(e:uname -m | one)]
		[&long=host-machine-id &arg-required=$true &default=$nil]
		[&long=type &arg-required=$true &default='machine']
		[&long=target-id &arg-required=$true &default=$nil]
	])

	put $flags
}

fn detect-gcc-flags {
	str:split ' ' (e:gcc -'###' -E - -march=native 2>&1 | e:sed -r '/cc1/!d;s/(")|(^.* - )|( -mno-[^\ ]+)//g;s/-dumpbase \S+//') ^
		| each (remove-empty)
}

fn detect-rust-flags {
	if (has-external 'rustc') {
		re:find &max=1 'native - .*\(currently (.+)\)' (e:rustc -Ctarget-cpu=native --print=target-cpus | slurp) ^
			| each {|m|
				put '-Ctarget-cpu='$m['groups'][1]['text']
			}

		re:find 'target_feature="(.+)"' (e:rustc -Ctarget-cpu=native --print=cfg | slurp) ^
			| each {|m|
				put '-Ctarget-feature='$m['groups'][1]['text']
			}
	}
}

fn parse-srcinfo {
	var metadata = [&pkgnames= [&]]
	var section

	each {|l|
		if (or (empty $l) (==s $l[0] '#')) { continue }

		if (str:has-prefix $l 'pkgbase = ') {
			set metadata['pkgbase'] = (str:trim-prefix $l 'pkgbase = ')
		} elif (str:has-prefix $l 'pkgname = ') {
			set section = (str:trim-prefix $l 'pkgname = ')
			set metadata['pkgnames'][$section] = [&]
		} else {
			var parsed
			re:find '^[\s]+(.+) = (.*)' $l | each {|v| set parsed = $v }

			if (and (not-eq $parsed $nil) (>= (count $parsed['groups']) 3)) {
				var key value = (put $parsed['groups'][1]['text']; put $parsed['groups'][2]['text'])

				if (eq $section $nil) {
					set metadata = (init-key $metadata $key [])
					set metadata[$key] = (conj $metadata[$key] $value)
				} else {
					set metadata['pkgnames'][$section] = (init-key $metadata['pkgnames'][$section] $key [])
					set metadata['pkgnames'][$section][$key] = (conj $metadata['pkgnames'][$section][$key] $value)
				}
			}
		}
	}

	put $metadata
}

fn get-srcinfo-values-by-arch {|srcinfo key &arch='x86_64' &versioned=$true &pkgbase-only=$false|
	fn parse-section {|section|
		fn parse-key {|key|
			with-key $section $key {|data|
				if $versioned {
					all $data
				} else {
					for v $data {
						re:find '^([^<=>\s]+)' $v | each {|m| put $m['groups'][1]['text'] }
					}
				}
			}
		}

		parse-key $key
		parse-key $key'_'$arch
	}

	{
		parse-section $srcinfo

		if (not $pkgbase-only) {
			map-loop $srcinfo['pkgnames'] {|_ pkg|
				parse-section $pkg
			}
		}
	} | order | compact
}

fn parse-build-cmd {|cmd|
	var parts = [(str:split &max=3 ':' $cmd)]
	var options = (if (<= (count $parts) 1) { put 'b' } else { put $parts[0] })
	var names = [(str:split ',' (if (<= (count $parts) 1) { put $parts[0] } else { put $parts[1] }))]
	var targets = [(if (<= (count $parts) 2) { put '.' } else { str:split ',' $parts[2] })]

	var cmd = 'b'
	var force = $false
	var force-all = $false
	var install = $false
	var sys = $false
	var downgrades = $false
	var try = $false
	for c $options {
		if (has-value [b m a u U] $c) {
			set cmd = $c
		} elif (==s $c 'f') {
			set force = $true
		} elif (==s $c 'i') {
			set install = $true
		} elif (==s $c 's') {
			set sys = $true
		} elif (==s $c 'd') {
			set downgrades = $true
		} elif (==s $c 't') {
			set try = $true
		}
	}

	if (and (has-value [u U] $cmd) (empty $names)) {
		set names = ['']
	}

	for t $targets {
		for n $names {
			put [
				&cmd= $cmd
				&name= $n
				&target= $t
				&forced= $force
				&install= $install
				&sys= $sys
				&downgrades= $downgrades
				&try= $try
			]
		}
	}
}

fn parse-gcc-version {|str|
	re:find '.+ \(.+\) (.+)' $str | take 1 | each {|m| put $m['groups'][1]['text'] }
}

fn substitute-in-pacman-conf {|str substitute|
	each {|line|
		var comment-index = (str:index $line '#')

		if (== $comment-index 0) {
			put $line
		} elif (< $comment-index 0) {
			str:replace $str $substitute $line
		} else {
			put (str:replace $str $substitute $line[0..$comment-index])$line[$comment-index..]
		}
	}
}

fn retry {|fc &nb=3 &sleep-time=3 &on-retry={ echo 'Retrying...' }|
	range $nb | each {|i|
		var failed = $true

		try {
			$fc
			set failed = $false
		} catch e {
			if (== $i (- $nb 1)) {
				fail $e
			}
			sleep $sleep-time
		}

		if (not $failed) {
			break
		}

		$on-retry
	}
}

fn try-until-success {|&proj={|x| $x } &fail-msg='All failed.'|
	var success = $false

	each {|fc|
		try {
			$proj $fc
			set success = $true
			break
		} catch e { }
	}

	if (not $success) {
		fail $fail-msg
	}
}

fn safe-recurse {|start fc &key={|x| put $x } &on-loop-detect=$nop~|
	var stack = [&]

	fn recurse {|x|
		var k = ($key $x)
		if (has-key $stack $k) {
			$on-loop-detect
			return
		}
		set stack[$k] = $nil

		$fc $x {|next| recurse $next }
	}

	recurse $start
}

fn trim-right-of-first {|str sep|
	var index = (str:index $str $sep)
	if (>= $index 0) {
		put $str[0..$index]
	} else {
		put $str
	}
}

fn trim-right-of-last {|str sep|
	var index = (str:last-index $str $sep)
	if (>= $index 0) {
		put $str[0..$index]
	} else {
		put $str
	}
}

fn trim-left-of-first {|str sep|
	put $str[(+ (str:index $str $sep) 1)..]
}

fn trim-left-of-last {|str sep|
	put $str[(+ (str:last-index $str $sep) 1)..]
}

fn ensure-suffix {|str suffix|
	if (str:has-suffix $str $suffix) {
		put $str
	} else {
		put $str$suffix
	}
}

fn mkdir-safe {|dir|
	if (not (os:exists $dir)) {
		os:mkdir-all $dir
	}
}

fn parallel-cmd-sem {|&max-jobs=$nil &id-sem=$nil|
	var sem-args = [(
		if (not-eq $id-sem $nil) { put '--id' $id-sem }
		if (not-eq $max-jobs $nil) { put '-j' (print $max-jobs) }
	)]

	put 'parallel' (if (not-eq $max-jobs $nil) { put -j (print (* $max-jobs 2)) }) -- ^
		'echo {} | sem --pipe --fg' (put $@sem-args) 'bash'
}

fn host-arch {
	e:uname -m | one
}

fn makepkg-option {|opts name default|
	var res = $default

	for o $opts {
		if (==s $o '!'$name) {
			set res = $false
		} elif (==s $o $name) {
			set res = $true
		}
	}

	put $res
}

fn string-to-args {|str|
	all (e:bash -c (print $runtime:elvish-path -c "'put $args | to-json'" $str) | from-json)
}

fn parse-mirrors {|&pm-conf=$nil &arch=$nil|
	fn pacconf {|@args|
		e:pacconf ^
			(if (not-eq $pm-conf $nil) { put '--config='$pm-conf }) ^
			(if (not-eq $arch $nil) { put '--arch='$arch }) ^
			$@args
	}

	var repos = [&]

	pacconf --repo-list | each {|repo|
		set repos[$repo] = [(
			pacconf --repo=$repo | each {|line|
				if (str:has-prefix $line 'Server = ') {
					str:trim-prefix $line 'Server = '
				}
			}
		)]
	}

	put $repos
}

# Too slow to be usable.
fn get-needed-pkg-files {|cmds &skip-installed=$true|
	fn resolve-provider {|p on-remap|
		var pkgs = [($cmds[pacsift] --sync --exists --satisfies=$p | from-lines)]
		var pkgs-map = (for e $pkgs {
			var parts = [(str:split &max=2 '/' $e)]
			put [$parts[1] $nil]
		} | make-map)

		if (has-key $pkgs-map $p) {
			put $p
		} else {
			$on-remap
			put [(str:split &max=2 '/' $pkgs[0])][1]
		}
	}

	fn exclude-installed {
		peach {|p|
			if ?(silence-output { $cmds[pacsift] --local --not-exists --satisfies=$p }) {
				put $p
			}
		}
	}

	fn resolve-deps {
		var remapped = $true
		var remapped-map = [&]
		var done-map = [&]

		fn map-providers {
			each {|p|
				if (not (has-key $done-map $p)) {
					var p-remapped = $false
					var remapped-as = (resolve-provider $p { set p-remapped = $true })
					if $p-remapped {
						set remapped = $true
						set remapped-map[$p] = $nil
					}
					set done-map[$p] = $nil
					$cmds[pactree] -su $remapped-as | from-lines
				} elif (not (has-key $remapped-map $p)) {
					put $p
				}
			}
		}

		var pkgs = [(map-providers)]

		while $remapped {
			set remapped = $false
			set pkgs = [(all $pkgs | map-providers)]
		}

		order $pkgs | compact
	}

	var names = [(
		resolve-deps ^
			| each {|p| log { echo $p }; put $p } ^
			| { if $skip-installed { exclude-installed } else { all } }
	)]

	if (not-empty $names) {
		$cmds[pacsift] --sync --exact (for n $names { put '--name='$n }) ^
			| $cmds[pacinfo] ^
			| {
				var cur = [&]

				each {|line|
					if (empty $line) {
						put $cur
						set cur = [&]
					} elif (str:has-prefix $line 'File:') {
						set cur['file'] = (str:trim-space [(str:split &max=2 ':' $line)][1])
					} elif (str:has-prefix $line 'Repository:') {
						set cur['repo'] = (str:trim-space [(str:split &max=2 ':' $line)][1])
					}
				}
			} ^
			| uniq-key {|x| put $x['file'] }
	}
}

fn extract-pkginfo {|filename|
	e:tar -xf $filename '.PKGINFO' -O | each {|line|
		if (or (empty $line) (==s $line[0] '#')) { continue }
		put [(str:split &max=2 ' = ' $line)]
	} | make-map
}
