use str

var flags = [
	&'--install'= 1 &'--try'= 1 &'--try-all'= 1 &'--force'= 1 &'--force-all'= 1 &'--target'= 2 &'--root-path'= 2 &'--arch'= 2
	&'--read-only'= 1 &'--as-root'= 1 &'--gui'= 1 &'--bind-session-bus'= 1 &'--login'= 1 &'--temp'= 1
	&'--jobs'= 2 &'--pools'= 2 &'--ignore'= 2 &'--skip-deps'= 1 &'--skip-gpg-checks'= 1 &'--skip-review'= 1
	&'--skip-aur-fetch'= 1 &'--check'= 2 &'--container-paths'= 2 &'--repo'= 2 &'--styles'= 2 &'--ignore-arch'= 2
	&'--confirm'= 2 &'--sudo-loop'= 1 &'--clean'= 2 &'--clean-build'= 2 &'--report-file'= 2 &'--regex'= 1
]

set edit:completion:arg-completer['pat-aur'] = {|@args|
	var cur = $args[(- (count $args) 1)]

	var prev-word = ''
	var commands = [(
		range 1 (count $args) | each {|i|
			set prev-word = $args[(- $i 1)]
			var word = $args[$i]

			if (and (not (str:has-prefix $word '-')) ^
			        (or (not (str:has-prefix $prev-word '-')) ^
			            (and (has-key $flags $prev-word) (== $flags[$prev-word] 1)))) {
				put $word
			}
		}
	)]

	#pprint $commands

	var nb = (count $commands)

	if (str:has-prefix $cur '-') {
		keys $flags
	} elif (and (str:has-prefix $prev-word '-') (has-key $flags $prev-word) (== $flags[$prev-word] 2)) {
		if (==s $prev-word '--arch') {
			e:pat-aur list arch | from-lines
		} elif (==s $prev-word '--target') {
			e:pat-aur list repos | from-lines
		} elif (==s $prev-word '--styles') {
			put 'yes' 'no' 'detect'
		} elif (has-value ['--check' '--confirm' '--ignore-arch'] $prev-word) {
			put 'yes' 'no'
		}
	} else {
		if (<= $nb 1) {
			put 'help' 'make' 'layer-run' 'bootstrap-run' 'target-config' 'host-config' 'container' 'cache' 'repo'
		} else {
			if (==s $commands[0] 'repo') {
				if (== $nb 2) {
					put 'init' 'add' 'remove' 'clear' 'path' 'root' 'list'
				}
			} elif (==s $commands[0] 'cache') {
				if (== $nb 2) {
					put 'clear'
				} else {
					if (==s $commands[1] 'clear') {
						if (== $nb 3) {
							put 'pkg' 'source' 'log' 'sync' 'aur'
						}
					}
				}
			} elif (==s $commands[0] 'container') {
				if (== $nb 2) {
					put 'bootstrap' 'install' 'uninstall' 'install-files' 'delete' 'update' 'pacman-key' 'exec' 'layer'
				}
			}
		}
	}
}
