use os
use str
use re

use ./utils u
use ./config c
use ./ssh
use ./graph g

var install-first-packages = (put 'archlinux-keyring' | u:make-set)

fn run {|fc &silent=$false &conf-path=$nil &use-styles='detect'|
	if (u:string-is-true $use-styles) {
		set u:use-styles = $true
	} elif (u:string-is-false $use-styles) {
		set u:use-styles = $false
	}
	u:set-use-colors (and $u:use-styles (not (u:env-is-not-empty 'NO_COLOR')))

	set c:conf = (c:read-host-config &conf-path=$conf-path)
	#pprint $c:conf

	u:program-context &debug-varname='PATAUR_DEBUG' &silent=$silent $fc
}

fn setup-executor {|target fc|
	if (or (eq $target $nil) (eq $target '.')) {
		var local-exec = {|cmd @args &close-stdin=$false|
			#u:log { echo $cmd $@args }
			if $close-stdin {
				(external $cmd) $@args <&-
			} else {
				(external $cmd) $@args
			}
		}
		$fc [
			&type= 'local'
			&executor= [
				&exec= $local-exec
				&exec-shell= $local-exec
				&bash-script= {|@script| bash -l -c (print $@script) }
				&bash-script-shell= {|@script| bash -l -c (print $@script) }
				&target-config= { c:read-target-config-by-host (c:try-exec-target-config) }
			]
			&master-ssh= $nil
		]
	} elif (has-key $c:conf['repos'] $target) {
		var local-exec = {|cmd @args| (external $cmd) $@args }
		var target-specs = $c:conf['repos'][$target]

		$fc [
			&type= 'repo'
			&executor= [
				&exec= $local-exec
				&exec-shell= $local-exec
				&bash-script= {|@script| bash -l -c (print $@script) }
				&bash-script-shell= {|@script| bash -l -c (print $@script) }
				&target-config= {
					var tc = (c:try-exec-target-config ^
						&arch=$target-specs['arch'] ^
						&target-id=$target ^
						&type='repo' ^
						&target-conf-file=$target-specs['config-cmd'] ^
						&pm-conf=(if (has-key $target-specs 'pacman-conf') { put $target-specs['pacman-conf'] } else { put $nil }))
					put (c:read-target-config-by-host $tc)
				}
			]
			&master-ssh= $nil
		]
	} else {
		var master-ssh-socket = (
			if (and (os:exists $target) (==s (os:stat $target)['type'] 'socket')) {
				put $target
			} else {
				put (u:xdg-runtime-dir)'/pat-aur-ssh-socket-'$pid(if (eq $target $nil) { put '' } else { put '-'$target })
			}
		)

		u:scoped (u:make (ssh:ctor-ssh-connection $master-ssh-socket $target)) {|master-ssh|
			$fc [
				&type= 'remote'
				&executor= [
					&exec= {|cmd @args &close-stdin=$false| $master-ssh[exec] &close-stdin=$close-stdin (u:bash-cmd $cmd $@args) }
					&exec-shell= {|cmd @args &close-stdin=$false| $master-ssh[exec-shell] &close-stdin=$close-stdin (u:bash-cmd $cmd $@args) }
					&bash-script= {|@script| $master-ssh[exec] bash -l -c (print $@script) }
					&bash-script-shell= {|@script| $master-ssh[exec-shell] bash -l -c (print $@script) }
					&target-config= {
						c:read-target-config-by-host (
							$master-ssh[exec] (u:bash-cmd pat-aur target-config --host-machine-id=(cat '/etc/machine-id' | one)) | from-json
						)
					}
				]
				&master-ssh= $master-ssh
			]
		}
	}
}

fn pacman-cmd {|target-config @args|
	all $target-config['pacman-cmd']
	if (or (not $u:use-styles) (not $u:use-colors)) { put '--color' 'never' }
	put $@args
}

fn pacman-install-cmd {|target-config @args|
	all $target-config['pacman-install-cmd']
	if (or (not $u:use-styles) (not $u:use-colors)) { put '--color' 'never' }
	put $@args
}

fn install-packages {|to-install executor target-config &refresh=$true &noconfirm=$false &copy-sync-db=$nil|
	if (u:not-empty $to-install) {
		$executor[bash-script-shell] (
			if $copy-sync-db {
				all $target-config['sudo-cmd']
				put 'paclock' --run cp -f $copy-sync-db'/*' '/var/lib/pacman/sync;'

				all $target-config['sudo-cmd']
				put 'paclock' --run chmod 644 '/var/lib/pacman/sync/*;'
			}

			var install-first = []

			set to-install = [(
				for p $to-install {
					if (has-key $install-first-packages $p) {
						set install-first = (conj $install-first $p)
					} else {
						put $p
					}
				}
			)]

			if (u:not-empty $install-first) {
				all $target-config['sudo-cmd']
				pacman-install-cmd $target-config ^
					(if $noconfirm { put '--noconfirm'}) ^
					(if $refresh { put '-y'}) ^
					$@install-first
				put ';'
			}

			all $target-config['sudo-cmd']
			pacman-install-cmd $target-config ^
				(if $noconfirm { put '--noconfirm'}) ^
				(if $refresh { put '-y'}) ^
				$@to-install
		)
	}
}

fn setup-targets {|targets host-target-config fc|
	var targets-desc = [&]
	var target-cmd-mapping = [&]

	var all-targets = [({
		all $targets
		put $host-target-config['id']
		#u:map-loop $c:conf['repos'] {|k t| if (u:has-key-and-value $t 'remappable' $true) { put $k } }
		keys $c:conf['repos']
	} | order | compact)]
	#u:log { pprint $all-targets }

	u:recursive-loop $all-targets {|target cont|
		setup-executor (u:select (eq $target $host-target-config['id']) $nil $target) {|target-desc|
			var tc = (
				if (eq $target $host-target-config['id']) {
					put $host-target-config
				} else {
					$target-desc['executor'][target-config]
				}
			)
			set target-desc['target-config'] = $tc
			set target-cmd-mapping[$target] = $tc['id']
			set target-desc['is-host'] = $false
			set target-desc['is-target'] = (has-value $targets $target)
			set targets-desc[$tc['id']] = $target-desc
			$cont
		}
	} {
		var archs = [(keys $c:conf['arch'])]
		var host-targets-desc = [&]

		u:recursive-loop $archs {|arch cont|
			var arch-config = (c:get-arch-config $arch)

			var tc = (
				if (eq $arch $host-target-config['arch']) {
					put $host-target-config
				} else {
					c:read-target-config-by-host (
						c:try-exec-target-config &arch=$arch &target-conf-file=$arch-config['host-config']
					)
				}
			)
			#u:log { echo $arch':'; echo; pprint $arch-config; echo }
			set host-targets-desc[$arch] = $tc['id']

			if (not (has-key $targets-desc $tc['id'])) {
				setup-executor (u:select (eq $tc['id'] $host-target-config['id']) $nil $tc['id']) {|target-desc|
					set target-desc['target-config'] = $tc
					set target-cmd-mapping[$tc['id']] = $tc['id']
					set target-desc['arch-config'] = $arch-config
					set target-desc['is-target'] = $false
					set target-desc['is-host'] = $true
					set targets-desc[$tc['id']] = $target-desc
					$cont
				}
			} else {
				set targets-desc[$tc['id']]['arch-config'] = $arch-config
				set targets-desc[$tc['id']]['is-host'] = $true
				$cont
			}
		} {
			#u:log { pprint $host-targets-desc }
			$fc $targets-desc $host-targets-desc $target-cmd-mapping
		}
	}
}

fn parse-commands {|commands host-target-id|
	for cmd $commands {
		u:parse-build-cmd $cmd | each {|p|
			if (eq $p['target'] '.') {
				set p['target'] = $host-target-id
			}
			put $p
		}
	}
}

fn filter-updates {|target ignore-list|
	each (u:filter-out ^
			&proj={|x| put $x['name'] } ^
			&on-filtered={|x| u:echo-info 'Update for '$x['name']' on '$target' ignored.' } ^
		$ignore-list)
}

fn parse-updates {|target type|
	each {|line|
		var pkg oldversion _ newversion = (str:split ' ' $line)

		put [&name= $pkg &target= $target &type= $type &oldver= $oldversion &newver= $newversion]
	}
}

# Check for system updates.
fn check-sys {|pkgs ignore-list target target-desc check-updates-script allow-downgrades force-refresh|
	$target-desc['executor'][exec] bash -l -c 'cat > /tmp/pat-aur-check-updates.elv; chmod +x /tmp/pat-aur-check-updates.elv' < $check-updates-script
	var update-report = ($target-desc['executor'][exec] '/tmp/pat-aur-check-updates.elv' (if $allow-downgrades { put -d }) (if $force-refresh { put -f }) (for i $ignore-list { put -i $i }) | from-json)

	for u $update-report['updates'] {
		put [
			&repo= $u[1]
			&name= $u[2]
			&newver= $u[3]
			&oldver= $u[0]
			&target= $target
			&type= 'sys'
			&provides= [(all $u | drop 4)]
		]
	} ^
	| order &key={|p| put $p['name'] }

	for u $update-report['problems'] {
		put [
			&name= $u
			&type= 'problem'
			&target= $target
		]
	}
}

# Check for AUR updates from repository or system.
fn check-aur {|pkgs ignore-list target target-desc local-repos pacman-conf-tpl temp-dir check-updates-script|
	if (==s $target-desc['type'] 'repo') {
		var repo-conf = (all $local-repos | u:to-pacman-repo-config | slurp)

		print $pacman-conf-tpl | u:substitute-in-pacman-conf '@local-repo@' $repo-conf | to-lines ^
			> $temp-dir/'pacman.'$target'.conf'

		for r $local-repos {
			e:aur repo -l -c $temp-dir/'pacman.'$target'.conf' -d $r['name'] -f '%n %v\n'
		} | {
			if (u:empty $pkgs) {
				from-lines
			} else {
				each (u:filter-in $pkgs &proj={|x| put $x[0..(str:index $x ' ')] })
			}
		}
	} else {
		$target-desc['executor'][exec] (all $target-desc['target-config']['pacman-cmd']) -Q $@pkgs | from-lines
	} | {
		var pkgs-to-check = (to-lines | slurp)

		u:all-of $target-desc['target-config']['aur'] | each {|aur-loc|
			print $pkgs-to-check | e:env (c:define-aur-location (c:select-aur-location $aur-loc)) ^
					aur vercmp ^
				| parse-updates $target 'aur' ^
				| each {|p|
					set p['aur-loc'] = $aur-loc
					put $p
				}
		}
	} ^
	| u:uniq-key {|e| put $e['name'] } ^
	| filter-updates $target (conj $ignore-list (keys $target-desc['target-config']['aur-blacklist'])) ^
	| each {|p|
		set p['cmd'] = 'b'
		set p['forced'] = $false
		set p['force-all'] = $false
		set p['install'] = $false
		set p['try'] = $false
		put $p
	}
}

# Check updates from official arch repositories that we want to compile from source.
fn check-official {|pkgs ignore-list target target-desc local-repos pacman-conf-tpl temp-dir force-refresh|
	var compile-from-source = [(
		all $target-desc['target-config']['compile-from-source'] ^
			| { if (u:empty $pkgs) { all } else { each (u:filter-in $pkgs) } }
	)]

	if (u:empty $compile-from-source) {
		return
	}

	u:silence {
		var esc-target = (u:escape-quoted $target)
		print $pacman-conf-tpl | u:substitute-in-pacman-conf '@local-repo@' '' | to-lines ^
			| $target-desc['executor'][bash-script] ^
				'mkdir -p /tmp/pat-aur-updates-'$esc-target'/sync-updates; ln -snf /var/lib/pacman/local /tmp/pat-aur-updates-'$esc-target'/sync-updates/local; cat - > /tmp/pat-aur-updates-'$esc-target'/pacman.conf'
		u:retry {
			$target-desc['executor'][exec] ^
				pacsync ^
					(if $force-refresh { put --force }) ^
					--config='/tmp/pat-aur-updates-'$target'/pacman.conf' ^
					--dbpath='/tmp/pat-aur-updates-'$target'/sync-updates'
		}
	}

	if (==s $target-desc['type'] 'repo') {
		var target-repo-pkgs = (
			for r $local-repos {
				e:aur repo -l -c $temp-dir/'pacman.'$target'.conf' -d $r['name']
			} | each {|l|
				var parts = [(str:split "\t" $l)]

				if (has-value $compile-from-source $parts[0]) {
					put $parts
				}
			} | make-map
		)

		#u:log { pprint $target-repo-pkgs }

		if (u:not-empty [(keys $target-repo-pkgs)]) {
			$target-desc['executor'][exec] (all $target-desc['target-config']['pacman-cmd']) ^
					-Sl --config='/tmp/pat-aur-updates-'$target'/pacman.conf' ^
					--dbpath='/tmp/pat-aur-updates-'$target'/sync-updates' ^
				| each {|l|
					var parts = [(str:split ' ' $l)]
					if (has-key $target-repo-pkgs $parts[1]) {
						#u:log { echo 'Comparing version of '$parts[1]' '$target-repo-pkgs[$parts[1]]' -> '$parts[2] }
						if (==s (e:vercmp $parts[2] $target-repo-pkgs[$parts[1]] | slurp) '1') {
							put $parts[1]' '$target-repo-pkgs[$parts[1]]' -> '$parts[2]
						}
					}
				}
		}
	} else {
		u:ignore-errors {
			$target-desc['executor'][exec] (all $target-desc['target-config']['pacman-cmd']) ^
					-Qu --config='/tmp/pat-aur-updates-'$target'/pacman.conf' ^
					--dbpath='/tmp/pat-aur-updates-'$target'/sync-updates' ^
				(all $compile-from-source)
		}
	} ^
	| parse-updates $target 'arch' ^
	| filter-updates $target $ignore-list ^
	| each {|p|
		set p['cmd'] = 'a'
		set p['forced'] = $false
		set p['force-all'] = $false
		set p['install'] = $false
		set p['try'] = $false
		put $p
	}
}

# Check for periodical updates.
fn check-periodic {|target target-desc temp-dir|
	var cur-time = (num (e:date -u +'%s'))
	var user-period-check-state-dir = '.local/state/pat-aur/periodic-checks'

	if (==s $target-desc['type'] 'repo') {
		set user-period-check-state-dir = $user-period-check-state-dir/$target
	}

	var periodic-checks-dir = (
		if (u:not-empty $target-desc['target-config']['periodic-update-check']) {
			if (not-eq $target-desc['master-ssh'] $nil) {
				var dir = $temp-dir/'periodic-checks-'$target
				os:mkdir-all $dir
				#u:log { e:ls $dir }
				put $dir
			} else {
				put ~/$user-period-check-state-dir
			}
		} else {
			put $nil
		}
	)

	var target-home
	if (and (not-eq $periodic-checks-dir $nil) (not-eq $target-desc['master-ssh'] $nil)) {
		set target-home = ($target-desc['executor'][bash-script] ^
			"mkdir -p ~/"(u:escape-quoted $user-period-check-state-dir)"; echo \"$HOME\"" | one)
		$target-desc['master-ssh'][mount] $target-home/$user-period-check-state-dir $periodic-checks-dir
	} else {
		os:mkdir-all ~/$user-period-check-state-dir
	}

	defer {
		if (and (not-eq $periodic-checks-dir $nil) (not-eq $target-desc['master-ssh'] $nil)) {
			$target-desc['master-ssh'][unmount] $periodic-checks-dir
		}
	}

	for periodic-updates $target-desc['target-config']['periodic-update-check'] {
		for p $periodic-updates['pkgs'] {
			var filename = $periodic-checks-dir/$p

			var need-update = (
				if (os:exists $filename) {
					var mod-time = (num (e:stat -c '%Z' $filename))
					var due-check-time = (+ $mod-time (* $periodic-updates['frequency'] 86400))

					<= $due-check-time $cur-time
				} else {
					put $true
				}
			)

			if $need-update {
				put [
					&cmd= 'b'
					&name= $p
					&target= $target
					&type= 'aur'
					&periodic= $true
					&forced= $true
					&install= $false
					&try= $false
				]

				e:touch $filename
			}
		}
	}
}

fn check-rebuild {|targets-desc targets update-pkgs|
	#u:log { pprint $update-pkgs }

	all $targets | peach {|target|
		var target-desc = $targets-desc[$target]

		var aur-pkgs = [(
			u:all-of $target-desc['target-config']['aur'] | each {|aur-loc|
				u:silence-errors { g:aur-pkglist &aur-location=$aur-loc } | u:make-set
			}
		)]

		var updates = ({
			for u $update-pkgs {
				if (and (==s $u['type'] 'sys') (has-key $u 'oldver') (==s $u['target'] $target)) {
					put [$u['name'] $u]
				}
			}

			var aur-updates = (
				for u $update-pkgs {
					if (and (==s $u['type'] 'aur') (has-key $u 'oldver') (==s $u['target'] $target)) {
						put [$u['name'] $u]
					}
				} | make-map
			)

			if (not (u:empty [(keys $aur-updates)])) {
				u:all-of $target-desc['target-config']['aur'] | each {|aur-loc|
					keys $aur-updates | g:aur-query-info &aur-location=$aur-loc &remove-version=$false
				} ^
				| u:uniq-key {|e| put $e['name'] } ^
				| each {|p|
					var out = $aur-updates[$p['name']]
					set out['provides'] = $p['provides']
					put [$p['name'] $out]
				}
			}
		} | make-map)

		fn find-aur-pkg {|pkg|
			for list $aur-pkgs {
				if (has-key $list $pkg) {
					put $true
					return
				}
			}

			put $false
		}

		var updates-provides-map = [&]

		u:map-loop $updates {|name u|
			set updates-provides-map[$name] = (u:append-to-list (u:default-conf-value $updates-provides-map $name $nil) $u['newver'])

			for prov $u['provides'] {
				if (str:contains $prov '=') {
					var prov-name prov-version = (str:split &max=2 '=' $prov)
					set updates-provides-map[$prov-name] = (u:append-to-list (u:default-conf-value $updates-provides-map $prov-name $nil) $prov-version)
				}
			}
		}

		fn find-providers {|dep mod ver|
			u:with-key $updates-provides-map $dep {|entry|
				put $true
				var found = $false
				for prov-ver $entry {
					if (g:dep-satisfied $ver $mod $prov-ver) {
						set found = $true
						break
					}
				}
				put $found
			} &or-else= { put $false $false }
		}

		$target-desc['executor'][exec] expac '%n	%D' | peach {|line|
			var name depends-str = (str:split &max=2 '	' $line)

			if (and (not (has-key $updates $name)) (find-aur-pkg $name)) {
				var depends = [(if (u:not-empty $depends-str) { str:split '  ' $depends-str })]
				for d $depends {
					var parsed-dep = [(g:parse-depstring $d)]
					if (== (count $parsed-dep) 3) {
						var found provided = (find-providers $@parsed-dep)

						if (and $found (not $provided)) {
							u:echo-info 'Dependency `'$d'` of `'$name'` for target `'$target'` not satisfied, adding to rebuild list.'
							put [
								&cmd= 'b'
								&name= $name
								&target= $target
								&type= 'aur'
								&periodic= $false
								&forced= $true
								&install= $false
								&rebuild= $true
								&try= $false
							]
							break
						}
					}
				}
			}
		}
	}
}

fn check-updates {|targets-desc updates|
	var pacman-conf-path = (u:find-in-data-dirs 'pat-aur/defaults/pacman.conf.in')
	var pacman-conf-tpl = (cat $pacman-conf-path | slurp)
	var temp-dir = (u:create-temp-dir 'updates-')
	defer { e:rm -rf --one-file-system $temp-dir }
	var check-updates-script = (u:find-in-data-dirs 'pat-aur/scripts/check-updates')

	keys $updates | peach {|target|

		var target-desc = $targets-desc[$target]
		var update-desc = $updates[$target]

		var local-repos = [(if (==s $target-desc['type'] 'repo') {
			put $target-desc['target-config']['local-repo']
		})]

		var checks = [(
			if (u:has-key-and-value $target-desc 'is-target' $true) {
				if (and $update-desc['sys'] (!=s $target-desc['type'] 'repo')) {
					put {
						check-sys $update-desc['pkgs'] $update-desc['ignore'] $target $target-desc $check-updates-script $update-desc['downgrades'] $update-desc['refresh']
					}
				}

				put {
					check-aur $update-desc['pkgs'] $update-desc['ignore'] $target $target-desc $local-repos $pacman-conf-tpl $temp-dir $check-updates-script
				}

				if (u:not-empty $target-desc['target-config']['compile-from-source']) {
					put {
						check-official $update-desc['pkgs'] $update-desc['ignore'] $target $target-desc $local-repos $pacman-conf-tpl $temp-dir $update-desc['refresh']
					}
				}

				if (u:not-empty $target-desc['target-config']['periodic-update-check']) {
					put { check-periodic $target $target-desc $temp-dir }
				}
			}
		)]

		if (u:not-empty $checks) {
			#for c $checks { $c }
			run-parallel $@checks
			u:log { echo 'Checked for `'$target'`.' }
		}
	}
}
