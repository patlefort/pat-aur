use path
use os
use str
use re

use ./utils u

fn remove-container-files {|@args|
	e:unshare -r --map-auto -- rm $@args
}

fn bind-local-repos {
	each {|info|
		put [&ro-bind= [$info['root'] $info['root']]]
	}
}

fn resolve-path {|container-path path|
	var rp = (u:realpath &relative-base=$container-path/'files/root' $container-path/'files/root'$path)
	if (!=s $rp[0] '/') {
		put '/'$rp
	} else {
		put $rp
	}
}

fn extract-opt {|key|
	each {|o|
		u:with-key $o $key {|v|
			put $v
		}
	}
}

fn conf-container-opts {|conf|
	if (and (has-key $conf 'bind-toolchain') (not-eq $conf['bind-toolchain'] $nil)) {
		var prefix = (u:default-conf-value $conf['bind-toolchain'] 'prefix' '/usr')
		var tuple = $conf['bind-toolchain']['tuple']

		put [&ro-bind= [$prefix/$tuple '/toolchain'/$tuple]]
		put [&ro-bind= [$prefix/'lib/gcc' '/toolchain/lib/gcc']]
		put [&ro-bind= [$prefix/'lib/gcc'/$tuple '/usr/lib'/$tuple]]

		var exec-prefix = (
			if (os:is-dir &follow-symlink=$true $prefix/'libexec') {
				put $prefix/'libexec/gcc'
			} else {
				put $prefix/'lib/gcc'
			}
		)
		put [&ro-bind= [$exec-prefix '/toolchain/libexec/gcc']]
		put [&ro-bind= [$prefix/'share' '/toolchain/share']]

		put [&tmpfs= '/toolchain/bin']

		put $prefix/'bin'/$tuple'-'* | each {|f|
			var base-f = (path:base $f)

			if (==s (os:stat $f)['type'] 'symlink') {
				put [&symlink= [(e:readlink $f | one) '/toolchain/bin'/$base-f]]
			} else {
				put [&ro-bind= [$f '/toolchain/bin'/$base-f]]
			}
			#put [&symlink= [$base-f '/toolchain/bin'/(str:trim-prefix $base-f $tuple'-')]]
		}

		u:with-key $conf['bind-toolchain'] 'tools' {|tools|
			for t $tools {
				put [&ro-bind= [$prefix'/bin/'$t '/toolchain/bin'/$t]]
			}
		}

		var gcc-cmd = (external $prefix/'bin/'$tuple'-gcc')
		var gcc-version = (u:parse-gcc-version ($gcc-cmd --version | slurp))

		#u:log { echo 'GCC version: '$gcc-version }

		put [&bin-path= ['/toolchain/bin']]
		put [&env-var= ['CHOST' $tuple]]
		put [&env-var= ['GCC_EXEC_PREFIX' '/toolchain/libexec/gcc'/$tuple/$gcc-version]]
		put [&env-var= ['CC' $tuple'-gcc --sysroot=/ -B/toolchain/libexec/gcc'/$tuple/$gcc-version]]
		put [&env-var= ['CXX' $tuple'-g++ --sysroot=/ -B/toolchain/libexec/gcc'/$tuple/$gcc-version]]
		put [&env-var= ['LD' $tuple'-ld --sysroot=/ -B/toolchain/libexec/gcc'/$tuple/$gcc-version]]
	}

	u:with-key $conf 'tmpfs' {|o|
		for d $o {
			put [&tmpfs= $d]
		}
	}

	u:with-key $conf 'binds' {|o|
		for bo $o {
			put [&bind= $bo]
		}
	}

	u:with-key $conf 'dev-binds' {|o|
		for bo $o {
			put [&dev-bind= $bo]
		}
	}

	u:with-key $conf 'ro-binds' {|o|
		for bo $o {
			put [&ro-bind= $bo]
		}
	}

	u:with-key $conf 'env-vars' {|o|
		u:map-loop $o {|name value|
			put [&env-var= [$name $value]]
		}
	}

	u:with-key $conf 'symlinks' {|o|
		u:map-loop $o {|source dest|
			put [&symlink= [$source $dest]]
		}
	}

	u:with-key $conf 'bin-path' {|o|
		put [&bin-path= $o]
	}

	u:with-key $conf 'cap-add' {|o|
		for v $o {
			put [&cap-add= $v]
		}
	}
	u:with-key $conf 'cap-drop' {|o|
		for v $o {
			put [&cap-drop= $v]
		}
	}

	u:with-key $conf 'seccomp-filters' {|o|
		for v $o {
			put [&seccomp-filter= $v]
		}
	}

	u:with-key $conf 'fs' {|o|
		all $o
	}
}

fn to-overlay-args {|dest &temp=$false &read-only=$false &work-dir=$nil &rw-src=$nil|
	var paths = [(all)]

	if (u:empty $paths) {
		if $temp {
			put --tmpfs $dest
		} elif (not-eq $rw-src $nil) {
			put --bind $rw-src $dest
		}

		if $read-only {
			put --remount-ro $dest
		}
	} elif (and (not $temp) (== (count $paths) 1)) {
		if $read-only {
			put --ro-bind $paths[0] $dest
		} else {
			put --bind $paths[0] $dest
		}
	} else {
		for src $paths { put --overlay-src $src }

		if $read-only {
			put --ro-overlay $dest
		} elif $temp {
			put --tmp-overlay $dest
		} else {
			put --overlay $rw-src $work-dir $dest
		}
	}
}

fn container-opts-to-bwrap-args {|container &dest=''|
	var container-path = ($container[clean-path])
	var bin-path = []

	fn bind-opt {|o|
		if (==s (kind-of $o) 'list') {
			var cp = (u:clean-path $o[0])
			put $cp

			if (has-key $o 1) {
				put $dest(resolve-path $container-path $o[1])
			} else {
				put $dest(resolve-path $container-path $o[0])
			}
		} else {
			var cp = (u:clean-path $o)
			put $cp $dest(resolve-path $container-path $o)
		}
	}

	fn overlay-src {|o|
		var if-exists = (u:has-key-and-value $o 'if-exists' $true)

		if (u:has-key-and-value $o 'internal' $true) {
			for src $o['src'] {
				$container[parents] | each {|c|
					var cp = ($c[path])/'files/root'$src
					if (and $if-exists (not (os:exists &follow-symlink=$true $cp))) { continue }

					u:clean-path $cp
				}
			}
		} else {
			for src $o['src'] {
				if (and $if-exists (not (os:exists &follow-symlink=$true $src))) { continue }
				u:clean-path $src
			}
		}
	}

	each {|opt|
		u:with-key $opt 'bind' {|o|
			put [--bind (bind-opt $o)]
		}
		u:with-key $opt 'bind-try' {|o|
			put [--bind-try (bind-opt $o)]
		}
		u:with-key $opt 'ro-bind' {|o|
			put [--ro-bind (bind-opt $o)]
		}
		u:with-key $opt 'ro-bind-try' {|o|
			put [--bind-try (bind-opt $o)]
		}
		u:with-key $opt 'dev-bind' {|o|
			put [--dev-bind (bind-opt $o)]
		}
		u:with-key $opt 'dev-bind-try' {|o|
			put [--dev-bind-try (bind-opt $o)]
		}
		u:with-key $opt 'env-var' {|o|
			put [--setenv $o[0] (u:default-conf-value $o 1 '')]
		}
		u:with-key $opt 'tmpfs' {|o|
			put [--tmpfs $dest(resolve-path $container-path $o)]
		}
		u:with-key $opt 'symlink' {|o|
			#os:remove-all $container-path/'files/root'$o[1]
			put [--symlink $o[0..2]]
		}
		u:with-key $opt 'bin-path' {|o|
			set bin-path = (conj $o $@bin-path)
		}
		u:with-key $opt 'container-root' {|o|
			$o['container'][-bind-root] ^
				&dest=$o['dest'] ^
				&read-only=(u:default-conf-value $o 'read-only' $false) ^
				&temp=(u:default-conf-value $o 'temp' $false)
		}
		u:with-key $opt 'dir' {|o|
			put [(
				u:with-key $o 'perms' {|perm|
					put --perms $perm
				}
				put --dir $dest(resolve-path $container-path $o['path'])
			)]
		}
		u:with-key $opt 'ro-overlay' {|o|
			var srcs = [(overlay-src $o)]
			var overlay-dest = $dest(resolve-path $container-path $o['dest'])

			if (u:empty $srcs) {
				put [
					--tmpfs $overlay-dest
					--remount-ro $overlay-dest
				]
			} elif (== (count $srcs) 1) {
				put [--ro-bind $srcs[0] $overlay-dest]
			} else {
				for src $srcs { put [--overlay-src $src] }
				put [--ro-overlay $overlay-dest]
			}
		}
		u:with-key $opt 'tmp-overlay' {|o|
			var srcs = [(overlay-src $o)]
			var overlay-dest = $dest(resolve-path $container-path $o['dest'])

			if (u:empty $srcs) {
				put [--tmpfs $overlay-dest]
			} else {
				for src $srcs { put [--overlay-src $src] }
				put [--tmp-overlay $overlay-dest]
			}
		}
		u:with-key $opt 'overlay' {|o|
			var srcs = [(overlay-src $o)]
			var overlay-dest = $dest(resolve-path $container-path $o['dest'])

			if (u:empty $srcs) {
				put [--bind $o['rw-src'] $overlay-dest]
			} else {
				for src $srcs { put [--overlay-src $src] }
				put [--overlay $o['rw-src'] $o['work-dir'] $overlay-dest]
			}
		}
		u:with-key $opt 'cap-add' {|o|
			put [--cap-add $o]
		}
		u:with-key $opt 'cap-drop' {|o|
			put [--cap-drop $o]
		}
	}

	if (u:not-empty $bin-path) {
		put [--setenv PATH (str:join $path:list-separator $bin-path)':/usr/local/sbin:/usr/local/bin:/usr/bin']
	}
}

fn ctor-container-base {|path &opts=[]|

	var container-init-cmd = (search-external dumb-init)
	var unshare-bwrap-cmd = (u:find-in-data-dirs 'pat-aur/scripts/unshare-bwrap')
	#var dlwrap-cmd = (u:find-in-data-dirs 'pat-aur/scripts/dlwrap')
	#var container-init-cmd = (u:find-in-data-dirs 'pat-aur/scripts/container-cmd-wrap')
	#var container-init-cmd = (search-external tini)
	#var container-init-cmd = (search-external pid1)

	fn ctor-container-layer {|parent-container path &arch=$nil &override-base-opts=$false &opts=[]|
		var container-base = (ctor-container-base $path &opts=$opts)
		var id = (+ ($parent-container[id]) 1)

		if (eq $arch $nil) {
			set arch = ($parent-container[arch])
		}

		u:concat-maps $container-base [
			&id= {|_| put $id }
			&ctor= {|this|
				e:mkdir -p ($this[path])/'files'/{'root' 'work'}
			}
			&root= {|_| $parent-container[root] }
			&parent= {|_| put $parent-container }
			&parents= {|this &include-self=$true| $parent-container[parents]; if $include-self { put $this } }
			&find= {|this path &follow-symlink=$false|
				var found = ($container-base[find] &follow-symlink=$follow-symlink $this $path)

				if (eq $found $nil) {
					$parent-container[find] &follow-symlink=$follow-symlink $path
				} else {
					put $found
				}
			}
			&find-all= {|this path &follow-symlink=$false &include-self=$true|
				$parent-container[find-all] &follow-symlink=$follow-symlink $path
				$container-base[find-all] &follow-symlink=$follow-symlink &include-self=$include-self $this $path
			}
			&save= {|this|
				$container-base[save] $this
				echo ($parent-container[path]) > ($this[path])/'overlay-on'
				echo $arch > ($this[path])/'arch'
			}
			&-bind-root= {|this &dest='' &read-only=$false &temp=$false &upper-most=$true|
				var path = ($this[clean-path])
				var root-path = ($parent-container[clean-path])

				$parent-container[-bind-root] &dest=$dest &read-only=$true &temp=$false &upper-most=$false

				put [--overlay-src $root-path/'files/root']
				if $upper-most {
					var resolved-dest = (resolve-path $path $dest)

					if (or $temp $read-only) {
						put [
							--overlay-src $path/'files/root'
							--tmp-overlay $resolved-dest
						]
					} else {
						put [--overlay $path/'files/root' $path/'files/work' $resolved-dest]
					}
				}
			}
			&-options= {|this|
				if (not $override-base-opts) {
					$parent-container[-options]
				}

				$container-base[-options] $this
			}
			&-bwrap-args-before= {|this &read-only=$false &dest=''|
				$parent-container[-bwrap-args-before] &read-only=$true &dest=$dest
				$container-base[-bwrap-args-before] $this &read-only=$read-only &dest=$dest
			}
			&-bwrap-args-after= {|this &dest='' &read-only=$false &temp=$false &upper-most=$true|
				$parent-container[-bwrap-args-after] &dest=$dest &read-only=$true &temp=$false &upper-most=$false
				$container-base[-bwrap-args-after] $this &dest=$dest &read-only=$read-only &temp=$temp &upper-most=$upper-most

				if (and $upper-most $read-only) {
					var resolved-dest = (resolve-path ($this[path]) $dest)
					put [--remount-ro $resolved-dest]
				}
			}
		]
	}

	fn pacman-sysroot {|this @args|
		var clean-path = (u:clean-path $path)
		var arch = ($this[arch])
		#u:echo-info 'Executing pacman in `'$clean-path'` with `'$arch'`.'

		#u:log { pprint (u:flatten ($this[-bind-root] &dest=$clean-path'/files/root')) }
		#u:log { pprint (u:flatten ($this[-bwrap-args] &dest=$clean-path'/files/root')) }

		e:unshare -r --map-auto ^
			-- bwrap ^
				--dev-bind '/' '/' ^
				(u:flatten ($this[-bind-root] &dest=$clean-path'/files/root')) ^
				(u:flatten ($this[-bwrap-args] &dest=$clean-path'/files/root')) ^
				-- pacman --arch $arch --sysroot $clean-path'/files/root' $@args
	}

	if (os:exists &follow-symlink=$true $path/'config') {
		set opts = [(
			conf-container-opts (u:scoped-lock &excl=$false $path/'lock' { $path/'config' } | from-json)
			all $opts
		)]
	}

	if (os:exists &follow-symlink=$true $path/'config.json') {
		set opts = [(
			conf-container-opts (u:scoped-lock &excl=$false $path/'lock' { from-json < $path/'config.json' })
			all $opts
		)]
	}

	put [
		&id= {|_| put 0 }
		&path= {|_| put $path }
		&clean-path= {|this| u:clean-path ($this[path]) }
		&arch= {|this| cat ($this[path])/'arch' | one }
		&root= {|_|}
		&-bind-root= {|_ &dest='' &read-only=$false &temp=$false &upper-most=$true|}
		&-options= {|_| all $opts }
		&-bwrap-args-before= {|_ &read-only=$false &dest=''| }
		&-bwrap-args-after= {|_ &dest='' &read-only=$false &temp=$false &upper-most=$true|}
		&home= {|_| u:env-or-default 'HOME' '/home/pat-aur-user' }
		&parents= {|this &include-self=$true| if $include-self { put $this } }
		&find= {|this path &follow-symlink=$false|
			var in-ct-path = ($this[path])/'files/root'$path

			if (os:exists &follow-symlink=$follow-symlink $in-ct-path) {
				put $in-ct-path
			} else {
				put $nil
			}
		}
		&find-all= {|this path &follow-symlink=$false &include-self=$true|
			if $include-self {
				var in-ct-path = ($this[path])/'files/root'$path

				if (os:exists &follow-symlink=$follow-symlink $in-ct-path) {
					put $in-ct-path
				}
			}
		}
		&exec= {|this cmd @args
				&read-only=$false
				&temp=$false
				&opts=[]
				&work-dir=$nil
				&as-root=$false
				&gui=$false
				&bind-session-bus=$false
				&override-opts=$false
				&use-seccomp-filters=$true
				&lock-file=$nil
				&dest=''|
			var path = ($this[path])

			#var arch = ($this[arch])
			#var other-args = [(
			#	if (u:not-empty $emulator) {
			#		echo emulator['bin-format'] >&12
			#		put [--ro-bind (u:clean-path (search-external $emulator['cmd'][0])) '/emulator.bin']
			#		set emulator['cmd'][0] = '/emulator.bin'
			#	}
			#)]
			#u:log { pprint (u:flatten $@opt-args-bwrap) }
			#u:log { pprint $other-args }

			var other-args = [({
				if (has-env 'SSH_AUTH_SOCK') {
					put [
						&bind= [$E:SSH_AUTH_SOCK '/run/host_ssh_auth_socket']
						&env-var= ['SSH_AUTH_SOCK' '/run/host_ssh_auth_socket']
					]
				}
				if $as-root {
					put [&env-var= ['HOME' '/root']]
				} else {
					put [&env-var= ['HOME' ($this[home])]]
					if (has-env 'USER') {
						put [&env-var= ['USER' $E:USER]]
					}
				}

				var container-runtime-dir = '/run/user-runtime'

				put [
					&env-var= ['XDG_RUNTIME_DIR' $container-runtime-dir]
					&dir= [&path= $container-runtime-dir &perms= '700']
				]
				put [&env-var= ['LANG' 'en_US.UTF-8']]

				if $gui {
					var runtime-dir = (u:xdg-runtime-dir)

					if (has-env 'WAYLAND_DISPLAY') {
						put [&bind= [$runtime-dir/$E:WAYLAND_DISPLAY'.lock' $container-runtime-dir'/wayland-0.lock']]
						put [
							&bind= [$runtime-dir/$E:WAYLAND_DISPLAY $container-runtime-dir'/wayland-0']
							&env-var= ['WAYLAND_DISPLAY' 'wayland-0']
						]
					}

					if (has-env 'XAUTHORITY') {
						put [
							&bind= [$E:XAUTHORITY $container-runtime-dir'/xauth']
							&env-var= ['XAUTHORITY' $container-runtime-dir'/xauth']
						]
					}

					if (has-env 'DISPLAY') {
						put [
							&bind= '/tmp/.X11-unix'
							&env-var= ['DISPLAY' $E:DISPLAY]
						]
					}

					if (and $bind-session-bus (has-env 'DBUS_SESSION_BUS_ADDRESS')) {
						re:find &max=1 '^unix:path=(.+)' $E:DBUS_SESSION_BUS_ADDRESS | each {|addr|
							set addr = $addr['groups'][1]['text']
							var dest = $container-runtime-dir/(path:base $addr)
							put [
								&bind= [$addr $dest]
								&env-var= ['DBUS_SESSION_BUS_ADDRESS' 'unix:path='$dest]
							]
						}
					}

					if (os:exists &follow-symlink=$true $runtime-dir/'at-spi') {
						put [&bind= $runtime-dir/'at-spi']
					}

					if (os:exists &follow-symlink=$true $runtime-dir/'pipewire-0') {
						put [&bind= [$runtime-dir/'pipewire-0' $container-runtime-dir'/pipewire-0']]
					}

					if (os:exists &follow-symlink=$true $runtime-dir/'pulse') {
						put [&bind= [$runtime-dir/'pulse' $container-runtime-dir'/pulse']]
					}

					put [&dev-bind-try= '/dev/dri']
				}
			} | container-opts-to-bwrap-args $this &dest=$dest)]

			if (not $override-opts) {
				set opts = (conj [($this[-options])] $@opts)
			}

			var opt-args-bwrap = [(all $opts | container-opts-to-bwrap-args $this &dest=$dest)]
			#u:log { pprint $opt-args-bwrap }

			var temp-seccomp-file

			{
				var seccomp-filters = [(
					if $use-seccomp-filters {
						all $opts | extract-opt 'seccomp-filter'
					}
				)]

				#u:log { pprint $opts }
				#u:log { pprint $seccomp-filters }

				if (u:not-empty $seccomp-filters) {
					set temp-seccomp-file = (u:create-temp-file 'exec-seccomp-XXXXXXXXXX')

					if (u:empty $seccomp-filters) {
						print ''
					} else {
						for f $seccomp-filters {
							print (put [&0= $f] | to-json)"\x00"
						}
					} > $temp-seccomp-file
				}
			}

			defer {
				if (not-eq $temp-seccomp-file $nil) {
					os:remove $temp-seccomp-file
				}
			}

			u:execute (
				if (not-eq $lock-file $nil) {
					put flock -x $lock-file
				}

				put $unshare-bwrap-cmd (u:empty-str-if-nil $temp-seccomp-file) (u:select $as-root '1' '0') ^
					--unshare-uts --unshare-pid --unshare-ipc ^
					--clearenv ^
					(u:flatten ($this[-bind-root] &dest=$dest &read-only=$read-only &temp=$temp)) ^
					(u:flatten ($this[-bwrap-args-before] &dest=$dest &read-only=$read-only)) ^
					(if (not-eq $work-dir $nil) { put --chdir $work-dir }) ^
					(u:flatten $@opt-args-bwrap) ^
					(u:flatten $@other-args) ^
					(if (==s $dest '') { put --ro-bind $container-init-cmd '/init' }) ^
					(u:flatten ($this[-bwrap-args-after] &dest=$dest &read-only=$read-only &temp=$temp)) ^
					--as-pid-1 ^
					-- (u:select (==s $dest '') '/init' $container-init-cmd) $cmd $@args
					#--ro-bind $dlwrap-cmd '/usr/bin/pat-aur-dlwrap' ^
					#--ro-bind $container-init-cmd '/init' ^
					#-- '/init' $cmd $@args
					#-- '/init' -p SIGTERM -g -- $cmd $@args
			)
		}
		&bash-login-exec= {|this cmd @args
				&read-only=$false
				&temp=$false
				&opts=[]
				&work-dir=$nil
				&as-root=$false
				&gui=$false
				&bind-session-bus=$false
				&override-opts=$false
				&use-seccomp-filters=$true
				&lock-file=$nil
				&dest=''|
			$this[exec] (u:bash-cmd $cmd $@args) ^
				&read-only=$read-only &temp=$temp &opts=$opts &work-dir=$work-dir &as-root=$as-root &gui=$gui &bind-session-bus=$bind-session-bus ^
				&override-opts=$override-opts &use-seccomp-filters=$use-seccomp-filters &lock-file=$lock-file &dest=$dest
		}
		&layer= {|this layer-path &arch=$nil &override-base-opts=$false &opts=[]|
			u:make (ctor-container-layer $this $layer-path &arch=$arch &override-base-opts=$override-base-opts &opts=$opts)
		}
		&save= {|_|}
		&delete= {|this|
			remove-container-files -rf ($this[path])/'files'
		}
		&remove-files= {|this &r=$false &f=$false @files|
			remove-container-files -(u:select $r 'r' '')(u:select $f 'f' '') ($this[path])/'files/root'$@files
		}
		&update= {|this @pkgs &noconfirm=$false &allow-downgrades=$false &opts=[]|
			$this[exec] &lock-file=(($this[root])[path])/'pacman-lock' &use-seccomp-filters=$false &as-root=$true &opts=$opts pacman -Syu (if $noconfirm { put '--noconfirm' }) (if $allow-downgrades { put '-u' }) -- $@pkgs
			#pacman-sysroot $this -Syuu --noconfirm
		}
		&install= {|this @pkgs &noconfirm=$false &needed=$false &refresh=$false &opts=[]|
			$this[exec] &lock-file=(($this[root])[path])/'pacman-lock' &use-seccomp-filters=$false &as-root=$true &opts=$opts pacman -S ^
				(if $noconfirm { put '--noconfirm' }) ^
				(if $needed { put '--needed' }) ^
				(if $refresh { put '-y' }) ^
				-- $@pkgs
			#pacman-sysroot $this -Sy --noconfirm $@pkgs
		}
		&install-files= {|this @pkgs &noconfirm=$false &needed=$false &opts=[]|
			$this[exec] &lock-file=(($this[root])[path])/'pacman-lock' &use-seccomp-filters=$false &as-root=$true &opts=$opts pacman -U (if $noconfirm { put '--noconfirm' }) (if $needed { put '--needed' }) -- $@pkgs
			#pacman-sysroot $this -U --noconfirm $@pkgs
		}
		&uninstall= {|this @pkgs &opts=[] &noconfirm=$false|
			$this[exec] &lock-file=(($this[root])[path])/'pacman-lock' &use-seccomp-filters=$false &as-root=$true &opts=$opts pacman -R (if $noconfirm { put '--noconfirm' }) -- $@pkgs
			#pacman-sysroot $this -R --noconfirm $@pkgs
		}
		&pacman= {|this @args &as-root=$false &opts=[]|
			#pacman-sysroot $this $@args
			$this[exec] &use-seccomp-filters=$false &as-root=$as-root &opts=$opts pacman $@args
		}
		&pacman-key= {|this @args|
			var path = ($this[path])
			e:unshare -r --map-auto -- ^
				bwrap ^
					--dev-bind '/' '/' ^
					--bind $path/'files/root/usr/share/pacman/keyrings' '/usr/share/pacman/keyrings' ^
					-- pacman-key --config $path/'files/root/etc/pacman.conf' --gpgdir $path/'files/root/etc/pacman.d/gnupg' $@args
		}
		&ready= {|this|
			os:is-dir ($this[path])/'files'
		}
	]
}

fn gen-locale {|locale charset prefix|
	e:localedef --prefix=$prefix -i $locale -c -f $charset -A '/usr/share/locale/locale.alias' $locale.$charset
}

var bootstrappers = [
	&x86_64= {|path pacman-conf pkgs|
		u:echo-title 'Bootstrapped root container.'

		os:mkdir $path/'files/root/etc'

		cat $pacman-conf | u:substitute-in-pacman-conf '@local-repo@' '' | to-lines > $path/'files/root/etc/pacman.conf'

		e:pacstrap -NMG -C $path/'files/root/etc/pacman.conf' $path/'files/root/' ^
			base-devel $@pkgs

		fn pm-key {|@args|
			e:unshare -r --map-auto pacman-key --config $path/'files/root/etc/pacman.conf' --gpgdir $path/'files/root/etc/pacman.d/gnupg' $@args
		}

		pm-key --init
		pm-key --populate 'archlinux'

		e:systemd-machine-id-setup --root=$path/'files/root/'

		to-lines [{en_US de_DE}'.UTF-8 UTF-8'] > $path/'files/root/etc/locale.gen'
		#$this[exec] &as-root=$true locale-gen
		gen-locale 'en_US' 'UTF-8' $path/'files/root'
		gen-locale 'de_DE' 'UTF-8' $path/'files/root'

		os:remove-all $path/'files/root/var/cache/pacman'

		var username = (u:env-or-default 'USER' 'pat-aur-user')
		var uid = (e:id -u | one)
		var gid = (e:id -g | one)
		var home = (u:env-or-default 'HOME' '/home/pat-aur-user')
		echo $username':x:'$gid':' >> $path/'files/root/etc/group'
		echo $username':x:'$uid':'$gid'::'$home':/usr/bin/bash' >> $path/'files/root/etc/passwd'
		echo $username':!!:'(/ (e:date -u +%s) 86400)'::::::' >> $path/'files/root/etc/shadow'
	}
]

fn ctor-container-root {|path &opts=[]|
	var container-base = (ctor-container-base $path &opts=$opts)

	u:concat-maps $container-base [
		&root= {|this| put $this }

		&bootstrap= {|this pacman-conf &arch=$nil &pkgs=[]|
			var path = ($this[clean-path])

			if (not (os:is-dir $path/'files')) {
				os:mkdir-all $path/'files/root'

				try {
					set arch = (u:default-if-nil $arch (u:host-arch))

					if (not (has-key $bootstrappers $arch)) {
						fail 'No bootstrapper found for arch `'$arch'`.'
					}

					u:log { $bootstrappers[$arch] $path $pacman-conf $pkgs }
					echo $arch > $path/'arch'
				} catch e {
					$this[delete]
					fail $e
				}

			} else {
				$this[update] &noconfirm=$true &allow-downgrades=$true
			}
		}
		&-bind-root= {|this &dest='' &read-only=$false &temp=$false &upper-most=$true|
			if (!=s $dest '') {
				put [--dev-bind / /]
			}

			if $upper-most {
				var resolved-dest = (resolve-path ($this[path]) $dest)

				if $temp {
					put [
						--overlay-src ($this[clean-path])/'files/root'
						--tmp-overlay $resolved-dest
					]
				} else {
					put [--(u:select $read-only 'ro-bind' 'bind') ($this[clean-path])/'files/root' $resolved-dest]
				}
			}
		}
		&-bwrap-args-before= {|this &read-only=$false &dest=''|
			var path = ($this[path])

			all [
				[&tmpfs= '/tmp']
				[&tmpfs= '/var/tmp']
				[&tmpfs= '/run']
				[&ro-bind-try= ['/etc/resolv.conf' '/etc/resolv.conf']]
				[&bind= '/sys']
			] | container-opts-to-bwrap-args $this &dest=$dest

			put [
				--proc $dest'/proc'
				--dev $dest'/dev'
				--mqueue $dest'/dev/mqueue'
			]

			$container-base[-bwrap-args-before] $this &read-only=$true &dest=$dest

			#u:ignore-errors {
			#	u:silence {
			#		u:get-local-repos-info &pm-conf=$path/'files/root/etc/pacman.conf' ^
			#			| bind-local-repos | container-opts-to-bwrap-args $this &dest=$dest
			#	}
			#}
		}
		&-bwrap-args-after= {|this &dest='' &read-only=$false &temp=$false &upper-most=$true|
			$container-base[-bwrap-args-after] $this &dest=$dest &read-only=$read-only &temp=$temp &upper-most=$upper-most

			if (and $upper-most $read-only) {
				var resolved-dest = (resolve-path ($this[path]) $dest)
				put [--remount-ro $resolved-dest]
			}
		}
	]
}

fn maybe-bootstrap {|root-path container arch-config &opts=[]|
	os:mkdir-all $root-path

	if ($container[lock] &excl=$false {|c| not ($c[ready]) }) {
		$container[lock] {|c| u:log { $c[bootstrap] $arch-config['pacman-conf'] &arch=$arch-config['arch'] &pkgs=$arch-config['packages'] } }
	}
}

fn setup-container-layers {|root-path layers fc &bootstrap=$true &arch-config=$nil &opts=[]|
	u:scoped (u:make (ctor-container-root &opts=$opts $root-path)) {|container|
		u:scoped (u:make (u:ctor-synchronized $root-path/'lock' $container)) {|container|
			if $bootstrap {
				maybe-bootstrap $root-path $container $arch-config
			}

			if (u:empty $layers) {
				$fc $container $root-path
			} else {
				fn create-layer {|parent @paths|
					u:scoped ($parent[layer] $paths[0]['path'] &arch=$paths[0]['arch']) {|new-layer|
						u:scoped (u:make (u:ctor-synchronized $paths[0]['path']/'lock' $new-layer)) {|new-layer|
							#u:echo-info 'Created layer in `'$paths[0]['path']'` with parent `'($parent[path])'`'

							if (== (count $paths) 1) {
								$fc $new-layer $paths[0]['path']
							} else {
								#u:echo-info 'Locking container at `'$paths[0]['path']'`'
								$new-layer[lock] &excl=$false {|new-layer|
									create-layer $new-layer (all $paths[1..])
								}
							}
						}
					}
				}

				#u:echo-info 'Locking container at `'$root-path'`'
				$container[lock] &excl=$false {|container|
					create-layer $container $@layers
				}
			}
		}
	}
}

fn load-container {|path fc &bootstrap=$true &arch-config=$nil &read-arch-config-opts=$true &opts=[]|
	fn load-layers {|path|
		u:fail-if (and (not $bootstrap) (not (os:is-dir &follow-symlink=$true $path/'files'))) 'Container path `'$path'` does not exists.'

		if (os:exists $path/'overlay-on') {
			load-layers (cat $path/'overlay-on' | one)
		}

		put [
			&path= $path
			&arch=(
				if (os:is-regular $path/'arch') {
					cat $path/'arch' | one
				} else {
					put $arch-config['arch']
				}
			)
		]
	}

	if $read-arch-config-opts {
		set opts = (conj $opts (conf-container-opts $arch-config))
	}

	var layers = [(load-layers $path)]
	#u:log { pprint $layers }
	setup-container-layers ^
			&bootstrap=$bootstrap &arch-config=$arch-config &opts=$opts ^
		$layers[0]['path'] $layers[1..] $fc
}
