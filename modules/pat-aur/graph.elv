use str
use os
use re
use runtime

use ./utils u
use ./config c

fn remove-version {|pkgname|
	re:find '^([^<=>\s]+)' $pkgname | each {|m| put $m['groups'][1]['text'] }
}

fn parse-depstring {|depstring|
	var mod-pos-start = (str:index-any $depstring '<=>')

	if (!= $mod-pos-start -1) {
		put $depstring[..$mod-pos-start]

		var len = (count $depstring)
		var i = (+ $mod-pos-start 1)
		while (< $i $len) {
			if (not (has-value ['<' '=' '>'] $depstring[$i])) {
				put $depstring[$mod-pos-start..$i]
				put $depstring[$i..]
				break
			}
			set i = (+ $i 1)
		}
	} else {
		put $depstring
	}
}

fn dep-satisfied {|dep-ver mod prov-ver|
	var cmp = (e:vercmp $prov-ver $dep-ver | one)

	if (==s $mod '=') {
		eq $cmp 0
	} elif (==s $mod '>') {
		> $cmp 0
	} elif (==s $mod '>=') {
		>= $cmp 0
	} elif (==s $mod '<') {
		< $cmp 0
	} elif (==s $mod '<=') {
		<= $cmp 0
	}
}

fn aur-depends {|@rest
		&include-self=$true
		&include-depends=$true
		&include-make-depends=$true
		&include-check-depends=$true
		&include-opt-depends=$false
		&aur-location=$nil|
	#u:log { pprint $rest }

	var res = ({
		var opts = [
			(
				if (not $include-make-depends) {
					put '--no-makedepends'
				}
				if (not $include-depends) {
					put '--no-depends'
				}
				if (not $include-check-depends) {
					put '--no-checkdepends'
				}
				if $include-opt-depends {
					put '--optdepends'
				}
			)

			--json --no-provides $@rest
		]
		#u:log { pprint $opts }

		var output = (u:ignore-errors { e:env (c:define-aur-location (c:select-aur-location $aur-location)) aur depends $@opts | slurp })
		if (or (u:empty $output) (eq $output $nil)) {
			set output = '{}'
		}
		print $output

	} | from-json)

	fn has-required-by-not-self {|p|
		u:map-loop $p['RequiredBy'] {|pkgname type|
			if (!=s $type 'Self') {
				put $true
				return
			}
		}

		put $false
	}

	u:map-loop $res {|pkgname p|
		if (or $include-self (has-required-by-not-self $p)) { put $p }
	}
}

fn translate-aur-results {|&aur-blacklist=[&] &remove-version=$true|
	peach {|p|
		if (or (not (has-key $p 'PackageBase')) (has-key $aur-blacklist $p['Name'])) {
			put [
				&name= $p['Name']
				&depended-by= [(
					u:with-key $p 'RequiredBy' {|rby|
						u:map-loop $rby {|p type|
							put [$p $type]
						}
					}
				)]
				&provides= []
			]
			continue
		}

		put [
			&name= $p['Name']
			&pkgbase= $p['PackageBase']
			&version= $p['Version']
			&provides= [(
				u:with-key $p 'Provides' {|pr|
					for d $pr {
						if $remove-version {
							remove-version $d
						} else { put $d }
					}
				} | each (u:filter-out [$p['Name']]) | order | compact
			)]
			&depended-by= [(
				u:with-key $p 'RequiredBy' {|rby|
					u:map-loop $rby {|p type|
						if (!=s $type 'Self') {
							put [$p $type]
						}
					}
				}
			)]
			&depends= [(
				u:with-key $p 'Depends' {|dps|
					for d $dps {
						if $remove-version {
							remove-version $d
						} else { put $d }
					}
				}
			)]
			&check-depends= [(
				u:with-key $p 'CheckDepends' {|cdps|
					for d $cdps {
						if $remove-version {
							remove-version $d
						} else { put $d }
					}
				}
			)]
			&make-depends= [(
				u:with-key $p 'MakeDepends' {|mdps|
					for d $mdps {
						if $remove-version {
							remove-version $d
						} else { put $d }
					}
				}
			)]
		]
	}
}

fn aur-query-info {|@args &aur-location=$nil &remove-version=$true|
	var res = (to-lines | e:env (c:define-aur-location (c:select-aur-location $aur-location)) aur query -t info - $@args | from-json)

	all $res['results'] | translate-aur-results &remove-version=$remove-version
}

fn aur-search-packages {|&aur-location=$nil|
	var output = (u:ignore-errors { e:env (c:define-aur-location (c:select-aur-location $aur-location)) aur search --json -i -a -n (all) | slurp })
	if (and (not-eq $output $nil) (u:not-empty $output)) {
		all (print $output | from-json)
	}
}

fn aur-pkglist {|&aur-location=$nil|
	e:env (c:define-aur-location (c:select-aur-location $aur-location)) aur pkglist | from-lines
}

fn query-aur-dependencies {|
		&global-providers-map=[&]
		&pkg-config-map=[&]
		&protect=[&]
		&assume-installed=[]
		&include-self=$true
		&include-depends=$true
		&include-make-depends=$true
		&include-check-depends=$true
		&include-opt-depends=$false
		&aur-location=$nil
		&aur-blacklist=[&]|

	fn aur-depends-impl {|@args|
		aur-depends ^
			&include-self=$include-self ^
			&include-depends=$include-depends ^
			&include-make-depends=$include-make-depends ^
			&include-check-depends=$include-check-depends ^
			&include-opt-depends=$include-opt-depends ^
			&aur-location=$aur-location ^
			--assume-installed (str:join ',' $assume-installed) $@args

		set include-self = $true
	}

	fn get-deps-impl {|deps|
		var providers-list = $deps
		var resolved-deps = [( for d $deps { put [&Name= $d] } )]
		var protected-list = $protect
		var add-pkgs-done = [&]

		fn check-providers {
			var changed = $false

			for d $resolved-deps {
				if (and (not (has-key $protected-list $d['Name'])) (has-key $global-providers-map $d['Name'])) {
					set protected-list[$d['Name']] = $nil
					set changed = $true

					var mapping = $global-providers-map[$d['Name']]
					u:echo-info 'provider found: '$d['Name']' -> '(print $mapping)

					set providers-list = (conj $providers-list (all $mapping))
				}

				u:with-key $d 'PackageBase' {|pkgbase|
					if (not (has-key $add-pkgs-done $pkgbase)) {
						set add-pkgs-done[$pkgbase] = $nil

						u:with-keys $pkg-config-map [$pkgbase 'make-depends'] {|mds|
							set changed = $true

							u:echo-info 'additional make depends for `'$pkgbase'` found: '(print $mds)
							set providers-list = (conj $providers-list (all $mds))
						}

						u:with-keys $pkg-config-map [$pkgbase 'providers-map'] {|mds|
							set changed = $true
							u:map-loop $mds {|k list|
								u:echo-info 'Dependency mappings for `'$pkgbase'` found: '(print $list)
								set providers-list = (conj $providers-list (all $list))
							}
						}
					}
				}
			}

			put $changed
		}

		check-providers | u:discard
		if (> (count $providers-list) 0) {
			set resolved-deps = [(aur-depends-impl -a $@providers-list)]
		}

		while (check-providers) {
			set resolved-deps = [(aur-depends-impl -a $@providers-list)]
		}

		all $resolved-deps
	}

	get-deps-impl [(all)] | translate-aur-results &aur-blacklist=$aur-blacklist
}

#fn get-arch-dependencies {|
#		&global-providers-map=[&]
#		&pkg-providers-map= [&]
#		&protect=[]
#		&pm-conf=$nil
#		&syncdb=$nil|
#
#	each {|p|
#		e:pactree ^
#			(if (not-eq $pm-conf $nil) { put '--config' $pm-conf }) ^
#			(if (not-eq $syncdb $nil) { put '--dbpath' $syncdb }) ^
#			-u $p
#	}
#}

fn init-build-graph {|requested-pkgs target-id
		&prefix=''
		&md-target-id=$nil|

	if (eq $md-target-id $nil) {
		set md-target-id = $target-id
	}

	var pkgbases = [&]
	var pkgnames-by-base = [&]

	var graph = [(each {|p|
		if (has-key $p 'pkgbase') {
			set pkgnames-by-base[$p['pkgbase']] = (u:append-to-list (u:default-conf-value $pkgnames-by-base $p['pkgbase'] $nil) $p['name'])
		}
		put $p
	})]

	all $graph | each {|p|
		if (not (has-key $p 'pkgbase')) {
			put [
				&type= 'phony'
				&name= $p['name']
				&target-id= $target-id
				&prefix= $prefix
				&provides= $p['provides']
			]
			continue
		}

		if (not (has-key $pkgbases $p['pkgbase'])) {
			set pkgbases[$p['pkgbase']] = $nil

			put [
				&type= 'pkgbase'
				&name= $p['pkgbase']
				&target-id= $target-id
				#&build-params= $build-params
				&prefix= $prefix
				&ignore-arch= $false
				&make-depends= [(
					for d $p['make-depends'] {
						put [&name= $d &target-id= $md-target-id]
					}
				)]
				&check-depends= [(
					for d $p['check-depends'] {
						put [&name= $d &target-id= $target-id]
					}
				)]
				&pkgnames= $pkgnames-by-base[$p['pkgbase']]
				&version= $p['version']
			]
		}

		var requested try force install = (u:with-key $requested-pkgs [$target-id $p['name']] {|rp|
			put $true $rp['try'] $rp['forced'] $rp['install']
		} &or-else={ put $false $false $false $false })

		put [
			&type= 'pkg'
			&name= $p['name']
			&pkgbase= $p['pkgbase']
			&depends= [(
				for d $p['depends'] {
					put [&name= $d &target-id= $target-id]
				}
			)]
			&make-depends= [(
				for d $p['make-depends'] {
					put [&name= $d &target-id= $md-target-id]
				}
			)]
			&check-depends= [(
				for d $p['check-depends'] {
					put [&name= $d &target-id= $target-id]
				}
			)]
			&depended-by= $p['depended-by']
			&provides= $p['provides']
			&target-id= $target-id
			&for= [(if $requested { put $target-id })]
			&try= $try
			&forced= $force
			&install= [(if $install { put $target-id })]
			&prefix= $prefix
		]
	}
}

fn detect-pkgver {|container dir &opts=[]|
	var detect-pkgver-cmd = (u:find-in-data-dirs 'pat-aur/scripts/detect-pkgbuild-pkgver')

	# Run in temporary container for extra security.
	u:silence {
		$container[bash-login-exec] &temp=$true &as-root=$false &opts=[(
				all $opts
				put [&env-var= ['CARCH' ($container[arch])]]
				put [&bind= [$dir '/pkgbuild/pkg']]
				put [&ro-bind= [$detect-pkgver-cmd '/pkgbuild/detect-pkgver']]
			)] ^
			'/pkgbuild/detect-pkgver' '/pkgbuild/pkg'
	}
}

fn printsrcinfo {|container makepkg-conf dir &opts=[] &lock-file=$nil|
	# Run in temporary container for extra security.
	$container[bash-login-exec] &lock-file=$lock-file &work-dir='/pkgbuild/pkg' &temp=$true &as-root=$false &opts=[(
			all $opts
			put [&env-var= ['CARCH' ($container[arch])]]
			put [&bind= [$dir '/pkgbuild/pkg']]
			put [&ro-bind= [$makepkg-conf '/etc/makepkg.conf']]
		)] ^
		makepkg --printsrcinfo
}

fn is-provided {|build-dir pacman-conf pkgname pkgver target-id|
	u:silence { e:pacsift --dbpath=$build-dir/'local-db-'$target-id --config=$pacman-conf ^
		--sync --exact --exists --satisfies=$pkgname'>='$pkgver --name=$pkgname <&- }
}

fn early-skip-check {|build-params|
	peach {|pkg|
		if (==s $pkg['type'] 'pkgbase') {
			var skip = $false

			if (and (not $pkg['try']) (not $pkg['forced'])) {
				set skip = $true

				for pn $pkg['pkgnames'] {
					#u:echo-info 'Checking `'$pn'` '$pkg['version']
					var bp = $build-params[[&target-id= $pkg['target-id'] &host-id= $pkg['host-id']]]
					if (not ?(is-provided $bp['build-dir'] $bp['target-pacman-conf'] $pn $pkg['version'] $pkg['target-id'])) {
						set skip = $false
						#u:echo-info '`'$pn'` not provided.'
						break
					}
				}

				#if $skip {
				#	u:echo-info $pkg['name']' skipped, already built.'
				#}
			}

			set pkg['skip'] = $skip
		}

		put $pkg
	} | each {|p|
		if (and (==s $p['type'] 'pkgbase') $p['skip']) {
			u:echo-info 'Package base `'$p['name']'@'$p['target-id']'` skipped, already built.'
		}
		put $p
	}
}

fn remove-skipped {
	var skipped = [&]

	var graph = [(each {|p|
		if (and (==s $p['type'] 'pkgbase') $p['skip']) {
			set skipped[[$p['target-id'] $p['name']]] = $nil
		} else {
			put $p
		}
	})]

	all $graph | peach {|p|
		if (or (!=s $p['type'] 'pkg') (not (has-key $skipped [$p['target-id'] $p['pkgbase']]))) {
			put $p
		}
	}
}

fn maybe-run-check {|pkg &check=$false &check-force=$nil|
	if (not-eq $check-force $nil) {
		put $check-force
	} else {
		u:with-keys $c:conf['pkg-config'] [$pkg['name'] 'check'] {|c|
			put $c
		} &or-else= {
			put $check
		}
	}
}

fn remap-depends {|targets-desc|
	peach {|pkg|
		fn remap-key {|key pkg-providers t-conf|
			if (has-key $pkg $key) {
				set pkg[$key] = [(
					for d $pkg[$key] {
						fn do-remap {|remapped|
							u:echo-info $pkg['type']' '$pkg['name']'@'$pkg['target-id']' '$key' dependency remapped: `'$d['name']'@'$d['target-id']'` -> '(print $remapped)
							set d['name'] = $remapped[0]
							drop 1 $remapped | each {|rd|
								put [&name= $rd &target-id= $d['target-id']]
							}
						}

						if (has-key $pkg-providers $d['name']) {
							do-remap $pkg-providers[$d['name']]
						} elif (has-key $t-conf['providers-map'] $d['name']) {
							do-remap $t-conf['providers-map'][$d['name']]
						}

						put $d
					}
				)]
			}
		}

		if (==s $pkg['type'] 'pkg') {
			var t-conf = $targets-desc[$pkg['target-id']]['target-config']
			var providers = (
				u:with-keys $t-conf['pkg-config'] [$pkg['pkgbase'] 'providers-map'] {|providers|
					put $providers
				} &or-else= {
					put [&]
				}
			)

			remap-key 'depends' $providers $t-conf
			remap-key 'make-depends' $providers $t-conf
			remap-key 'check-depends' $providers $t-conf
		} elif (==s $pkg['type'] 'pkgbase') {
			var t-conf = $targets-desc[$pkg['target-id']]['target-config']
			var providers = (u:concat-maps (
				u:with-keys $t-conf['pkg-config'] [$pkg['name'] 'providers-map'] {|providers|
					put $providers
				} &or-else= {
					put [&]
				}
			))

			remap-key 'depends' $providers $t-conf
			remap-key 'make-depends' $providers $t-conf
			remap-key 'check-depends' $providers $t-conf

			u:with-keys $t-conf['pkg-config'] [$pkg['name'] 'make-depends'] {|mds|
				set pkg['make-depends'] = [(
					all $pkg['make-depends']

					for md $mds {
						put [&name= $md &target-id= $pkg['target-id']]
					}
				)]
			}
		}

		put $pkg
	}
}

fn select-compile-flags {|pkg target-config|
	fn select-flags {|p key|
		if (or (not (has-key $target-config['pkg-config'] $p)) ^
		       (not (u:has-key-and-value $target-config['pkg-config'][$p] $key'-override' $true))) {
			all $target-config[$key]
		}
		if (and (has-key $target-config['pkg-config'] $p) (has-key $target-config['pkg-config'][$p] $key)) {
			all $target-config['pkg-config'][$p][$key]
		}
	}

	fn select-env {|p key|
		u:concat-maps $target-config[$key] (
			if (and (has-key $target-config['pkg-config'] $p) (has-key $target-config['pkg-config'][$p] $key)) {
				put $target-config['pkg-config'][$p][$key]
			}
		)
	}

	u:map-loop $c:build-flags {|flag _|
		set pkg[$flag'-flags'] = [(select-flags $pkg['name'] $flag'-flags')]
	}
	set pkg['env-vars'] = (select-env $pkg['name'] 'env-vars')

	put $pkg
}

fn fetch-aur {|targets-desc &skip-fetch=$false|
	var fetch-pkg-cmd = (external (u:find-in-data-dirs 'pat-aur/scripts/fetch-pkg'))
	var alter-pkg-cmd = (external (u:find-in-data-dirs 'pat-aur/scripts/alter-pkg'))

	peach &num-workers=$c:conf['parallel-downloads'] {|p|
		if (==s $p['type'] 'pkgbase') {
			var t-conf = $targets-desc[$p['target-id']]['target-config']

			if (has-key $p 'dir') {
				set p['lock-file'] = $nil
			} else {
				fn try-fetch {|aur-loc|
					var pkgbuilds-location = (
						if (eq $aur-loc $nil) {
							put $c:conf['pkgbuild-dir']/'aur'
						} else {
							put $c:conf['pkgbuild-dir']/'aur-'$aur-loc
						}
					)
					set aur-loc = (c:select-aur-location $aur-loc)

					set p['dir'] = $pkgbuilds-location/$p['name']
					set p['lock-file'] = $pkgbuilds-location/$p['name']'.~lock'

					if (not $skip-fetch) {
						os:mkdir-all $pkgbuilds-location
						u:log { $fetch-pkg-cmd $aur-loc['url'] $p['name'] $pkgbuilds-location $pid $p['lock-file'] }
					} elif (not (os:exists $p['dir'])) {
						fail 'Directory `'$p['dir']'` not found.'
					}
				}

				u:retry {
					u:all-of $t-conf['aur'] | u:try-until-success ^
						&proj={|x| try-fetch $x } ^
						&fail-msg='Failed to fetch sources for package `'$p['name']'`.'
				}
			}

			if (os:exists $p['dir']/'.pat-aur-last-head-'$pid) {
				set p['last-head'] = (one < $p['dir']/'.pat-aur-last-head-'$pid)
			} else {
				set p['last-head'] = 'HEAD^'
			}

			u:with-keys $t-conf['pkg-config'] [$p['name'] 'alter'] {|alter|
				u:log {
					var pkgbuilds-location = $c:conf['pkgbuild-dir']/$p['target-id']
					var new-dir = $pkgbuilds-location/$p['name']

					$alter-pkg-cmd $p['dir'] $new-dir (u:empty-str-if-nil $p['lock-file']) $runtime:elvish-path $alter

					set p['dir'] = $new-dir
					set p['lock-file'] = $pkgbuilds-location/$p['name']'.~lock'
				}
			}
		}

		put $p
	}
}

fn map-graph {|&type=$nil|
	if (eq $type $nil) {
		each {|p| put [(u:select-keys $p 'type' 'name' 'target-id') $p] } | make-map
	} else {
		each {|p| if (==s $p['type'] $type) { put [(u:select-keys $p 'name' 'target-id') $p] } } | make-map
	}
}

fn read-srcinfo {|targets-desc container|
	var static-libs = [&]

	peach {|p|
		if (==s $p['type'] 'pkgbase') {
			var tc = $targets-desc[$p['target-id']]['target-config']

			if (not (has-key $p 'srcinfo')) {
				set p['srcinfo'] = (printsrcinfo &lock-file=$p['lock-file'] $container $c:default-makepkg-conf $p['dir'] | u:parse-srcinfo)
			}

			var pkgbase-has-static = (
				u:with-key $p['srcinfo'] 'options' {|opts|
					u:makepkg-option $opts 'staticlibs' $false
				} &or-else= { put $false }
			)

			u:map-loop $p['srcinfo']['pkgnames'] {|pn pkg|
				var has-static = (
					u:with-key $pkg 'options' {|opts|
						u:makepkg-option $opts 'staticlibs' $pkgbase-has-static
					} &or-else= { put $pkgbase-has-static }
				)

				if $has-static {
					u:echo-warning 'Package `'$pn'` for target `'$p['target-id']'` will keep static libraries.'
					set static-libs[[$p['target-id'] $pn]] = $nil
				}
			}
		}

		put $p
	} | {
		var graph = (map-graph)

		#u:log { pprint $graph }

		#fn put-if-exists {|entry|
			#if (has-key $graph [&type= 'pkg' &name= $entry['name'] &target-id= $entry['target-id']]) {
				#put $entry
			#}
		#}

		u:p-map-loop $graph {|_ p|

			if (==s $p['type'] 'pkgbase') {

				var tc = $targets-desc[$p['target-id']]['target-config']
				var hc = $targets-desc[$p['host-id']]['target-config']
				var host-and-target-is-not-same = (!=s $tc['id'] $hc['id'])

				var make-depends = [(u:get-srcinfo-values-by-arch $p['srcinfo'] 'makedepends' ^
						&arch=$hc['arch'] &versioned=$false &pkgbase-only=$true ^
					| each {|pd|
						put [&name= $pd &target-id= $hc['id']]
					})]

				set p['depends'] = [(
					u:get-srcinfo-values-by-arch $p['srcinfo'] 'depends' &arch=$tc['arch'] &versioned=$false &pkgbase-only=$true ^
						| each {|pd|
							put [&name= $pd &target-id= $tc['id']]
						}

					# Make dependencies producing static libraries will also be considered dependencies.
					for md $make-depends {
						if (and $host-and-target-is-not-same ^
						        (or (has-key $tc['static-libs'] $md['name']) ^
						            (has-key $static-libs [$tc['id'] $md['name']]))) {
							put [&name= $md['name'] &target-id= $tc['id'] &static= $true]
						}
					}
				)]

				set p['make-depends'] = [(
					all $make-depends

					# We will consider all dependencies as make dependencies at this point in the graph.
					for d $p['depends'] {
						put [&name= $d['name'] &target-id= $hc['id']]
					}

					# When cross-compiling, we need to build make dependencies for the target as well.
					if (==s $p['cc-method']['method'] 'cc') {
						for d $make-depends {
							put [&name= $d['name'] &target-id= $p['target-id']]
						}
					}
				)]

				set p['check-depends'] = [(
					u:get-srcinfo-values-by-arch $p['srcinfo'] 'checkdepends' &arch=$tc['arch'] &versioned=$false &pkgbase-only=$true ^
						| each {|pd|
							put [&name= $pd &target-id= $tc['id']]
						}
				)]

			} elif (==s $p['type'] 'pkg') {

				var srcinfo = $graph[[&type= 'pkgbase' &name= $p['pkgbase'] &target-id= $p['target-id']]]['srcinfo']

				u:with-key $srcinfo['pkgnames'][$p['name']] 'arch' {|archs|
					set p['arch'] = $archs
				} &or-else={ set p['arch'] = $srcinfo['arch'] }

			}

			put $p
		}
	}
}

fn map-runtime-deps {
	{
		var pkgbase-deps = [&]

		var graph = [(
			each {|p|
				if (==s $p['type'] 'pkgbase') {
					set pkgbase-deps[[$p['target-id'] $p['name']]] = (
						all $p['depends'] | each {|d| put [$d['target-id'] $d['name']] } | u:make-set
					)
				}

				put $p
			}
		)]

		all $graph | peach {|p|
			if (==s $p['type'] 'pkg') {
				var deps = $pkgbase-deps[[$p['target-id'] $p['pkgbase']]]

				set p['depends'] = [(
					for d $p['depends'] {
						if (not (has-key $deps [(u:put-keys $d 'target-id' 'name')])) {
							set d['runtime'] = $true
						}

						put $d
					}
				)]
			}

			put $p
		}
	} | {
		var graph = (map-graph)

		fn get-runtime-deps {|p|
			u:safe-recurse &key={|p| put [(u:put-keys $p 'target-id' 'name')] } $p {|p recurse|
				for d $p['depends'] {
					if (u:has-key-and-value $d 'runtime' $true) {
						u:with-key $graph [&type= 'pkg' &name= $d['name'] &target-id= $d['target-id']] {|entry|
							put $d
							$recurse $entry
						}
					}
				}
			}
		}

		u:p-map-loop $graph {|_ p|
			if (==s $p['type'] 'pkg') {
				set p['runtime-depends'] = [(get-runtime-deps $p)]
				set p['depends'] = [(
					for d $p['depends'] {
						if (not (u:has-key-and-value $d 'runtime' $true)) {
							put $d
						}
					}
				)]
			}

			put $p
		}
	}
}

fn should-ignore-arch {|ignore-arch arch pkg-archs|
	and $ignore-arch (not (has-key $pkg-archs $arch)) (not (has-key $pkg-archs 'any'))
}

fn check-forced {|&try=$false &force=$false|
	var pkgbase-requested = [&]

	var graph = [(
		each {|p|
			if (==s $p['type'] 'pkg') {
				var k = [$p['pkgbase'] $p['target-id']]
				var pkgbase-try pkgbase-force = (u:with-key $pkgbase-requested $k {|pbr|
					put $pbr['t'] $pbr['f']
				} &or-else= { put $false $false })
				set pkgbase-requested[$k] = [
					&r= (or (u:default-conf-value $pkgbase-requested $k $false) (u:not-empty $p['for']))
					&f= (or $pkgbase-force (u:has-key-and-value $p 'forced' $true))
					&t= (or $pkgbase-try (u:has-key-and-value $p 'try' $true))
				]
			}
			put $p
		}
	)]

	var try-all = (eq $try 'all')
	var try-requested = (eq $try $true)
	var force-all = (eq $force 'all')
	var force-requested = (eq $force $true)

	all $graph | peach {|p|
		if (==s $p['type'] 'pkgbase') {
			set p['try'] = (or (u:has-key-and-value $p 'try' $true) ^
				                 $try-all ^
				                 $pkgbase-requested[[$p['name'] $p['target-id']]]['t'] ^
				                 (and $try-requested ^
				                      $pkgbase-requested[[$p['name'] $p['target-id']]]['r']))
			set p['forced'] = (or (u:has-key-and-value $p 'forced' $true) ^
				                    $force-all ^
				                    $pkgbase-requested[[$p['name'] $p['target-id']]]['f'] ^
				                    (and $force-requested ^
				                         $pkgbase-requested[[$p['name'] $p['target-id']]]['r']))
		}
		put $p
	}
}

fn configure-build-graph {|host-container targets-desc build-params
		&skip-gpg-checks=$false
		&check=$false
		&check-force=$nil
		&ignore-arch=$false
		&clean=$nil
		&clean-build=$nil|

	var allowed-keys = ['env-vars' 'check' 'ignore-arch' 'gpg-keys' 'pacman-keys' 'skip-gpg-checks' 'patches' 'forced' 'category']
	set allowed-keys = (conj $allowed-keys (keys $c:build-flags | each {|k| put $k'-flags' $k'-flags-override' }))

	peach {|p|
		if (==s $p['type'] 'pkgbase') {
			var tc = $targets-desc[$p['target-id']]['target-config']

			set p = (select-compile-flags $p $tc)
			set p['check'] = (maybe-run-check $p &check=$check &check-force=$check-force)
			set p['ignore-arch'] = (should-ignore-arch $ignore-arch $tc['arch'] $p['srcinfo']['arch'])
			set p['run-prepare-on-build'] = (u:has-keys-and-value $tc['pkg-config'] [$p['name'] 'run-prepare-on-build'] $true)
			set p['gpg-keys'] = [(u:with-keys $c:conf['pkg-config'] [$p['name'] 'import-gpg-keys'] {|ks| all $ks })]
			set p['pacman-keys'] = [(u:with-keys $c:conf['pkg-config'] [$p['name'] 'import-pacman-keys'] {|ks| all $ks })]
			set p['skip-gpg-checks'] = (or $skip-gpg-checks ^
			                               (and (has-key $c:conf['pkg-config'] $p['name']) ^
			                                    (u:has-key-and-value $c:conf['pkg-config'][$p['name']] 'skip-gpg-checks' $true)))
			set p['patches'] = [(u:with-keys $tc['pkg-config'] [$p['name'] 'patches'] {|p| all $p })]

			if (not-eq $tc['config-package'] $nil) {
				fn on-end {|ns|
					for k $allowed-keys {
						u:with-key $ns['p'] $k {|v| set p[$k] = $v }
					}
				}

				u:log {
					eval &ns=(ns [&p= $p]) &on-end=$on-end~ $tc['config-package']
				}
			}

			if (not-eq $clean $nil) {
				set p['clean'] = $clean
			}
			if (not-eq $clean-build $nil) {
				set p['clean-build'] = $clean-build
			}

			#u:log { pprint $p }
		}

		#u:log { pprint $p }

		put $p
	}
}

fn remove-dups {
	var inserted = [&]

	each {|p|
		var k = [(==s $p['type'] 'pkgbase') $p['name'] $p['target-id']]
		if (not (has-key $inserted $k)) {
			put $p
			set inserted[$k] = $nil
		}
	}
}

fn config-package-by-host {|p targets-desc host-targets|
	var arch = $targets-desc[$p['target-id']]['target-config']['arch']

	fn pkg-config-host-var {|key &default={|k| put $c:conf[$k] }|
		u:with-keys $c:conf['pkg-config'] [$p['name'] $key] {|v|
			put $v
		} &or-else= { $default $key }
	}

	set p['heavy'] = (pkg-config-host-var 'heavy' &default={|_| put $false })
	set p['clean'] = (pkg-config-host-var 'clean')
	set p['clean-build'] = (pkg-config-host-var 'clean-build')

	set p['cc-method'] = (
		u:with-keys $c:conf['pkg-config'] [$p['name'] 'cross-compile-method' $arch] {|v|
			put $v
		} &or-else={
			u:with-key $c:conf['cross-compile-method'] $arch {|v|
				put $v
			} &or-else={ put [&method= 'emu'] }
		}
	)

	if (not-eq $c:conf['config-package'] $nil) {
		var conf = ($c:conf['config-package'] $p)
		for k ['cc-method' 'heavy' 'clean' 'clean-build'] {
			u:with-key $c:conf $k {|v| set p[$k] = $v }
		}
	}

	#u:log { echo $p['name']', '$p['target-id']', '$arch; pprint $p['cc-method'] }

	if (==s $p['cc-method']['method'] 'cc') {
		set p['host-id'] = $host-targets[$p['cc-method']['host-arch']]
	} else {
		set p['host-id'] = $host-targets[(u:default-conf-value $p['cc-method'] 'host-arch' $arch )]
	}

	#u:log { echo $p['target-id']; pprint $host-targets }

	put $p
}

fn remap-targets {|targets-desc host-targets|
	var remapped-pkgs = [&]

	var graph = (
		each {|p|
			if (==s $p['type'] 'pkgbase') {
				var new-target = ($c:conf['remap-target'] $p $targets-desc[$p['target-id']]['target-config'])

				if (!=s $new-target $p['target-id']) {
					u:echo-info 'Package base `'$p['name']'` for target `'$p['target-id']'` remapped to target `'$new-target'`.'

					for n $p['pkgnames'] {
						set remapped-pkgs[$n] = $new-target
					}

					set p['target-id'] = $new-target
					set p = (config-package-by-host $p $targets-desc $host-targets)
				}
			}

			put $p
		} | map-graph
	)

	#u:log { pprint $remapped-pkgs }

	u:p-map-loop $graph {|_ p|
		fn remap {|type|
			if (has-key $p $type) {
				set p[$type] = [(
					for d $p[$type] {
						u:with-key $remapped-pkgs $d['name'] {|new-target|
							u:echo-info 'Dependency `'$d['name']'` of type `'$type'` of `'$p['type']'@'$p['name']'@'$p['target-id']'`' ^
								'remapped to target `'$new-target'`.'
							set d['target-id'] = $new-target
						}
						put $d
					}
				)]
			}
		}

		remap 'depends'
		remap 'make-depends'
		remap 'check-depends'

		if (==s $p['type'] 'pkg') {
			u:with-key $remapped-pkgs $p['name'] {|new-target|
				set p['target-id'] = $new-target
			}
		}

		put $p
	}
}

fn merge-dups {
	var merged-attr = [&]

	var graph = (each {|p|
		var k = [$p['name'] $p['target-id']]

		if (==s $p['type'] 'pkg') {
			if (not (has-key $merged-attr $k)) {
				set merged-attr[$k] = [
					&for= []
					&try= $false
					&forced= $false
					&install= []
				]
			}

			set merged-attr[$k]['for'] = (conj $merged-attr[$k]['for'] (all $p['for']))
			set merged-attr[$k]['try'] = (or $merged-attr[$k]['try'] $p['try'])
			set merged-attr[$k]['forced'] = (or $merged-attr[$k]['forced'] $p['forced'])
			set merged-attr[$k]['install'] = (conj $merged-attr[$k]['install'] (all $p['install']))
		}

		put $p

	} | map-graph)

	u:map-loop $graph {|_ p|
		var k = [$p['name'] $p['target-id']]

		if (==s $p['type'] 'pkg') {
			set p = (u:concat-maps $p $merged-attr[$k])
			#u:log { echo $p['name'] '->' (all $p['for']) }
		}

		put $p
	}
}

fn read-pkgbuild {|cmd targets-desc host-container &ignore-arch=$false &check=$false|
	var dir = (if (has-key $cmd 'dir') { u:clean-path $cmd['dir'] } else { u:clean-path $cmd['name'] })
	var container-arch = $targets-desc[$cmd['target']]['target-config']['arch']
	var srcinfo = (printsrcinfo $host-container $c:default-makepkg-conf $dir | u:parse-srcinfo)
	set ignore-arch = (should-ignore-arch $ignore-arch $container-arch $srcinfo['arch'])
	var target-arch = (if $ignore-arch { put 'x86_64' } else { put $container-arch })
	var pkgbase-depends = [(u:get-srcinfo-values-by-arch $srcinfo 'depends' &arch=$target-arch &versioned=$false &pkgbase-only=$true)]
	var pkgbase-provides = [(u:get-srcinfo-values-by-arch $srcinfo 'provides' &arch=$target-arch &versioned=$false &pkgbase-only=$true)]

	put [
		&target-id= $cmd['target']
		&dir= $dir
		&pkgbase= $srcinfo['pkgbase']
		&srcinfo= $srcinfo
		&try= $cmd['try']
		&forced= $cmd['forced']
		&install= $cmd['install']
		&depends= [(u:get-srcinfo-values-by-arch $srcinfo 'depends' &arch=$target-arch &versioned=$false &pkgbase-only=$false)]
		&pkgname-depends= (
			u:map-loop $srcinfo['pkgnames'] {|pkgname pkg|
				put [
					$pkgname
					(conj $pkgbase-depends (u:get-srcinfo-values-by-arch $pkg 'depends' &arch=$target-arch &versioned=$false &pkgbase-only=$true))
				]
			} | make-map
		)
		&make-depends= [(u:get-srcinfo-values-by-arch $srcinfo 'makedepends' &arch=$target-arch &versioned=$false &pkgbase-only=$true)]
		&check-depends= [(if (not-eq $check $false) {
			u:get-srcinfo-values-by-arch $srcinfo 'checkdepends' &arch=$target-arch &versioned=$false &pkgbase-only=$true
		})]
		&requested-pkgs= [(
			if (has-key $cmd 'dir') {
				put $cmd['name']
			} else {
				keys $srcinfo['pkgnames']
			}
		)]
		&provides= (
			u:map-loop $srcinfo['pkgnames'] {|pkgname pkg|
				var pkg-provides = [(u:get-srcinfo-values-by-arch $pkg 'provides' &arch=$target-arch &versioned=$false &pkgbase-only=$true)]

				put [$pkgname [(
					if (u:empty $pkg-provides) {
						all $pkgbase-provides
					} else {
						all $pkg-provides
					} | each (u:filter-out [$pkgname])
				)]]
			} | make-map
		)
	]
}

fn insert-pkg-specs {|specs requested-pkgs target-id
		&force=$false
		&try=$false|

	for spec $specs {
		var version = $spec['srcinfo']['pkgver'][0]
		u:with-key $spec['srcinfo'] 'pkgrel' {|v|
			set version = $version'-'$v[0]
		}
		u:with-key $spec['srcinfo'] 'epoch' {|v|
			set version = $v[0]':'$version
		}

		var pkg = [
			&type= 'pkgbase'
			&name= $spec['pkgbase']
			&target-id= $target-id
			&dir= $spec['dir']
			&srcinfo= $spec['srcinfo']
			&prefix= ''
			&forced= (not-eq $force $false)
			&try= (not-eq $try $false)
			&ignore-arch= $false
			&pkgnames= [(keys $spec['srcinfo']['pkgnames'])]
			&version= $version
		]

		u:with-key $spec 'last-head' {|last-head|
			set pkg['last-head'] = $last-head
		}

		put $pkg

		keys $spec['srcinfo']['pkgnames'] | each {|pkgname|
			var requested try force install = (u:with-key $requested-pkgs [$target-id $pkgname] {|rp|
				put $true $rp['try'] $rp['forced'] $rp['install']
			} &or-else={ put $false $false $false $false })

			put [
				&type= 'pkg'
				&name= $pkgname
				&pkgbase= $spec['pkgbase']
				&depends= [(
					for d $spec['pkgname-depends'][$pkgname] {
						put [&name= $d &target-id= $target-id]
					}
				)]
				&make-depends= [(
					for d $spec['make-depends'] {
						put [&name= $d &target-id= $target-id]
					}
				)]
				&check-depends= [(
					for d $spec['check-depends'] {
						put [&name= $d &target-id= $target-id]
					}
				)]
				&provides= $spec['provides'][$pkgname]
				&target-id= $target-id
				&for= [(if $requested { put $target-id })]
				&forced= $force
				&try= $try
				&install= [(if $install { put $target-id })]
				&prefix= ''
			]
		}
	}
}

fn missing-deps-as-phonies {
	var graph-pkgnames = [&]

	var graph = [(
		each {|p|
			put $p

			if (or (==s $p['type'] 'phony') (==s $p['type'] 'pkg')) {
				set graph-pkgnames[[&name= $p['name'] &target-id= $p['target-id']]] = $nil
			}
		}
	)]

	for p $graph {
		fn check-deps {|key|
			u:with-key $p $key {|deps|
				for d $deps {
					var k = [&name= $d['name'] &target-id= $d['target-id']]
					if (not (has-key $graph-pkgnames $k)) {
						set graph-pkgnames[$k] = $nil
						put [
							&type= 'phony'
							&name= $d['name']
							&target-id= $d['target-id']
							&prefix= ''
							&provides= []
						]
					}
				}
			}
		}

		put $p

		check-deps 'depends'
		check-deps 'make-depends'
		check-deps 'check-depends'
	}
}

fn list-installable {|report pkgs-map key|
	var built-pkgs = (
		for r $report {
			if (==s $r['status'] 'built') {
				put [[&name= $r['pkgbase'] &target-id=$r['target-id']] $r]
			}
		} | make-map
	)

	u:p-map-loop $pkgs-map {|_ p|
		if (u:not-empty $p['for']) {
			fn find-depends {|type d|
				u:safe-recurse &key={|d| put [(u:put-keys $d 'target-id' 'name')] } $d {|d recurse|
					if (not (u:has-key-and-value $d 'static' $true)) {
						if (has-key $built-pkgs [&name= $d['pkgbase'] &target-id= $d['target-id']]) {
							for t $p[$key] {
								put [&name= $d['name'] &target-id= $t]
							}
						}

						for dep $d[$type] {
							u:with-key $pkgs-map [&name= $dep['name'] &target-id= $dep['target-id']] {|entry|
								$recurse $entry
							}
						}
					}
				}
			}

			if (has-key $built-pkgs [&name= $p['pkgbase'] &target-id= $p['target-id']]) {
				for t $p[$key] {
					put [&name= $p['name'] &target-id= $t]
				}
			}

			find-depends 'depends' $p
			find-depends 'runtime-depends' $p
		}
	}
}

fn remove-orphans {
	var graph = (map-graph)

	fn check {|p|
		u:safe-recurse &key={|d| put [(u:put-keys $d 'target-id' 'name')] } $p {|d recurse|
			var pkgbase-key = [&type= 'pkgbase' &target-id= $d['target-id'] &name= $d['pkgbase']]
			var pkgbase-entry = $graph[$pkgbase-key]

			set graph[$pkgbase-key]['r'] = $true

			fn find-depends {|type p|
				for dep $p[$type] {
					var pkg-key = [&type= 'pkg' &name= $dep['name'] &target-id= $dep['target-id']]
					u:with-key $graph $pkg-key {|entry|
						set graph[$pkg-key]['r'] = $true
						$recurse $entry
					}
				}
			}

			find-depends 'depends' $d
			find-depends 'make-depends' $pkgbase-entry
			find-depends 'check-depends' $pkgbase-entry
		}
	}

	u:map-loop $graph {|k p|
		if (and (==s $p['type'] 'pkg') (u:not-empty $p['for'])) {
			set graph[$k]['r'] = $true
			check $p
		}
	}

	u:map-loop $graph {|_ p|
		if (or (==s $p['type'] 'phony') (u:has-key-and-value $p 'r' $true)) {
			put $p
		}
	}
}
