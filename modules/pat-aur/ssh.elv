use os

use pat-aur/utils u

fn ctor-ssh-connection {|socket target|
	var master-ssh-pid = 0

	var host port
	var master-conn-ctl = (external (u:find-in-data-dirs 'pat-aur/scripts/ssh-master-conn-ctl'))
	var mounts = [&]

	put [
		&socket= {|_| put $socket }
		&target= {|_| put $target }
		&host= {|_| put $host }
		&port= {|_| put $port }
		&ctor= {|_|
			if (os:exists $socket) {
				if (!=s (os:stat $socket)['type'] 'socket') {
					fail 'File `'$socket'` isn''t a socket file.'
				}
				set host = $nil;
				set port = $nil
			} else {
				set host port = (u:parse-host $target)

				u:echo-title 'Connecting to `'$target'`...'
				set master-ssh-pid = (
					$master-conn-ctl start $socket ^
							(if (eq $host $nil) { put 'dummy' } else { put $host }) ^
							(if (not-eq $port $nil) { put $port }) ^
						| from-json
				)['pid']
			}
		}
		&destroy= {|_|
			if (!= $master-ssh-pid 0) {
				var mount-paths = [(keys $mounts)]
				if (u:not-empty $mount-paths) {
					u:echo-title 'Unmounting remote filesystems.'
					for p $mount-paths {
						e:fusermount3 -u $p
					}
				}

				u:echo-title 'Terminating connection'(if (not-eq $host $nil) { put ' to `'$host'`' })'.'
				$master-conn-ctl stop $master-ssh-pid
			}
		}
		&exec= {|_ @args &close-stdin=$false|
			var argstr = (print (u:escape-quoted $@args))
			if $close-stdin { set argstr = $argstr' <&-' }
			#u:log { echo $argstr }
			e:ssh -S $socket 'dummy' $argstr
		}
		&exec-shell= {|_ @args &close-stdin=$false|
			var argstr = (print (u:escape-quoted $@args))
			if $close-stdin { set argstr = $argstr' <&-' }
			e:ssh -S $socket 'dummy' -t $argstr
		}
		&copy= {|_ @files dest &quiet=$true|
			e:scp (if $quiet { put '-q' }) -o 'ControlPath='$socket $@files 'dummy:'$dest
		}
		&mount= {|_ src dest|
			var clean-path = (u:clean-path $dest)
			if (has-key $mounts $clean-path) {
				fail 'Path `'$clean-path'` already mounted.'
			}

			e:sshfs 'dummy:'$src -o ssh_command='ssh -S '$socket -o 'rw' $dest
			set mounts[$clean-path] = $src
		}
		&unmount= {|_ dest|
			var clean-path = (u:clean-path $dest)
			if (not (has-key $mounts $clean-path)) {
				fail 'Nothing mounted at `'$dest'`.'
			}

			e:fusermount3 -zu $dest
			del mounts[$clean-path]

			while ?(e:mountpoint -q $dest) {
				sleep 100ms
			}
		}
	]
}
