use path
use os
use str
use re
use math

use ./utils u

var conf

var config-dir = (u:xdg-config-dir)/'pat-aur'
var default-makepkg-conf = (u:find-in-data-dirs 'pat-aur/defaults/makepkg.conf')
var build-flags = [
	&c= 'CFLAGS'
	&cxx= 'CXXFLAGS'
	&ld= 'LDFLAGS'
	&lto= 'LTOFLAGS'
	&rust= 'RUSTFLAGS'
	&cpp= 'CPPFLAGS'
]

fn resolve-conf-path {|path &must-exists=$true|
	var new-path = ({
			if (path:is-abs $path) {
				put $path
			} else {
				put $config-dir/$path
			}
		})

	if $must-exists {
		u:fail-if (not (os:exists &follow-symlink=$true $new-path)) 'Config file `'$path'` not found.'
	}

	put $new-path
}

fn read-container-opts {|@confs|
	var default-cache-dir = (u:xdg-cache-dir)/'pat-aur'

	var res = [
		&binds= []
		&ro-binds= []
		&env-vars= [&]
		&symlinks= [&]
		&pkg-config= [&]
		&tmpfs= []
		&bin-path= []
		&import-pacman-keys= []
		&import-gpg-keys= []
		&source-dir= $default-cache-dir/'sources'
		&log-dir= $nil
		&seccomp-filters= []
		&cap-add= []
		&cap-drop= []
		&fs= []
	]

	for k [env-vars pkg-config symlinks] {
		for cf $confs {
			if (u:has-key-and-value $cf $k'-override' $true) {
				u:with-key $cf $k {|o|
					set res[$k] = $o
				} &or-else= { set res[$k] = [&] }
			} else {
				u:with-key $cf $k {|o|
					set res[$k] = (u:concat-maps $res[$k] $o)
				}
			}
		}
	}

	for k [tmpfs binds ro-binds import-pacman-keys import-gpg-keys cap-add cap-drop seccomp-filters fs] {
		for cf $confs {
			if (u:has-key-and-value $cf $k'-override' $true) {
				u:with-key $cf $k {|o|
					set res[$k] = $o
				} &or-else= { set res[$k] = [] }
			} else {
				u:with-key $cf $k {|o|
					set res[$k] = (conj $res[$k] $@o)
				}
			}
		}
	}

	for k [bin-path] {
		u:reverse-loop $confs {|cf|
			if (u:has-key-and-value $cf $k'-override' $true) {
				u:with-key $cf $k {|o|
					set res[$k] = $o
				} &or-else= { set res[$k] = [] }
			} else {
				u:with-key $cf $k {|o|
					set res[$k] = (conj $res[$k] $@o)
				}
			}
		}
	}

	for k [source-dir log-dir] {
		for cf $confs {
			u:with-key $cf $k {|o|
				set res[$k] = $o
			}
		}
	}

	#pprint $res >&2

	put $res
}

fn read-host-config {|&conf-path=$nil|
	var read-conf = []
	var default-cache-dir = (u:xdg-cache-dir)/'pat-aur'
	var temp-dir = (u:pat-aur-temp-dir)

	if (and (not-eq $conf-path $nil) (not (os:exists $conf-path))) {
		fail 'Config `'$conf-path'` not found.'
	}

	set conf-path = (coalesce $conf-path $config-dir/'config.host')

	if (os:exists $conf-path) {
		eval &on-end={|ns| set read-conf = $ns['config'] } (slurp < $conf-path)
	}

	u:concat-maps (
			put ^
				['arch' [&(e:uname -m | one)= []]] ^
				['build-containers-dir' $temp-dir/'build-containers'] ^
				['pkgbuild-dir' $default-cache-dir/'pkgbuild'] ^
				['pacman-sync-dir' $default-cache-dir/'sync'] ^
				['nice-level' 10] ^
				['parallel-pools' {
					math:floor (
						math:max (math:min (/ (e:awk '/MemAvailable/ {print $2}' '/proc/meminfo' | one) 8388608) ^
						                   (/ (e:nproc) 4)) ^
						         1
					)
				}] ^
				['parallel-heavy-pools' {
					math:floor (
						math:max (math:min (/ (e:awk '/MemAvailable/ {print $2}' '/proc/meminfo' | one) 33554432) ^
						                   (/ (e:nproc) 16)) ^
						         1
					)
				}] ^
				['hook-after' $nil] ^
				['local-repos' $nil] ^
				['continue-on-failure' $false] ^
				['ninja-log' $nil] ^
				['check' $false] ^
				['check-force' $false] ^
				['auto-import-pkgbuild-keys' $false] ^
				['gpg-signature-key' $nil] ^
				['repos' [&]] ^
				['remap-target' { put {|p @rest| put $p['target-id'] } }] ^
				['pkg-cache-dir' $default-cache-dir/'pkg-cache'] ^
				['clear-pkg-cache-after-build' $false] ^
				['parallel-downloads' 5] ^
				['download-agents' [&]] ^
				['use-download-engine' $true] ^
				['config-container' { put $nop~ }] ^
				['aur' [&]] ^
				['cross-compile-method' [&]] ^
				['config-package' $nil] ^
				['graph-review' $nil] ^
				['confirm-install' $true] ^
				['arch-pkg-clone-url' 'https://gitlab.archlinux.org/archlinux/packaging/packages'] ^
				['cgroup-slice' $nil] ^
				['cgroup-heavy-slice' $nil] ^
				['cgroup-slice-inherit' $true] ^
				['ignore-arch' $false] ^
				['clean' $false] ^
				['clean-build' $false] ^
				['check-rebuild' $true] ^
			| u:default-conf-key $read-conf | make-map) ^
		(read-container-opts $read-conf)
}

fn read-target-config {|t-conf &pm-conf=$nil|
	var makepkg-conf-path = (u:default-conf-value $t-conf 'makepkg-conf' $nil)

	if (not-eq $makepkg-conf-path $nil) {
		set t-conf['makepkg-conf'] = (resolve-conf-path $makepkg-conf-path)
	} else {
		set t-conf = (dissoc $t-conf 'makepkg-conf')
	}

	set t-conf['local-repo'] = (u:get-repo-info &pm-conf=$pm-conf (u:default-conf-value $t-conf 'local-repo' 'local-repo'))
	set t-conf['id'] = (u:default-conf-value $t-conf 'id' { one < '/etc/machine-id' })
	set t-conf['arch'] = (u:default-conf-value $t-conf 'arch' { e:uname -m | one })

	put $t-conf
}

fn read-target-config-by-host {|t-conf|
	var t-conf-res = ({
		u:map-loop $build-flags {|flag _|
			put [$flag'-flags' '']
		}

		put ^
			['id' { fail 'No target ID provided.' }] ^
			['arch' { fail 'No target architecture provided.' }] ^
			['env-vars' [&]] ^
			['providers-map' [&]] ^
			['pacman-cmd' [pacman]] ^
			['sudo-cmd' [sudo]] ^
			['pacman-install-cmd' [pacman -S]] ^
			['pkg-config' [&]] ^
			['ignore-updates' []] ^
			['assume-installed' []] ^
			['compile-from-source' []] ^
			['periodic-update-check' []] ^
			['aur' $nil] ^
			['static-libs' []] ^
			['makepkg-conf' { u:find-in-data-dirs 'pat-aur/defaults/makepkg.conf' }] ^
			['local-repo' { fail 'No target local repository provided.' }] ^
			['config-package' $nil] ^
			['aur-blacklist' []]
	} | u:default-conf-key $t-conf | make-map)

	set t-conf-res['static-libs'] = (all $t-conf-res['static-libs'] | u:make-set)
	set t-conf-res['assume-installed'] = (all $t-conf-res['assume-installed'] | u:make-set)
	set t-conf-res['aur-blacklist'] = (all $t-conf-res['aur-blacklist'] | u:make-set)
	put $t-conf-res
}

fn try-exec-target-config {|&arch=$nil &target-id=$nil &type='machine' &host-machine-id=(one < '/etc/machine-id') &target-conf-file=$nil &pm-conf=$nil|
	var cmd = ({
		if (not-eq $target-conf-file $nil) {
			if (os:exists &follow-symlink=$true $target-conf-file) {
				external $target-conf-file
			} else {
				external (resolve-conf-path $target-conf-file)
			}
		} else {
			var target-config-script = $config-dir/'config.target'
			if (os:exists &follow-symlink=$true $target-config-script) {
				external $target-config-script
			} else {
				external (u:find-in-data-dirs 'pat-aur/defaults/config.target')
			}
		}
	})

	read-target-config ($cmd ^
			(u:flag-if-not-nil 'arch' $arch) ^
			(u:flag-if-not-nil 'target-id' $target-id) ^
			(u:flag-if-not-nil 'type' $type) ^
			(u:flag-if-not-nil 'host-machine-id' $host-machine-id) | from-json) ^
		&pm-conf=$pm-conf
}

fn get-arch-config {|arch|
	if (not (has-key $conf['arch'] $arch)) { fail 'Architecture config not found for `'$arch'`.' }

	var specs = $conf['arch'][$arch]
	var pacman-conf-path = (u:default-conf-value $specs 'pacman-conf' $nil)
	if (eq $pacman-conf-path $nil) {
		set pacman-conf-path = (u:find-in-data-dirs 'pat-aur/defaults/pacman.conf.in')
	} else {
		set pacman-conf-path = (resolve-conf-path $pacman-conf-path)
	}

	u:concat-maps ^
		[
			&name= $arch
			&pacman-conf= $pacman-conf-path
		] ^
		(
			put ^
				['root' { put (u:xdg-cache-dir)/'pat-aur/roots/'$arch }] ^
				['layers' [&]] ^
				['arch' $arch] ^
				['host' $nil] ^
				['packages' []] ^
				['bind-toolchain' $nil] ^
				['host-config' $nil] ^
				['cross-compilers' [&]] ^
				['seccomp-filters' []] ^
			| u:default-conf-key $specs | make-map) ^
		(read-container-opts $conf $specs)
}

fn select-aur-location {|location|
	if (not-eq $location $nil) {
		u:with-key $conf['aur'] $location {|specs|
			put $specs
		} &or-else= {
			fail 'AUR '$location' not defined in host config.'
		}
	} else {
		put [
			&url= 'https://aur.archlinux.org'
			&rpc= '/rpc'
			&version= 5
		]
	}
}

fn define-aur-location {|specs|
	if (not-eq $specs $nil) {
		put 'AUR_LOCATION='$specs['url']
		u:with-key $specs 'rpc' {|rpc|
			put 'AUR_QUERY_RPC='$specs['url']$rpc
		}
		u:with-key $specs 'version' {|version| put 'AUR_QUERY_RPC_VERSION='(print $version) }
	}
}
