use str
use path
use os
use re

use ./utils u
use ./config c
use ./container ct
use ./download dl
use ./graph g

var goarch-uname-map = [
	&'i386'= '386'
	&'x86_64'= 'amd64'
	&'armv6l'= 'arm'
	&'armv7l'= 'arm'
	&'aarch64'= 'arm64'
]

fn map-go-arch {|arch|
	u:with-key $goarch-uname-map $arch {|a|
		put $a
	} &or-else= {
		put $arch
	}
}

fn setup-container-layer {|container-root fc &override-base-opts=$false &opts=[] &dir=$nil|
	if (eq $dir $nil) {
		set dir = $c:conf['build-containers-dir']
	}

	os:mkdir-all $dir
	var container-dir = (os:temp-dir &dir=$dir 'build-')
	defer {
		if (u:env-is-false 'PATAUR_DEBUG') {
			u:ignore-errors { e:rm -rf --one-file-system $container-dir }
		}
	}
	e:chmod 755 $container-dir

	u:scoped ($container-root[layer] &override-base-opts=$override-base-opts &opts=$opts $container-dir) {|layer|
		u:scoped (u:make (u:ctor-synchronized ($layer['path'])/'lock' $layer)) {|layer|
			defer {
				if (u:env-is-false 'PATAUR_DEBUG') {
					$layer[lock] {|layer| $layer[delete] }
				}
			}
			$layer[lock] {|layer| $layer[save] }
			$fc $layer
		}
	}
}

fn import-pacman-key {|container key &opts=[]|
	e:gpg --export $key | $container[exec] ^
			&opts=$opts ^
			&use-seccomp-filters=$false ^
			&as-root=$true ^
		bash -c 'gpg_dir="$(pacconf GPGDir)"; pacman-key --gpgdir="$gpg_dir" --add - && pacman-key --gpgdir="$gpg_dir" --lsign-key '$key
}

fn import-gpg-key {|container key &opts=[]|
	e:gpg --export $key | $container[exec] ^
			&opts=$opts ^
			&use-seccomp-filters=$false ^
			&as-root=$false ^
		bash -c -l 'gpg --homedir="$HOME/.gnupg" --import'
}

fn import-keys {|container arch-config &opts=[]|
	if (not-eq $c:conf['gpg-signature-key'] $nil) {
		import-pacman-key $container $c:conf['gpg-signature-key']
	}
	for key $arch-config['import-pacman-keys'] { import-pacman-key &opts=$opts $container $key }
	for key $arch-config['import-gpg-keys'] { import-gpg-key &opts=$opts $container $key }
}

fn repo-config {|target-config|
	{
		put $target-config['local-repo']

		if (u:has-key-and-not-nil $c:conf 'local-repos') {
			$c:conf['local-repos'] $target-config
		}
	} | u:to-pacman-repo-config | slurp
}

fn clear-pkg-cache {
	if (not-eq $c:conf['pkg-cache-dir'] $nil) {
		e:rm -rf $c:conf['pkg-cache-dir']/*[nomatch-ok]
	}
}

fn clear-source-cache {
	if (not-eq $c:conf['source-dir'] $nil) {
		e:rm -rf $c:conf['source-dir']/*[nomatch-ok]
	}
}

fn clear-log-cache {
	if (not-eq $c:conf['log-dir'] $nil) {
		e:rm -rf $c:conf['log-dir']/*[nomatch-ok]
	}
}

fn clear-sync-cache {
	e:rm -rf $c:conf['pacman-sync-dir']/*[nomatch-ok]
}

fn clear-aur-cache {
	e:rm -rf $c:conf['pkgbuild-dir']/*[nomatch-ok]
}

fn patch-url-substitute {|url srcinfo|
	var res = $url

	for term ['pkgbase'] {
		set res = (str:replace '{'$term'}' $srcinfo[$term] $res)
	}

	for term ['pkgver'] {
		set res = (str:replace '{'$term'}' $srcinfo[$term][0] $res)
	}

	put $res
}

fn get-seccomp-filters {|pkgbase arch-config|
	var override = $false

	u:with-key $c:conf['pkg-config'] $pkgbase {|entry|
		u:with-key $entry 'seccomp-filters' {|filters|
			all $filters
			set override = (u:has-key-and-value $entry 'seccomp-filters-override' $true)
		}
	}

	if (not $override) {
		all $arch-config['seccomp-filters']
	}
}

fn default-package-install {|f repo-path repo-root key|
	if (not-eq $key $nil) {
		u:log-redirect-to-error { u:echo-info 'Signed '(path:base $f)' with key `'$key'`' }
		u:sign-package $f $key
	}
	u:move-to-repo $f $repo-path $repo-root &sign=(not-eq $key $nil) &key=$key
	u:log-redirect-to-error { u:echo-info 'Installed `'(path:base $f)'` to `'$repo-root'`' }
}

fn build-in-container {|dir container build-opts arch-config container-config
		&jobs=$nil
		&container-opts=[]
		&report-file=$nil
		&after=$nil|
	var has-target-pacman-conf = (os:exists $build-opts['target-pacman-conf'])
	var target-repos-info = [(
		if $has-target-pacman-conf {
			u:get-local-repos-info &pm-conf=$build-opts['target-pacman-conf']
		}
	)]
	var host-repos-info = [(u:get-local-repos-info &pm-conf=$build-opts['host-pacman-conf'])]
	var container-path = ($container[clean-path])
	var target-repo-root = (path:dir $build-opts['repo'])

	var temp-dir = (u:create-temp-dir &sub-dir='builds' 'build-')
	defer {
		if (u:env-is-false 'PATAUR_DEBUG') {
			e:rm -rf --one-file-system $temp-dir
		}
	}

	var target-sync-path = $c:conf['pacman-sync-dir']/$build-opts['target-id']
	var host-sync-path = $c:conf['pacman-sync-dir']/$build-opts['host-id']
	var host-pkg-cache = (
		if (not-eq $c:conf['pkg-cache-dir'] $nil) {
			put $c:conf['pkg-cache-dir']/$build-opts['host-id']
		} else {
			os:mkdir-all $container-path/'files/host-pkgs'
			put $container-path/'files/host-pkgs'
		}
	)
	var target-pkg-cache = (
		if (not-eq $c:conf['pkg-cache-dir'] $nil) {
			put $c:conf['pkg-cache-dir']/$build-opts['target-id']
		} else {
			os:mkdir-all $container-path/'files/target-pkgs'
			put $container-path/'files/target-pkgs'
		}
	)

	os:mkdir-all $target-sync-path
	os:mkdir-all $host-sync-path
	os:mkdir-all $target-pkg-cache
	os:mkdir-all $host-pkg-cache
	os:mkdir-all $container-path/'files/pkgbuild'


	var patches = [(
		for p $build-opts['patches'] {
			set p['parts'] = (dl:parse-source (patch-url-substitute $p['url'] $build-opts['srcinfo']))
			set p['dir'] = (patch-url-substitute $p['dir'] $build-opts['srcinfo'])
			put $p
		}
	)]

	var target-arch = (
		if (has-key $c:conf['arch'] $build-opts['target-arch']) {
			put (c:get-arch-config $build-opts['target-arch'])['arch']
		} else { put $arch-config['arch'] }
	)

	var sources = [(
		u:get-srcinfo-values-by-arch $build-opts['srcinfo'] 'source' &arch=$target-arch &pkgbase-only=$true ^
			| each $dl:parse-source~
	)]

	var pacman-args = [(
		if (or (not $u:use-styles) (not $u:use-colors)) { put '--color' 'never' }
	)]

	var use-target-repo = (and $has-target-pacman-conf ^
	                           (or (u:not-empty $build-opts['static-libs']) (u:not-empty $build-opts['target-depends'])))

	var source-path

	#u:log { pprint $arch-config }
	#u:log { pprint $build-opts }

	var cc = (
		if (==s $build-opts['cc-method']['method'] 'cc') {
			u:with-keys $arch-config ['cross-compilers' $build-opts['target-arch']] {|cc|
				u:with-key $cc 'packages' {|pkgs|
					set build-opts['host-depends'] = (conj $build-opts['host-depends'] $@pkgs)
				}
				put $cc
			} &or-else={ put $nil }
		} else {
			put $nil
		}
	)
	var chost = (
		if (not-eq $cc $nil) {
			put $cc['tuple']
		} elif (not-eq $arch-config['host'] $nil) {
			put $arch-config['host']
		} else {
			put $nil
		}
	)
	#u:log { pprint $cc }

	if (and (not-eq $cc $nil) (u:has-key-and-not-nil $cc 'source-dir')) {
		set source-path = $cc['source-dir']
	} elif (not-eq $container-config['source-dir'] $nil) {
		set source-path = $container-config['source-dir']
	} else {
		set source-path = $container-path/'files/pkgbuild/src'
		os:mkdir-all $source-path
	}

	e:touch $temp-dir/'pkgbuild-copy'

	var base-opts = [(
		put ^
			[&bind= [$container-path/'files/pkgbuild' '/pkgbuild']] ^
			[&bind= [$source-path '/pkgbuild/src']] ^
			[&bind= [$dir '/pkgbuild/pkg']] ^
			[&ro-bind= [$dir '/pkgbuild/pkg-copy']] ^
			[&bind= [$temp-dir/'pkgbuild-copy' '/pkgbuild/pkg-copy/PKGBUILD']]

		if (and (not-eq $cc $nil) (u:has-key-and-not-nil $cc 'log-dir')) {
			var log-dir = $cc['log-dir']/$build-opts['target-id']
			os:mkdir-all $log-dir
			put [&bind= [$log-dir '/pkgbuild/log']]
		} elif (not-eq $container-config['log-dir'] $nil) {
			var log-dir = $container-config['log-dir']/$build-opts['target-id']
			os:mkdir-all $log-dir
			put [&bind= [$log-dir '/pkgbuild/log']]
		} else {
			os:mkdir-all $container-path/'files/pkgbuild/log'
		}

		ct:conf-container-opts $container-config
		$c:conf['config-container'] (assoc $build-opts 'arch' $arch-config['name'])
		all $container-opts

		u:map-loop $build-opts['env-vars'] {|key value|
			put [&env-var= [$key $value]]
		}

		all $host-repos-info | ct:bind-local-repos
		all $target-repos-info | ct:bind-local-repos
		put [&bind= [$target-repo-root $target-repo-root]]

		put [&tmpfs= ~/'.parallel']
		u:mkdir-safe ~/'.parallel/semaphores'
		put [&bind= ~/'.parallel/semaphores']

		if $use-target-repo {
			put [&bind= [$target-sync-path/'sync' '/var/lib/pacman/target-sync']]
		}

		if (not $u:use-colors) {
			put [&env-var= ['NO_COLOR' '1']]
		}

		if (not $u:use-styles) {
			put [&env-var= ['NO_STYLES' '1']]
		}

		if $has-target-pacman-conf {
			put [&ro-bind= [$build-opts['target-pacman-conf'] '/etc/pacman.target.conf']]
		}

		var hook-before-file = $c:config-dir/'hook-before'
		if (os:exists &follow-symlink=$true $hook-before-file) {
			put [&ro-bind= [$hook-before-file '/pkgbuild/exec-before']]
		}

		put ^
			[&ro-bind= [$build-opts['makepkg-conf'] '/etc/makepkg.conf']] ^
			[&bind= [$host-sync-path/'sync' '/var/lib/pacman/host-sync']] ^
			[&bind= [$host-pkg-cache '/var/cache/pacman/pkg']] ^
			[&bind= [$target-pkg-cache '/var/cache/pacman/target-pkg']] ^
			[&env-var= ['CARCH' $arch-config['arch']]] ^
			[&env-var= ['PATAUR_TARGET_ARCH' $build-opts['target-arch']]] ^
			[&env-var= ['PATAUR_USE_DL_ENGINE' (u:select $c:conf['use-download-engine'] '1' '0')]]
	)]

	fn setup-build-container {|container fc|
		var temp-dir = (u:create-temp-dir 'gnupg-')
		defer { e:rm -rf --one-file-system $temp-dir }

		os:mkdir-all &perm=0o700 $temp-dir/'gnupg'
		os:mkdir $temp-dir/'gnupg.wd'

		var opts = [
			[&overlay= [
				&dest= ($container[home])/'.gnupg'
				&internal= $true
				&if-exists= $true
				&src= [($container[home])/'.gnupg']
				&rw-src= $temp-dir/'gnupg'
				&work-dir= $temp-dir/'gnupg.wd'
			]]
		]

		setup-container-layer &opts=(conj $base-opts $@opts) $container {|layer|
			$layer[lock] &excl=$true {|layer|
				if (u:not-nil-or-empty $build-opts['gpg-keys']) {
					u:log { for key $build-opts['gpg-keys'] { import-gpg-key $layer $key } }
				}
				if (u:not-nil-or-empty $build-opts['pacman-keys']) {
					u:log { for key $build-opts['pacman-keys'] { import-pacman-key $layer $key } }
				}

				if (has-key $build-opts['srcinfo'] 'validpgpkeys') {
					for key $build-opts['srcinfo']['validpgpkeys'] {
						u:log { u:ignore-errors { import-gpg-key $layer $key } }
					}
				}

				$fc $layer
			}
		}
	}

	fn bind-container-db {|container pm-conf sync-path &bind-local=$true @args|
		os:mkdir-all $temp-dir/'pacman-db'
		os:mkdir-all $temp-dir/'pacman-db/empty-db'

		e:bwrap --dev-bind / / ^
			--ro-bind $pm-conf $temp-dir/'pacman.conf' ^
			--ro-bind $sync-path $temp-dir/'pacman-db/sync' ^
			(
				if $bind-local {
					$container[parents] | each {|par|
						var dbpath = ($par[path])/'files/root/var/lib/pacman/local'
						if (os:exists $dbpath) {
							put --overlay-src $dbpath
						}
					}

					put ^
						--overlay-src $temp-dir/'pacman-db/empty-db' ^
						--ro-overlay $temp-dir/'pacman-db/local'
				} else {
					put --tmpfs $temp-dir/'pacman-db/local'
				}
			) ^
			-- $@args
	}

	fn get-needed-pkgs {|container pm-conf sync-path &skip-installed=$true|
		var pkgs = [(all)]

		if (u:not-empty $pkgs) {
			to-lines $pkgs | bind-container-db $container $pm-conf $sync-path &bind-local=$skip-installed ^
				pacman --config=$temp-dir/'pacman.conf' --dbpath=$temp-dir/'pacman-db' -Sp --needed --print-format='%r:%f' - | each {|line|
					#u:log { echo $line }
					if (not (str:has-prefix $line '::')) {
						var parts = [(str:split &max=2 ':' $line)]
						put [
							&repo= $parts[0]
							&file= $parts[1]
						]
					}
				}
		}
	}

	fn get-download-urls {|pm-conf arch|
		var mirrors = (u:parse-mirrors &pm-conf=$pm-conf &arch=$arch)
		#u:log { pprint $mirrors }

		each {|p|
			#u:log { pprint $p }
			put [(
				for m $mirrors[$p['repo']] {
					put (u:ensure-suffix $m '/')$p['file']
				}
			)]
		}
	}

	fn gen-dl-cmd {|downloader urls container-dest host-dest &suffix='' &can-skip=$false|
		for url $urls {
			var parts = (dl:parse-source $url$suffix)
			u:escape-quoted ($downloader[download-cmd-parsed-url] ^
				&exists=(os:exists $host-dest/$parts['filename']) ^
				&can-skip=$can-skip $parts $container-dest) ^
			| str:join ' '
		} | str:join ' || '
	}

	fn gen-pkg-dl-cmds {|container downloader pm-conf arch sync-path container-dest host-dest &skip-installed=$true|
		get-needed-pkgs &skip-installed=$skip-installed $container $pm-conf $sync-path ^
			| get-download-urls $pm-conf $arch ^
			| each {|urls|
				#u:log { pprint $urls }
				echo (gen-dl-cmd $downloader $urls $container-dest $host-dest)
				echo (gen-dl-cmd $downloader $urls $container-dest $host-dest &suffix='.sig' &can-skip=$true)
			}
	}

	fn install-parallel {|layer|
		u:scoped (u:make (dl:ctor-downloader $c:conf['download-agents'] '/pat-aur/tools')) {|downloader|
			u:scoped-lock &excl=$false $host-sync-path/'sync/.pataur.lock' {
				put 'parallel' | gen-pkg-dl-cmds ^
					$layer ^
					$downloader ^
					($layer[find] '/etc/pacman.conf') ^
					$arch-config['arch'] ^
					$host-sync-path/'sync' ^
					'/var/cache/pacman/pkg' ^
					$host-pkg-cache
			} | {
				u:log {
					$layer[exec] ^
						&read-only=$true ^
						&as-root=$false ^
						&use-seccomp-filters=$false ^
					bash -l
				}
			}

			u:scoped-lock &excl=$false $host-sync-path/'sync/.pataur.lock' {
				u:log {
					$layer[install] ^
							&needed=$true ^
							&noconfirm=$true ^
						'parallel'
				}
			}
		}
	}

	fn setup-download-container {|container fc|
		setup-container-layer $container &dir=(u:pat-aur-temp-dir)/'temp-containers' &opts=[(
			for type $dl:download-tools {
				put [&ro-bind= [(u:find-in-data-dirs 'pat-aur/scripts/download.'$type) '/pat-aur/tools/download.'$type]]
			}
		)] {|layer|
			$layer[lock] {|layer|
				install-parallel $layer
				$fc $layer
			}
		}
	}

	fn prepare {|container|
		var prepare-duration
		time &on-end={|x| set prepare-duration = $x } {

			var repos = [(
				put [&dbpath= $host-sync-path &config= $build-opts['host-pacman-conf']]
				if $use-target-repo {
					put [&dbpath= $target-sync-path &config= $build-opts['target-pacman-conf']]
				}
			)]

			u:log {
				for r $repos {
					os:mkdir-all $r['dbpath']/'sync'
					u:scoped-lock $r['dbpath']/'sync/.pataur.lock' {
						u:retry { e:pacsync --dbpath=$r['dbpath'] --config=$r['config'] }
					}
				}
			}

			if (and $c:conf['use-download-engine'] (u:not-empty $build-opts['host-depends'])) {
				u:echo-info 'Downloading dependencies...'

				setup-download-container $container {|dl-layer|
					u:scoped (u:make (dl:ctor-downloader $c:conf['download-agents'] '/pat-aur/tools')) {|downloader|
						u:scoped-lock &excl=$false (for r $repos { put $r['dbpath']/'sync/.pataur.lock' }) {
							all $build-opts['host-depends'] | gen-pkg-dl-cmds ^
								$dl-layer ^
								$downloader ^
								$build-opts['host-pacman-conf'] ^
								$arch-config['arch'] ^
								$host-sync-path/'sync' ^
								'/var/cache/pacman/pkg' ^
								$host-pkg-cache

							if $use-target-repo {
								{
									if (u:not-empty $build-opts['static-libs']) { all $build-opts['static-libs'] }
									if (u:not-empty $build-opts['target-depends']) { all $build-opts['target-depends'] }
								} | gen-pkg-dl-cmds ^
										&skip-installed=$false ^
									$dl-layer ^
									$downloader ^
									$build-opts['target-pacman-conf'] ^
									$target-arch ^
									$target-sync-path/'sync' ^
									'/var/cache/pacman/target-pkg' ^
									$target-pkg-cache
							}
						} | {
							u:log {
								$dl-layer[bash-login-exec] ^
									&read-only=$true ^
									&as-root=$false ^
									&use-seccomp-filters=$false ^
								(u:parallel-cmd-sem &max-jobs=$c:conf['parallel-downloads'] &id-sem='pat-aur-dl')
							}
						}
					}
				}
			}

			u:echo-info 'Installing dependencies...'

			u:log {
				var prepare-opts = [(
					if (not-eq $cc $nil) {
						u:with-key $cc 'import-pacman-keys' {|keys|
							if (u:not-empty $keys) {
								os:mkdir-all $temp-dir/'target-gpg'
								for k $keys {
									e:gpg --export -o $temp-dir/'target-gpg'/$k'.gpg' $k
								}
								put [&ro-bind= [$temp-dir/'target-gpg' '/pkgbuild/target-gpg']]
								put [&env-var= ['PATAUR_TARGET_KEYS' (str:join ' ' $keys)]]
							}
						}
					}

					put ^
						[&tmpfs= '/var/lib/pacman/sync'] ^
						[&ro-bind= [$build-opts['host-pacman-conf'] '/etc/pacman.conf']] ^
						[&ro-bind= [(u:find-in-data-dirs 'pat-aur/scripts/prepare') '/pkgbuild/prepare']] ^
						[&env-var= ['PATAUR_STATIC_LIBS' (str:join ' ' $build-opts['static-libs'])]] ^
						[&env-var= ['PATAUR_TARGET_DEPENDS' (print (all $build-opts['target-depends']))]] ^
						[&env-var= ['PATAUR_HOST_DEPENDS' (print (all $build-opts['host-depends']))]]
				)]

				$container[bash-login-exec] ^
						&work-dir='/pkgbuild/pkg' ^
						&as-root=$true ^
						&opts=$prepare-opts ^
						&use-seccomp-filters=$false ^
					'/pkgbuild/prepare'
			}

			if $c:conf['use-download-engine'] {
				setup-download-container $container {|dl-layer|
					u:echo-info 'Downloading sources...'

					u:scoped (u:make (dl:ctor-downloader $c:conf['download-agents'] '/pat-aur/tools')) {|downloader|
						{
							for s $sources {
								echo (u:escape-quoted (
									$downloader[download-cmd-parsed-url] $s '/pkgbuild/src' &exists=(os:exists $source-path/$s['filename'])
								))
							}

							for patch $patches {
								#u:echo-info 'Patch found: `'$patch['url']'`'
								echo (u:escape-quoted (
									$downloader[download-cmd-parsed-url] $patch['parts'] '/pkgbuild/src' ^
										&exists=(os:exists $source-path/$patch['parts']['filename'])
								))
							}
						} | {
							u:log {
								$dl-layer[bash-login-exec] ^
									&work-dir='/pkgbuild/pkg' ^
									&as-root=$false ^
									&read-only=$true ^
									&use-seccomp-filters=$false ^
								(u:parallel-cmd-sem &max-jobs=$c:conf['parallel-downloads'] &id-sem='pat-aur-dl')
							}
						}

						var failed = $false
						for patch $patches {
							if (!=s $patch['checksum'] 'SKIP') {
								var real-sum = (u:trim-right-of-first (e:sha256sum $source-path/$patch['parts']['filename'] | slurp) ' ')
								if (!=s $patch['checksum'] $real-sum) {
									set failed = $true
									u:echo-error 'Integrity check of patch `'$patch['parts']['filename']'` failed. (got '$real-sum')'
								}
							}
						}

						if $failed {
							fail 'Integrity check of patches failed.'
						}
					}
				}
			}
		}

		put $prepare-duration
	}

	fn build {|container|
		var build-duration
		var build-res
		time &on-end={|x| set build-duration = $x } {
			var build-container-opts = [(
				if $c:conf['use-download-engine'] {
					put [&tmp-overlay= [
						&src= [$source-path]
						&dest= '/pkgbuild/src'
					]]
				}

				if $build-opts['forced'] {
					put [&env-var= ['PATAUR_FORCE' 1]]
				}

				if (not-eq $jobs $nil) {
					put [&env-var= ['PATAUR_MAXJOBS' $jobs]]
				}

				if (has-env 'MAKEFLAGS') {
					put [&env-var= ['MAKEFLAGS' $E:MAKEFLAGS]]
					re:find &max=1 '--jobserver-auth=fifo:(\S*)' $E:MAKEFLAGS | each {|m|
						put [&bind= [$m['groups'][1]['text'] $m['groups'][1]['text']]]
					}
				}

				if $build-opts['check'] {
					put [&env-var= ['PATAUR_CHECK' '1']]
				}
				if $build-opts['clean'] {
					put [&env-var= ['PATAUR_CLEAN' '1']]
				}
				if $build-opts['clean-build'] {
					put [&env-var= ['PATAUR_CLEANBUILD' '1']]
				}

				u:map-loop $build-opts['flags'] {|k v|
					put [&env-var= [$c:build-flags[$k] (str:join ' ' $v)]]
				}

				if (not-eq $chost $nil) {
					put [&env-var= ['CHOST' $chost]]
				}

				var lib-path = '/usr/pat-aur-static/usr/lib'

				put [&ro-bind= [$host-sync-path/'sync' '/var/lib/pacman/sync']]
				if (not-eq $cc $nil) {
					u:with-key $cc 'cmake-toolchain' {|tchain|
						put ^
							[&ro-bind= [(c:resolve-conf-path $tchain) '/pkgbuild/toolchain.cmake']] ^
							[&env-var= ['CMAKE_TOOLCHAIN_FILE' '/pkgbuild/toolchain.cmake']]
					}

					set lib-path = $lib-path':/usr'/$chost'/lib'

					fn replace-tool {|tool|
						if (os:exists $container-path/'files/root/usr/bin'/$tool) {
							os:rename $container-path/'files/root/usr/bin'/{$tool $chost'-'$tool}
							{
								echo '#!/usr/bin/env sh'
								echo $chost'-'$tool' "$@"'
							} > $container-path/'files/root/usr/bin'/$tool
							os:chmod 0o755 $container-path/'files/root/usr/bin'/$tool
						}
					}

					put 'clang' 'clang++' | each {|tool| replace-tool $tool }

					put $container-path/'files/root/usr/bin'/$chost'-'*[nomatch-ok] | each {|tool-path|
						var tool = (str:trim-prefix $tool-path $container-path/'files/root/usr/bin'/$chost'-')
						if (os:exists $container-path/'files/root/usr/bin'/$tool) {
							put [&ro-bind= [$container-path/'files/root/usr/bin'/$chost'-'$tool '/usr/bin'/$tool]]
						}
					}

					e:mkdir -p $container-path/'files/root/usr/pat-aur-target/usr'{'lib' 'include' 'bin'}

					var goarch = (map-go-arch $target-arch)

					put ^
						[&env-var= ['CC' $cc['tuple']'-gcc']] ^
						[&env-var= ['CXX' $cc['tuple']'-g++']] ^
						[&env-var= ['LD' $cc['tuple']'-ld']] ^
						[&env-var= ['GOARCH' $goarch]] ^
						[&env-var= ['GOOS' 'linux']] ^
						[&env-var= ['PKG_CONFIG_SYSROOT_DIR' '/usr'/$chost]] ^
						[&env-var= ['PKG_CONFIG_LIBDIR' '/usr'/$chost/'usr/lib/pkgconfig:/usr'/$chost/'usr/share/pkgconfig']] ^
						[&env-var= ['QEMU_LD_PREFIX' '/usr'/$chost]] ^
						[
							&ro-overlay= [
								&src= [$container-path/'files/root/usr'/$chost/'usr' $container-path/'files/root/usr/pat-aur-target/usr']
								&dest= '/usr'/$chost/'usr'
							]
						] ^
						[
							&ro-overlay= [
								&internal= $true
								&if-exists= $true
								&src= ['/usr/bin']
								&dest= '/usr'/$chost/'usr/bin'
							]
						]

					$container[parents] | each {|c|
						put ($c[path])'/files/root/usr'/$chost/'lib/ld-linux-'*[nomatch-ok] | each {|pt|
							put [&ro-bind= [$pt '/usr/lib'/(path:base $pt)]]
						}
					}

					if (os:exists $container-path/'files/root/usr/pat-aur-target/usr/bin/ldd') {
						put [&ro-bind= [$container-path/'files/root/usr/pat-aur-target/usr/bin/ldd' '/usr/bin/ldd']]
					}

					ct:conf-container-opts $cc

					u:with-key $cc 'import-gpg-keys' {|keys|
						for key $keys { pa:import-gpg-key $container $key }
					}

					u:with-key $cc 'seccomp-filters' {|filters|
						for scf $filters {
							put [&seccomp-filter= $scf]
						}
					}
				}

				put [&env-var= ['LIBRARY_PATH' $lib-path]]

				if (os:exists &follow-symlink=$true $c:config-dir/'build-hook') {
					put [&ro-bind= [$c:config-dir/'build-hook' '/pkgbuild/hook']]
				}

				fn source-is-local {|s| or (==s $s['type'] 'local') (==s $s['protocol'] 'local') }
				fn filter-out-local {|proj_in proj_out|
					u:filter {|x| not (source-is-local $x) } &proj_in=$proj_in &proj_out=$proj_out
				}

				all [
					[&ro-bind= [(u:find-in-data-dirs 'pat-aur/scripts/build') '/pkgbuild/build']]
					[&env-var= ['PKGBASE' $build-opts['srcinfo']['pkgbase']]]
					[&env-var= ['PKGVER' $build-opts['srcinfo']['pkgver'][0]]]
					[&env-var= ['PATAUR_IGNOREARCH' (u:select $build-opts['ignore-arch'] '1' '0')]]
					[&env-var= ['PATAUR_HOST_LOCAL_REPOS' (print (for r $host-repos-info { put $r['name'] }))]]
					[&env-var= ['PATAUR_TARGET_LOCAL_REPOS' (print (for r $target-repos-info { put $r['name'] }))]]
					[&env-var= ['PATAUR_TARGET_LOCAL_REPOS_ROOT' (for r $target-repos-info { put $r['root'] } | str:join ':')]]
					[&env-var= ['PATAUR_SKIP_GPG_CHECKS' (u:select $build-opts['skip-gpg-checks'] '1' '0')]]
					[&env-var= ['PATAUR_RUN_PREPARE_ON_BUILD' (u:select $build-opts['run-prepare-on-build'] '1' '0')]]

					[&env-var= ['PATAUR_PATCHES' (
						order $patches &key={|x| put $x['parts']['filename'] } | each {|x|
							put $x['parts']['filename']
							put $x['dir']
							u:select (source-is-local $x['parts']) '0' '1'
						} | str:join ':'
					)]]
					[&env-var= ['PATAUR_SOURCES' (
						all $sources | each (filter-out-local $u:identity~ {|x| put $x['filename'] }) | order | str:join ':'
					)]]

					[&env-var= ['SRCDEST' '/pkgbuild/src']]
					[&env-var= ['BUILDDIR' '/pkgbuild/out']]
					[&env-var= ['PKGDEST' '/pkgbuild/dest']]
					[&env-var= ['LOGDEST' '/pkgbuild/log']]

					[&env-var= ['CARCH' $target-arch]]
				]
			)]
			#u:log { pprint $build-container-opts }

			u:echo-info 'Building...'

			set build-res = ?(u:log {
				$container[bash-login-exec] ^
						&work-dir='/pkgbuild/pkg' ^
						&as-root=$false ^
						&read-only=$true ^
						&opts=$build-container-opts ^
					'/pkgbuild/build'
			})
		}

		put $build-duration
		put $build-res
	}

	setup-build-container $container {|container|
		var prepare-duration = (prepare $container)
		var build-duration build-res = (build $container)

		var report = [
			&pkgbase= $build-opts['srcinfo']['pkgbase']
			&target-id= $build-opts['target-id']
			&dir= $dir
			&build-duration= $build-duration
			&prepare-duration= $prepare-duration
		]

		fn default-package-install {|f &repo-path=$build-opts['repo'] &key=$c:conf['gpg-signature-key']|
			var repo-root = (path:dir $repo-path)

			if (not-eq $key $nil) {
				u:log-redirect-to-error { u:echo-info 'Signed '(path:base $f)' with key `'$key'`' }
				u:sign-package $f $key
			}
			u:move-to-repo $f $repo-path $repo-root &sign=(not-eq $key $nil) &key=$key
			u:log-redirect-to-error { u:echo-info 'Installed `'(path:base $f)'` to `'$repo-root'`' }
		}

		#u:log { pprint $build-res }
		if $build-res {
			if (eq $after $nil) {
				set after = {|@_|
					each {|pkg|
						default-package-install $pkg['filename']
					}
				}
			}

			u:log {
				e:find $container-path/'files/pkgbuild/dest' -mindepth 1 -type f -a '!' -name '*.sig' ^
					| each {|filename|
						put [
							&filename= $filename
							&info= (u:extract-pkginfo $filename)
						]
					} ^
					| $after $build-opts['repo'] $target-repo-root $c:conf['gpg-signature-key'] $default-package-install~
			} | u:discard

			set report['pkgs'] = [(keys $build-opts['srcinfo']['pkgnames'])]
			set report['status'] = 'built'
		} elif (and (u:has-keys-and-value $build-res ['reason' 'type'] 'external-cmd/exited') ^
								(== $build-res['reason']['exit-status'] 2)) {
			set report['status'] = 'skip'
			u:log-redirect-to-error { u:echo-info 'Skipped, already built.' }
			set build-res = $true
		} else {
			set report['status'] = 'fail'
		}

		if (not-eq $report-file $nil) {
			put $report | to-json > $report-file
		}

		if (not $build-res) {
			fail $build-res
		}
	}
}

fn sync-repos {|pacman-conf dest &arch=$nil|
	var repos = [(e:aur repo -c $pacman-conf --list-repo | from-lines)]

	#u:echo-info 'Syncing for '$pacman-conf
	#u:silence {
		os:mkdir-all $dest
		u:retry { e:pacsync --force --config=$pacman-conf --dbpath=$dest $@repos }
	#}
}

fn setup-build {|build-dir host-config target-config &master-ssh=$nil|
	var host-arch-config = (c:get-arch-config $host-config['arch'])
	var target-arch-config = (
		if (has-key $c:conf['arch'] $target-config['arch']) {
			c:get-arch-config $target-config['arch']
		} else { put $nil }
	)
	var build-params = [
		&host-pacman-conf= $build-dir/'pacman.host.conf'
		&target-pacman-conf= $build-dir/'pacman.target.conf'
		&target-id= $target-config['id']
		&host-id= $host-config['id']
		&build-dir= $build-dir
		&mounts= []
	]

	#defer {
		#if (not-eq $master-ssh $nil) {
			#u:retry { $master-ssh[unmount] $build-dir/'target-repo' }
		#}
	#}

	var target-repo-conf

	if (eq $master-ssh $nil) {
		set build-params['repo'] = $target-config['local-repo']['path']
		set build-params['remote'] = $false
		set build-params['makepkg-conf'] = $target-config['makepkg-conf']

		set target-repo-conf = (repo-config $target-config)
	} else {
		set build-params['repo'] = $build-dir/'target-repo'/(path:base $target-config['local-repo']['path'])
		set build-params['remote'] = $true
		set build-params['makepkg-conf'] = $build-dir/'makepkg.conf'
		set build-params['mounts'] = [$build-dir/'target-repo']

		os:mkdir-all $build-dir/'target-repo'
		$master-ssh[mount] $target-config['local-repo']['root'] $build-dir/'target-repo'
		$master-ssh[exec] cat $target-config['makepkg-conf'] > $build-params['makepkg-conf']

		set target-repo-conf = (
			{
				put [
					&name= $target-config['local-repo']['name']
					&server= 'file://'$build-dir'/target-repo'
					&sig-level= $target-config['local-repo']['sig-level']
				]

				if (u:has-key-and-not-nil $c:conf 'local-repos') {
					$c:conf['local-repos'] $target-config
				}
			} | u:to-pacman-repo-config | slurp
		)
	}

	var host-repo-conf = (repo-config $host-config)

	var pacman-conf-host-tpl = (cat $host-arch-config['pacman-conf'] | slurp)
	print $pacman-conf-host-tpl | u:substitute-in-pacman-conf '@local-repo@' $host-repo-conf | to-lines > $build-params['host-pacman-conf']

	if (not-eq $target-arch-config $nil) {
		var pacman-conf-target-tpl = (cat $target-arch-config['pacman-conf'] | slurp)
		print $pacman-conf-target-tpl | u:substitute-in-pacman-conf '@local-repo@' $target-repo-conf | to-lines > $build-params['target-pacman-conf']
	}

	u:silence {
		run-parallel (
			if (not-eq $target-arch-config $nil) {
				put { sync-repos $build-params['target-pacman-conf'] $build-params['build-dir']/'local-db-'$build-params['target-id'] }
			}
			put { sync-repos $build-params['host-pacman-conf'] $build-params['build-dir']/'local-db-'$build-params['host-id'] }
		)
	}

	put $build-params
}

fn setup-container-root {|fc &path=$nil &excl-lock=$false|
	set path = (coalesce $path $c:conf['container-root'])

	u:scoped (u:make (ct:ctor-container-root $path)) {|container-root|
		u:scoped (u:make (u:ctor-synchronized $path/'lock' $container-root)) {|container-root|
			$fc $container-root $path
		}
	}
}

fn ctor-builder-ninja {|build-dir|

	os:mkdir-all $c:conf['build-containers-dir']

	var makepkg-container-cmd = (u:find-in-data-dirs 'pat-aur/scripts/makepkg-container.elv')
	var init-container-cmd = (u:find-in-data-dirs 'pat-aur/scripts/init-root-container.elv')
	var job-server-cmd = (u:find-in-data-dirs 'pat-aur/scripts/job-server')
	var containers-dir = (os:temp-dir &dir=$c:conf['build-containers-dir'] 'roots-')

	put [
		&prepare= {|_ targets-desc container-paths-override build-params &pools=$nil &heavy-pools=$nil|

			os:mkdir-all $containers-dir
			os:mkdir-all $build-dir/'report'

			fn gen-target-name {|name target-id|
				u:escape-ninja-rule $name'_-_'$target-id
			}

			#{
				#var ninja-status = (u:escape-cmd (print (u:print-header) (styled '[%f/%t] ' bold green)))

				#echo 'build:'
				#echo '	env NINJA_STATUS="'$ninja-status'" ninja -k $(PATAUR_FAIL_COUNT) $(PATAUR_TARGETS)'
			#} > $build-dir/'Makefile'

			u:echo-to-file-context $build-dir/'build.ninja' {
				var requested-targets = []
				var skipped-message = (u:escape-cmd (u:log-context { u:echo-info 'Skipped, already built.' }))
				set pools = (coalesce $pools $c:conf['parallel-pools'])
				set heavy-pools = (coalesce $heavy-pools $c:conf['parallel-heavy-pools'])
				var container-paths = [&]
				var container-paths-by-cat = [&]

				echo 'pool build_pool'
				echo '  depth = '$pools
				echo
				echo 'pool heavy_build_pool'
				echo '  depth = '$heavy-pools
				echo

				var archs = (
					u:pairs $targets-desc | each {|exe|
						if (has-key $exe[1] 'arch-config') {
							put [$exe[1]['target-config']['arch'] $exe[1]['arch-config']]
						}
					} | make-map
				)
				#u:log { pprint $archs }

				u:map-loop $archs {|arch arch-config|
					var overridden = (has-key $container-paths-override $arch)
					var root-path = (
						if $overridden {
							put $container-paths-override[$arch]
						} else {
							put $arch-config['root']
						}
					)

					var dir = (os:temp-dir &dir=$containers-dir 'root-')
					set container-paths[$arch] = $dir

					echo 'rule init-root-container-rule-'(u:escape-ninja-rule $arch)
					echo '  pool = '(u:select (== $pools 1) 'console' 'build_pool')
					echo '  description = Preparing container for `'$arch'`.'
					printf "  command = %s -a %s -r %s -p %s\n" ^
						(u:escape-quoted ^
							$init-container-cmd ^
							$arch ^
							$root-path ^
							$dir
						)

					echo 'build init-root-container-'(u:escape-ninja-rule $arch)': init-root-container-rule-'(u:escape-ninja-rule $arch)
					echo

					set container-paths-by-cat[$arch] = [&]

					u:map-loop $arch-config['layers'] {|category layer|
						var dir = (os:temp-dir &dir=$containers-dir 'root-')
						set container-paths-by-cat[$arch][$category] = (
							if $overridden {
								put $container-paths-override[$arch]
							} else {
								put $dir
							}
						)
						var rule-name = (u:escape-ninja-rule $arch)'-'(u:escape-ninja-rule $category)

						if $overridden {
							echo 'build init-layer-container-'$rule-name': phony init-root-container-'(u:escape-ninja-rule $arch)
							echo
						} else {
							echo 'rule init-layer-container-rule-'$rule-name
							echo '  pool = '(u:select (== $pools 1) 'console' 'build_pool')
							echo '  description = Preparing layer for `'$arch'` and category `'$category'`.'
							printf "  command = pat-aur container --arch=%s bootstrap && pat-aur container --arch=%s layer %s && pat-aur container --root-path=%s install --needed --confirm=no %s && %s -a %s -r %s -p %s\n" ^
								(u:escape-quoted ^
									$arch ^
									$arch ^
									$layer['path'] ^
									$layer['path']
								) ^
								(print (all $layer['packages'])) ^
								(u:escape-quoted ^
									$init-container-cmd ^
									$arch ^
									$layer['path'] ^
									$dir
								)
							echo 'build init-layer-container-'$rule-name': init-layer-container-rule-'$rule-name
							echo
						}
					}
				}

				each {|p|

					fn echo-depends {|key prefix tc|
						u:with-key $p $key {|deps|
							put $prefix(
								all $deps | each (u:filter {|x| not (has-key $tc['assume-installed'] $x['name']) } ^
									&proj_out={|x| gen-target-name $x['name'] $x['target-id'] })
							)
						}
					}

					var rule-prefix = (if (has-key $p 'prefix') { put $p['prefix'] } else { put '' })

					if (==s $p['type'] 'pkgbase') {

						var tc = $targets-desc[$p['target-id']]['target-config']
						var bp = $build-params[[&target-id= $p['target-id'] &host-id= $p['host-id']]]
						var escaped-name = (gen-target-name $p['name'] $p['target-id'])
						var report-file = $build-dir'/report/'$p['name']'@'$p['target-id']
						var arch = $tc['arch']
						var use-cc = (and (==s $p['cc-method']['method'] 'cc') (!=s $p['cc-method']['host-arch'] $arch))
						var host-arch = $targets-desc[$p['host-id']]['target-config']['arch']
						var rule-name = $rule-prefix'build-'$escaped-name
						var container-init-target container-path = (
							u:with-key $p 'category' {|c|
								u:with-keys $container-paths-by-cat [$host-arch $c] {|path|
									put 'init-layer-container-'(u:escape-ninja-rule $host-arch)-(u:escape-ninja-rule $c)
									put $path
								} &or-else= {
									put 'init-root-container-'(u:escape-ninja-rule $host-arch)
									put $container-paths[$host-arch]
								}
							} &or-else= {
								put 'init-root-container-'(u:escape-ninja-rule $host-arch)
								put $container-paths[$host-arch]
							}
						)

						put [
							&pkgbase= $p['name']
							&dir= $p['dir']
							&target-id= $p['target-id']
							&status= 'not-run'
						] | to-json > $report-file

						echo 'rule '$rule-name
						echo '  pool = '(u:select (== $pools 1) 'console' (u:select $p['heavy'] 'heavy_build_pool' 'build_pool'))
						echo '  description = Building `'$p['name']'` for `'$p['target-id']'`.'
						if (u:has-key-and-value $p 'skip' $true) {
							var report = [
								&pkgbase= $p['name']
								&dir= $p['dir']
								&target-id= $p['target-id']
								&pkgs= [(keys $p['srcinfo']['pkgnames'])]
								&status= 'skip'
							]

							echo '  command = echo "'$skipped-message'" && echo '(u:escape-quoted (put $report | to-json))' >' ^
								(u:escape-quoted $report-file)
						} else {
							var fail-report = [
								&pkgbase= $p['name']
								&dir= $p['dir']
								&target-id= $p['target-id']
								&status= 'fail'
							]

							var options = ({
								put ^
									'target-id' 'host-id' 'skip-gpg-checks' 'check' 'ignore-arch' 'forced' 'env-vars' 'gpg-keys' ^
									'pacman-keys' 'srcinfo' 'patches' 'clean' 'clean-build' 'cc-method' 'run-prepare-on-build' | each {|k|
									put [$k $p[$k]]
								}

								put 'host-pacman-conf' 'target-pacman-conf' 'makepkg-conf' 'makepkg-conf' 'repo' | each {|k|
									put [$k $bp[$k]]
								}
							} | make-map)

							set options['flags'] = (u:map-loop $c:build-flags {|flag _|
								put [$flag $p[$flag'-flags']]
							} | make-map)

							set options['host-depends'] = [(
								for d $p['make-depends'] {
									put $d['name']
								}
								if $p['check'] {
									for d $p['check-depends'] {
										put $d['name']
									}
								}
							)]

							set options['target-depends'] = [(
								if $use-cc {
									for d $p['make-depends'] {
										put $d['name']
									}
									if $p['check'] {
										for d $p['check-depends'] {
											put $d['name']
										}
									}
								}
							)]

							set options['static-libs'] = [(
								if (not $use-cc) {
									for d $p['depends'] {
										if (u:has-key-and-value $d 'static' $true) { put $d['name'] }
									}
								}
							)]

							set options['target-arch'] = $arch
							set options['root-path'] = $container-path

							put $options | to-json > $build-dir/$rule-name'.json'

							printf "  command = echo %s > %s && %s %s --report=%s --build-opts-file=%s %s $in\n" ^
								(u:escape-quoted ^
									(put $fail-report | to-json) ^
									$report-file ^
								) ^
								(
									var cg-slice-key = (u:select $p['heavy'] 'cgroup-heavy-slice' 'cgroup-slice')
									if (not-eq $c:conf[$cg-slice-key] $nil) {
										print systemd-run --user --scope (u:select $c:conf['cgroup-slice-inherit'] '--slice-inherit' '') ^
											--slice=$c:conf[$cg-slice-key] -q -d --unit=pat-aur-$pid'-'$escaped-name --
									} else {
										print 'env'
									}
								) ^
								(u:escape-quoted ^
									$makepkg-container-cmd ^
									$report-file ^
									$build-dir/$rule-name'.json'
								) ^
								(u:select (== $pools 1) '' '-q')
						}
						echo

						echo 'build' $rule-prefix'pkgbase-md-'$escaped-name':' 'phony' (
							if (not $p['skip']) {
								echo-depends 'make-depends' $rule-prefix'pkg-b-' $tc
							}
						)
						echo

						echo 'build' $rule-prefix'pkgbase-d-'$escaped-name':' 'phony' (echo-depends 'depends' $rule-prefix'pkg-b-' $tc)
						echo

						echo 'build' $rule-prefix'pkgbase-cd-'$escaped-name':' 'phony' (
							if (and $p['check'] (not $p['skip'])) {
								echo-depends 'check-depends' $rule-prefix'pkg-b-' $tc
							}
						)
						echo

						echo 'build' $rule-prefix'pkgbase-build-'$escaped-name':' $rule-prefix'build-'$escaped-name $p['dir'] '||' ^
							$rule-prefix'pkgbase-md-'$escaped-name' '$rule-prefix'pkgbase-d-'$escaped-name ^
							(u:select $p['check'] $rule-prefix'pkgbase-cd-'$escaped-name '') ^
							(u:select (u:has-key-and-value $p 'skip' $true) '' $container-init-target)
						echo

					} elif (==s $p['type'] 'phony') {

						var escaped-name = (gen-target-name $p['name'] $p['target-id'])
						echo 'build' $rule-prefix'pkg-b-'$escaped-name':' 'phony'
						echo

					} else {

						var escaped-name = (gen-target-name $p['name'] $p['target-id'])

						echo 'build' $rule-prefix'pkg-pkgbase-'$escaped-name':' 'phony' ^
							$rule-prefix'pkgbase-build-'(gen-target-name $p['pkgbase'] $p['target-id'])
						echo

						echo 'build' $rule-prefix'pkg-rt-'$escaped-name':' 'phony' (
							u:with-key $p 'runtime-depends' &or-else={ put '' } {|deps|
								for d $deps {
									put $rule-prefix'pkg-pkgbase-'(gen-target-name $d['name'] $d['target-id'])
								}
							}
						)
						echo

						echo 'build' $rule-prefix'pkg-b-'$escaped-name':' 'phony '$rule-prefix'pkg-pkgbase-'$escaped-name ^
							$rule-prefix'pkg-rt-'$escaped-name ( ^
								echo-depends 'depends' $rule-prefix'pkg-b-' $targets-desc[$p['target-id']]['target-config']
							)
						echo

						if (u:not-empty $p['for']) {
							set requested-targets = (conj $requested-targets $rule-prefix'pkg-b-'$escaped-name)
						}

					}
				}

				#u:log { pprint $requested-targets }
				echo 'build requested: phony' $@requested-targets
				echo
				echo 'default requested'
			}

			#u:log { cat $build-dir/'build.ninja' }
		}

		&build= {|_
				&targets=['requested']
				&jobs=$nil|

			var nb-workers = (if (eq $jobs $nil) { e:nproc } else { put $jobs })
			var nb-sub-jobs = (/ $nb-workers $c:conf['parallel-pools'])

			tmp E:PATAUR_NB_SUBJOBS = (to-string $nb-sub-jobs)
			#tmp E:PATAUR_TARGETS = (print (for t $targets { u:escape-ninja-rule $t }))
			#tmp E:PATAUR_FAIL_COUNT = (u:select $c:conf['continue-on-failure'] '0' '1')
			tmp E:NINJA_STATUS = (u:escape-cmd (print (u:print-header) (u:maybe-styled '[%f/%t] ' bold green)))
			#tmp E:NINJA_JOBSERVER = 'fifo'

			var res = ?(
				u:scoped-temp-dir 'fifo' {|temp-dir|
					#u:log { e:nice -n $c:conf['nice-level'] make -j $jobs -C $build-dir build }
					u:log {
						e:nice -n $c:conf['nice-level'] ^
							$job-server-cmd (if (not-eq $jobs $nil) { put -j$jobs }) --fifo=$temp-dir/'fifo' ^
								ninja -C $build-dir (if (not-eq $jobs $nil) { put -j$jobs }) -k ^
									(u:select $c:conf['continue-on-failure'] '0' '1') ^
									(for t $targets { u:escape-ninja-rule $t })
					}
				}
			)

			ct:remove-container-files -rf --one-file-system $containers-dir

			if $c:conf['clear-pkg-cache-after-build'] {
				clear-pkg-cache
			}

			if (and (not-eq $c:conf['ninja-log'] $nil) (os:is-regular $build-dir/'.ninja_log')) {
				e:cp -af $build-dir/'.ninja_log' $c:conf['ninja-log']
			}

			cat $build-dir/'report/'*[nomatch-ok] | from-json

			if (not $res) {
				fail $res
			}
		}
	]
}

fn setup-builder {|build-dir fc|
	u:scoped (u:make (ctor-builder-ninja $build-dir)) $fc
}

fn build-report {
	u:section 'Build report' {
		var report = [(all)]

		fn echo-report {|r|
			fn print-status {
				if (==s $r['status'] 'skip') {
					u:maybe-styled '[S]' dim
				} elif (==s $r['status'] 'not-run') {
					u:maybe-styled '[-]' dim
				} elif (==s $r['status'] 'built') {
					u:maybe-styled '[Y]' bold green
				} else {
					u:maybe-styled '[N]' bold red
				}
			}

			fn print-time {
				u:with-key $r 'build-duration' {|dur|
					echo ' (build: '(u:format-time $dur)') (prepare: '(u:format-time $r['prepare-duration'])').'
				} &or-else= { put '' }
			}

			u:log {
				echo ^
					(print-status) ^
					(u:maybe-styled '`'$r['pkgbase']'` for `'$r['target-id']'`'(print-time) ^
					(u:select (has-value ['skip' 'not-run'] $r['status']) 'dim' 'bold'))
			}
		}

		#for r $report {
		#	if (==s $r['status'] 'not-run') {
		#		echo-report $r
		#	}
		#}

		for r $report {
			if (==s $r['status'] 'skip') {
				echo-report $r
			}
		}

		for r $report {
			if (==s $r['status'] 'built') {
				echo-report $r
			}
		}

		for r $report {
			if (==s $r['status'] 'fail') {
				echo-report $r
			}
		}
	}
}

fn check-pgp-keys {
	var invalid-keys = []

	if $c:conf['auto-import-pkgbuild-keys'] {
		peach {|p|
			if (and (==s $p['type'] 'pkgbase') (not $p['skip']) (not $p['skip-gpg-checks'])) {
				var key-files = [(put $p['dir']/'keys/pgp'/*[nomatch-ok])]
				if (u:not-empty $key-files) {
					u:echo-info 'Key file found in source package for `'$p['name']'`, they will be imported.'
					e:gpg --import $@key-files
				}

				u:with-key $p['srcinfo'] 'validpgpkeys' {|gpg-keys|
					for k $gpg-keys {
						u:silence {
							if (not ?(e:gpg --list-keys $k)) {
								u:echo-info 'Key '$k' not found in keyring, it will be imported from key server.'
								try {
									e:gpg --receive-keys $k
								} catch e {
									u:echo-warning 'Failed importing key `'$k'` from key server.'
								}
							}
						}
					}
				}
			}
			put $p
		}
	} else {
		all
	}
}

fn build {|build-cmds container-paths-override host-targets targets-desc
		&install=$false
		&try=$false
		&force=$false
		&jobs=$nil
		&pools=$nil
		&ignore-arch=$false
		&skip-deps=$false
		&check-force=$nil
		&skip-gpg-checks=$false
		&graph-hook=$nil
		&graph-review=$nil
		&skip-aur-fetch=$false
		&clean=$nil
		&clean-build=$nil
		&build-report-file=$nil|

	set check-force = (u:select (eq $check-force $nil) $c:conf['check-force'] $check-force)
	set ignore-arch = (u:select (eq $ignore-arch $nil) $c:conf['ignore-arch'] $ignore-arch)

	#var host-config = $targets-desc[$host-target-id]['target-config']

	fn prepare-host-container {|fc|
		var host-arch-config = (c:get-arch-config (u:host-arch))
		var root-path = (
			if (has-key $container-paths-override $host-arch-config['name']) {
				put $container-paths-override[$host-arch-config['name']]
			} else {
				put $host-arch-config['root']
			}
		)
		ct:load-container $root-path ^
				&bootstrap=$true &arch-config=$host-arch-config ^
			{|host-container _|
				$host-container[lock] &excl=$false {|host-container|
					$fc $host-container
				}
			}
	}

	var ninja-build-dir = (u:create-temp-dir 'ninja-')
	defer {
		if (u:env-is-false 'PATAUR_DEBUG') {
			e:rm -rf --one-file-system $ninja-build-dir
		}
	}

	fn parse-arch-pkgbase {|name|
		var matches = [(re:find '.+\((.+)\)' $name)]

		if (u:empty $matches) {
			put $name
		} else {
			put $matches[0]['groups'][1]['text']
		}
	}

	u:map-loop $targets-desc {|target target-desc|
		var arch-pkgbases = [(
			for p $build-cmds { if (and (==s $p['target'] $target) (==s $p['cmd'] 'a')) { parse-arch-pkgbase $p['name'] } } | order | compact
		)]

		var fetch-pkg-cmd = (external (u:find-in-data-dirs 'pat-aur/scripts/fetch-pkg'))

		if (u:not-empty $arch-pkgbases) {
			u:log {
				os:mkdir-all $c:conf['pkgbuild-dir']/'arch'/$target
				tmp pwd = $c:conf['pkgbuild-dir']/'arch'/$target

				all $arch-pkgbases | peach &num-workers=$c:conf['parallel-downloads'] {|p|
					u:retry { $fetch-pkg-cmd $c:conf['arch-pkg-clone-url'] $p . $pid $p'.~lock' }
				}
			}
		}
	}

	set build-cmds = [({
		for p $build-cmds {
			if (==s $p['cmd'] 'a') {
				set p['dir'] = $c:conf['pkgbuild-dir']/'arch/'$p['target']/(parse-arch-pkgbase $p['name'])
				set p['cmd'] = 'm'
			}

			put $p
		}
	})]

	set build-cmds = [(
		for p $build-cmds {
			if (==s $p['cmd'] 'm') {
				var matches = [(re:find '(.+)\((.+)\)' $p['name'])]
				if (u:not-empty $matches) {
					set p['name'] = $matches[0]['groups'][1]['text']
					set p['dir'] = $matches[0]['groups'][2]['text']
				}
			}

			put $p
		}
	)]

	#u:log { pprint $build-cmds }

	var build-params = [&]
	var build-params-cc = [&]
	var pkgs-done = [&]

	defer {
		u:map-loop $build-params {|_ bp|
			for m $bp['mounts'] {
				$targets-desc[$bp['target-id']]['master-ssh'][unmount] $m
			}

			if (u:env-is-false 'PATAUR_DEBUG') {
				e:rm -rf --one-file-system $bp['build-dir']
			}
		}
	}

	fn prepare-graph {|host-container pkgbuild-specs requested-pkgs|
		fn gather-dependencies {|target-config aur-loc &include-self=$true|
			g:query-aur-dependencies ^
				&include-check-depends=(not-eq $check-force $false) ^
				&global-providers-map=$target-config['providers-map'] ^
				&pkg-config-map=$target-config['pkg-config'] ^
				&aur-location=$aur-loc ^
				&aur-blacklist=$target-config['aur-blacklist'] ^
				&include-self=$include-self ^
			| g:init-build-graph $requested-pkgs $target-config['id']
		}

		fn setup-build-target {|t|
			var target-desc = $targets-desc[$t['target-id']]
			var target-config = $target-desc['target-config']
			var build-dir = (u:create-temp-dir 'build-'$t['target-id']'-')

			set build-params[$t] = (setup-build $build-dir $targets-desc[$t['host-id']]['target-config'] $target-config ^
				&master-ssh=$target-desc['master-ssh'])
		}

		fn init-build-params {
			each {|p|
				if (==s $p['type'] 'pkgbase') {
					var entry = [&host-id= $p['host-id'] &target-id= $p['target-id']]
					if (not (has-key $build-params $entry)) {
						setup-build-target $entry
					}
				}

				put $p
			}
		}

		fn fetch-read-and-map {
			var graph = [(
				each {|p|
					if (==s $p['type'] 'pkgbase') {
						set p = (g:config-package-by-host $p $targets-desc $host-targets)
					}

					put $p
				} ^
				| init-build-params
			)]

			all $graph ^
				| each {|p| set pkgs-done[[$p['target-id'] $p['name']]] = $nil; put $p } ^
				| g:check-forced &try=$try &force=$force ^
				| {
					if (and (not-eq $try 'all') (not-eq $force 'all')) {
						g:early-skip-check $build-params ^
							| g:remove-skipped
					} else {
						all
					}
				} ^
				| g:fetch-aur $targets-desc &skip-fetch=$skip-aur-fetch ^
				| g:read-srcinfo $targets-desc $host-container ^
				| g:remap-depends $targets-desc ^
				| g:remap-targets $targets-desc $host-targets ^
				| init-build-params
		}

		fn maybe-init-new-graph {
			var pkgs-to-query = [&]
			var has-new = $false

			fn check-pkg {|k|
				if (not (has-key $pkgs-done $k)) {
					set pkgs-done[$k] = $nil
					set pkgs-to-query[$k[0]] = (u:append-to-list (u:default-conf-value $pkgs-to-query $k[0] $nil) $k[1])
					set has-new = $true
				}
			}

			each {|p|
				if (==s $p['type'] 'pkgbase') {
					#if (!=s $p['host-id'] $p['target-id']) {
						check-pkg [$p['host-id'] $p['name']]
					#}

					if (==s $p['cc-method']['method'] 'cc') {
						check-pkg [$p['target-id'] $p['name']]
					}
				}

				#u:log { pprint $p; echo }

				put $p
			}

			if $has-new {
				keys $pkgs-to-query | each {|target|
					u:all-of $targets-desc[$target]['target-config']['aur'] | each {|aur-loc|
						all $pkgs-to-query[$target] | gather-dependencies $targets-desc[$target]['target-config'] $aur-loc &include-self=$false
					}
				} | g:remove-dups | fetch-read-and-map | maybe-init-new-graph
			}
		}

		u:map-loop $targets-desc {|target target-desc|
			u:all-of $target-desc['target-config']['aur'] | each {|aur-loc|
				put [&target= $target &target-desc= $target-desc &aur-loc= $aur-loc]
			}
		} | peach &num-workers=$c:conf['parallel-downloads'] {|td|
			var target-pkgbuild-specs = [(all $pkgbuild-specs | each (u:filter {|x| ==s $x['target-id'] $td['target'] }))]

			var aur-pkgs = [(
				all $build-cmds | each (u:filter &proj_out={|x| put $x['name'] } {|x| and (==s $x['cmd'] 'b') (==s $x['target'] $td['target']) })
			)]
			var aur-pkgs-set = (all $aur-pkgs | u:make-set)

			#u:log { echo $td['target']; pprint $aur-pkgs; echo }
			#u:log { pprint $target-pkgbuild-specs; echo }

			if (u:not-empty $target-pkgbuild-specs) {
				g:insert-pkg-specs $target-pkgbuild-specs $requested-pkgs $td['target'] ^
					&force=$force &try=$try
			}

			if (or (u:not-empty $aur-pkgs) (u:not-empty $target-pkgbuild-specs)) {
				if $skip-deps {
					if (u:not-empty $aur-pkgs) {
						all $aur-pkgs | g:aur-query-info &aur-location=$td['aur-loc'] | g:init-build-graph $requested-pkgs $td['target']
					}
				} else {
					run-parallel (
						if (u:not-empty $aur-pkgs) {
							put {
								all $aur-pkgs | gather-dependencies $td['target-desc']['target-config'] $td['aur-loc']
							}
						}

						if (u:not-empty $target-pkgbuild-specs) {
							put {
								for spec $target-pkgbuild-specs {
									all $spec['make-depends']
									all $spec['depends']
									all $spec['check-depends']
								} | gather-dependencies $td['target-desc']['target-config'] $td['aur-loc']
							}
						}
					) | g:remove-dups
				}
			}
		} | fetch-read-and-map | maybe-init-new-graph | g:merge-dups | g:remove-orphans
	}

	fn setup-graph {|host-container|
		g:configure-build-graph $host-container $targets-desc $build-params ^
			&skip-gpg-checks=$skip-gpg-checks ^
			&check=$c:conf['check'] ^
			&check-force=$check-force ^
			&ignore-arch=$ignore-arch ^
			&clean= $clean ^
			&clean-build= $clean-build ^
		| g:missing-deps-as-phonies ^
		| g:map-runtime-deps
		#| each {|p| u:log { pprint $p }; put $p }
	}

	#u:log { pprint $build-params }

	var graph-pkgs = [&]

	setup-builder $ninja-build-dir {|builder|
		prepare-host-container {|host-container|
			var graph = [(
				u:log { echo }
				u:section 'Preparing build graph...' {
					var pkgbuild-specs = [(
						all $build-cmds | peach {|p|
							if (==s $p['cmd'] 'm') {
								g:read-pkgbuild $p $targets-desc $host-container &ignore-arch=$ignore-arch &check=$check-force
							}
						}
					)]
					#u:log { pprint $pkgbuild-specs }

					var requested-pkgs = ({
						for spec $pkgbuild-specs {
							for pn $spec['requested-pkgs'] {
								put [[$spec['target-id'] $pn] (u:select-keys $spec 'try' 'forced' 'install')]
							}
						}

						for p $build-cmds {
							if (==s $p['cmd'] 'b') {
								put [[$p['target'] $p['name']] (u:select-keys $p 'try' 'forced' 'install')]
							}
						}
					} | make-map)
					#u:log { pprint $requested-pkgs }

					prepare-graph $host-container $pkgbuild-specs $requested-pkgs
				}
			)]

			if (u:empty $build-params) {
				fail 'Nothing to build.'
			}

			#u:log { pprint $graph }

			fn do-setup {
				all $graph | setup-graph $host-container ^
					| { if (not-eq $graph-hook $nil) { $graph-hook } else { all } } ^
					| each {|p|
						if (and (==s $p['type'] 'pkg') (or $install (u:not-empty $p['install']))) {
							set graph-pkgs[[&name= $p['name'] &target-id= $p['target-id']]] = $p
						}
						put $p
					}
					#| each {|p| u:log { pprint $p }; put $p }
			}

			fn do-execute {
				var except
				var report = [(
					u:log { echo }
					u:section 'Building packages...' {
						if (not $skip-gpg-checks) { check-pgp-keys } else { all } | ^
							$builder[prepare] $targets-desc $container-paths-override $build-params &pools=$pools

						set except = ?($builder[build] &jobs=$jobs)
					}
				)]

				if (not-eq $build-report-file $nil) {
					put $report | to-json > $build-report-file
				}

				u:log { echo }
				all $report | build-report

				g:list-installable $report $graph-pkgs (u:select $install 'for' 'install')

				if (not $except) {
					fail [&type= 'build' &list= [$except] &message= 'Some build commands failed.']
				}
			}

			if (not-eq $graph-review $nil) {
				set graph = [(do-setup)]
				$graph-review $graph { all $graph | do-execute }
			} else {
				do-setup | do-execute
			}
		}
	}
}
