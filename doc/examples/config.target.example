#!/usr/bin/env elvish

#######################################################################
# Example target configuration file
#
# This file must be executable and should output json.
#######################################################################

use pat-aur/utils u

# Parse flags. See `pat-aur` manual in section `Target configuration file`.
var flags = (u:parse-target-config-flags $args)

# Figure out if the host and the target are the same.
var machine-arch = (e:uname -m | one)
var machine-id = (cat '/etc/machine-id' | one)
var is-local = (and (eq $machine-id $flags['host-machine-id']) (eq $machine-arch $flags['arch']))

# Get C flags.
var c-flags = (
	if (and $is-local (==s $flags['arch'] 'x86_64')) {
		put '-march=native' '-mtune=native'
	} else {
		if (==s $flags['arch'] $host-arch) {
			u:detect-gcc-flags
		}
	}
)

put [
	#######################################################################
	# General options
	#######################################################################

	# [Optional] ID for this target.
	# Default: Content of `/etc/machine-id`.
	&id= 'my-machine'

	# [Optional] Architecture of this target.
	# Default: Detected from `uname` command.
	&arch= 'x86_64'

	# [Optional] What makepkg.conf file to use. A relative path will be resolved relative to `~/.config/pat-aur`. The file is located on the
	# target machine and will be transferred via SSH if remote.
	# Default: '<pat-aur data dir>/defaults/makepkg.conf' on the host.
	&makepkg-conf= 'makepkg.conf'

	# [Optional] Local repository of this target. Packages will, by default, be installed to this repo. If given a string, information for
	# the repo will be read from the `pacman.conf` of the target machine or repository. For a remote target machine, it will be mounted via
	# sshfs.
	# Default: 'local-repo'
	&local-repo= 'local-repo'
	# or
	&local-repo= [
		# Directory for the repo.
		&root= '/srv/repo/my-local-repo'

		# Name of the repo.
		&name= 'my-local-repo'

		# Signature levels.
		&sig-level= ['PackageOptional' 'DatabaseOptional']
	]

	# [Optional] Pacman command to use for this target.
	# Default: [pacman]
	&pacman-cmd= ['pacman-wrapper' --somearg]

	# [Optional] Sudo command.
	# Default: [sudo]
	&sudo-cmd= ['doas']

	# [Optional] Command to use to install packages.
	# Default: [pacman -S]
	&pacman-install-cmd= ['pacman-wrapper' --somearg]

	# [Optional] AUR location as defined in host config `&aur`. Can be a string or a list. a $nil value will correspond to the default AUR.
	# An entry placed before another entry will take precedence when finding packages.
	# Default: $nil, use the default AUR.
	&aur= 'my-aur'
	# or
	&aur= ['my-aur' $nil]

	# [Optional] Don't fetch packages in this list from the AUR. This can be useful if a package is in both official repositories and the AUR but you want to use the official one.
	&aur-blacklist= ['package1' 'package2']

	# [Optional] List of packages to ignore when checking for updates.
	&ignore-updates= ['broken-package']

	# [Optional] This list of packages will be removed from dependencies and assumed to be already built and available in repositories.
	# This is useful for some packages that have circular dependencies to break the loop.
	&assume-installed= ['weird-package']

	# [Optional] Packages from official Arch repositories to compile from source when checking for updates.
	&compile-from-source= ['obs-studio' 'blender']

	# [Optional] Packages to rebuild periodically when checking for updates.
	&periodic-update-check= [
		[
			# Frequency in days.
			&frequency= 30

			# List of packages
			&pkgs= ['oh-my-zsh-git']
		]
	]

	# [Optional] Packages producing static libraries should be added here. Unnecessary for packages using `options=(staticlibs)`. These
	# packages will be compiled for the target as well as the host even if declared as make dependencies.
	&static-libs= ['libgmp-static']


	#######################################################################
	# Compiler flags
	#
	# The following optional config keys applies to all packages.
	#######################################################################

	# [Optional] Array of compiler flags. It will populate the following variables: CFLAGS, CXXFLAGS, LDFLAGS, LTOFLAGS and RUSTFLAGS.
	&c-flags= $c-flags
	&cxx-flags= $c-flags
	&cpp-flags= []
	&rust-flags= [(
		if $is-local {
			put '-Ctarget-cpu=native'
		} else {
			if (==s $flags['arch'] $host-arch) {
				u:detect-rust-flags
			}
		}
	)]
	&ld-flags= []
	&lto-flags= []

	# [Optional] Environment variables.
	&env-vars= [
		&SOME_VAR= 'value'
	]


	#######################################################################
	# Provider mapping
	#######################################################################

	# [Optional] Global map of providers to packages. Providers are not mapped automatically, so they have to be mapped here or in
	# `&pkg-config`/`&providers-map`.
	&providers-map= [
		&ninja= ['ninja-jobserver']
		&libpamac= ['libpamac-full']
		&libc++utilities.so= ['c++utilities']
		&libqtforkawesome.so= ['qtforkawesome']
		&libqtutilities.so= ['qtutilities']
		&libhttplib.so= ['cpp-httplib-compiled']
	]


	#######################################################################
	# Per package configuration
	#
	# Each key correspond to a package base (pkgbase).
	#######################################################################

	# [Optional] Each entry can contain all compiler flags keys from above. For each `*-flags`, a corresponding `*-flags-override` can be set
	# to override global flags. Otherwise, flags set here will be appended to the global flags.
	&pkg-config= [
		&some-package= [
			#&<compiler flags>=...

			# [Optional] Alter files in the source package. Note that it shouldn't alter dependencies or package naming. This string will be
			# executed by the host with `elvish -c` command. `u:serialize-for-eval` can be used to serialize an elvish function to a string.
			&alter= (u:serialize-for-eval {
				e:sed -i 's/_option=/_option=value/' 'PKGBUILD'
			})

			# [Optional] Extra make dependencies to use.
			&make-depends= ['llvm']

			# [Optional] Patches to apply on extracted sources before build. Patches will be downloaded and cached into the source cache
			# directory. Only work if `&use-download-engine` in host config is $true. For `&url` and `&dir`, the following will be substituted:
			# {pkgbase}: Package base.
			# {pkgver}: Package version.
			&patches= [
				[
					# URL of the patch. Format is the same as sources in a PKGBUILD.
					&url= '{pkgbase}-258.patch::https://invent.kde.org/plasma/{pkgbase}/-/merge_requests/258.patch'

					# sha256 checksum.
					&checksum= 'bf1c68ff889779ccac0e06cbd601e988d025e016c1ad1749d30df18fe8bc1b97'

					# Which directory to cd into before applying patch, relative to ${srcdir}.
					&dir= '{pkgbase}-{pkgver}'
				]
			]

			# [Optional] Map of providers to packages.
			&providers-map= [
				&libpamac= ['libpamac-full']
			]

			# [Optional] By default, prepare() and build() will be run in 2 separate commands. This can break some packages that expect variables
			# exported in prepare() to be stil available in build(). This option force running prepare() function again when it would have run
			# only build(). This option is incompatible with `&patches`.
			&run-prepare-on-build= $true
		]
	]

	# [Optional] This function can be used to configure packages programmatically. It is a string that will be passed to `eval` on the host.
	# `u:serialize-for-eval` can be used to serialize an elvish function to a string. Variable `p` will contain the current package
	# configuration. Only the following keys will be read: 'env-vars', 'check', 'ignore-arch', 'gpg-keys', 'pacman-keys', 'skip-gpg-checks',
	# 'patches', 'forced', 'category', all '*-flags' and '*-flags-override'. A category may be assigned to packages, see host config
	# `&arch/.../&layers`.
	&config-package= (u:serialize-for-eval {|p|
		use pat-aur/utils u

		fn has-make-depends {|name|
			for md $p['make-depends'] {
				if (==s $md['name'] $name) {
					put $true
					return
				}
			}

			put $false
		}

		var res = [&]

		if (has-make-depends 'clang') {
			# Remove unsupported flag
			set res['c-flags'] = [(all $p['c-flags'] | each (u:filter-out ['-mabm']))]
			set res['cxx-flags'] = [(all $p['cxx-flags'] | each (u:filter-out ['-mabm']))]
		}

		put $res
	})
] | to-json
