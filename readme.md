pat-aur
==============================

Highly configurable tool to build Arch Linux packages in clean containers. It make use of unprivileged containers through unshare and bubblewrap to build in a clean container without requiring root access. It isn't meant to be a pacman wrapper and some things work differently from traditional makepkg command.

## Features

*  Build packages in unprivileged containers.
*  Per package build configuration, flags and patching.
*  Dependency resolution and fetching from the AUR or official arch packages.
*  Provider mapping for dependencies via configuration.
*  Build for different targets with different configuration and architecture.
*  Parallelized builds.
*  Parallel download of sources, dependencies and pkgbuild files across builds.
*  Target a repository or a machine (accessible by ssh).
*  Package signing.
*  Check, build and install updates.
*  Check for packages needing a rebuild on updates.
*  Select official arch packages to be built from source.
*  Check for update periodically on selected packages.
*  Manage containers for building packages.
*  Manage package repositories.
*  Review PKGBUILD files before build.
*  Cross-compiling support, with some limitations, read below.
*  seccomp filter support.

## Dependencies

*  From official arch repos: `pacman`, `parallel`, `arch-install-scripts`, `pacutils`, `socat`, `cmake`, `python`.
	*  Optional: `sshfs`.
*  From the AUR: `aurutils`, `dumb-init-git`, `bubblewrap-overlayfs`.
	*  Optional: `seccomp-filtered-run`.
	*  The latest elvish from git: `elvish-git`.
	*  `ninja-jobserver`: ninja that support GNU make job server. Needs to be mapped in your target config in `&providers-map` so it gets installed into build containers:

```elvish
&providers-map= [
	&ninja= ['ninja-jobserver']
]
```

## Setup

*  Install from the AUR `pat-aur-git`. You'll have to bootstrap it manually with makepkg or other tools, including its dependencies. Host machines need to have `pat-aur-host-git` and targets, only `pat-aur-client-git`. You can use `bootstrap` script to automate the process.
*  Sub-uid and Sub-gid will need to be added to your system `/etc/subuid` and `/etc/subgid`:
	*  In `/etc/subuid` add the following line: `<user>:100000:100000`. Replace <user> with the username or ID of the user that will be building packages.
	*  In `/etc/subgid` add the following line: `<user>:100000:100000`. Replace <user> same as above.
*  Target machines will need to have a local repository initialized and modifiable by the user building packages and added to their system's `pacman.conf`. You can initiate a repository with command `pat-aur repo init <path> <name>`. The host building packages is also a target, so that include the host.
*  By default, the filesystem inside the container will be mounted read-only during build. Some packages need to write to your home, so you will have to configure bindings for your container, see `config.host.example`.
*  `makepkg.conf` and `pacman.conf`: By default, it will use its internal defaults, see `config/makepkg.conf` and `config/pacman.conf.in`. Copy these in your `~/.config/pat-aur` and make changes there. What `makepkg.conf` file to use can be configured per target, see `config.target.example` `&makepkg-conf`. `pacman.conf` template can be configured per architectures on the host, see `config.host.example` `&arch`.
*  Providers needs to be mapped manually in target's config. Pat-aur does not try to map dependencies automatically. See `config.target.example` `&providers-map` and `&pkg-providers-map`.

## Host and targets

In pat-aur terms, the host is the machine compiling and building packages. A target is the recipient of packages. There is 2 types of targets: a repository or a machine. The host is also a target, as it needs to build and use packages to build other packages.

The host is configured by the elvish script in `~/.config/pat-aur/config.host`, sourced into pat-aur. See `config.host.example` for documentation.

A target is configured by a script which must output json. A machine target is configured by the file `~/.config/pat-aur/config.target`. This script will be executed on the target machine via SSH if it is a remote target. See `config.target.example` for documentation.

A repository target is configured in `config.host`, see `config.host.example` `&repos`.

## Known issues

*  While you can interrupt pat-aur with ctrl+c, it won't be able to perform cleanup. It is a known problem with the elvish language.
*  Stale locks: pat-aur uses file locks in multiple places, but if for some reason pat-aur can't unlock due to an interruption, you will have
to remove them by hand. Look in the following locations:
	*  Package cache dir (host config: `&pkg-cache-dir`, files ending in `.~lock`).
	*  Source cache dir (host config: `&source-dir`, files ending in `.~lock`).
	*  Pacman sync database dir (host config: `&pacman-sync-dir`, filename `.pataur.lock`).
	*  Container directory (filename `lock`).
	*  Local repository (filename `.pataur.lock`).
	*  GNU parallel semaphores (directory `~/.parallel/semaphores`).
*  Output may sometimes overlap since elvish lacks synchronization primitives.
*  Some PKGBUILD are simply broken (ex: linux-tkg) are will probably not work.
*  seccomp filters won't work if a build need to run a 32 bits program.

## Cross-compiling

It is possible to cross-compile packages to other architectures. 2 modes are supported: cross-compiling from a native tool chain and compiling through emulation. A mix of both is also possible. At the moment, bootstrapping a new container is only supported for x86-64. Other architectures will have to be done manually. There are definitely issues and not all packages will work. Don't forget to use the `-A` flag if you want to attempt to build a package not explicitly supported for the target architecture.

In cross-compile from a native tool chain mode, it is assumed that the host and target are in lockstep when it comes to version for their packages. Same goes for the native compiler. You can define cross-compilers in host config `&arch/<architecture>/&cross-compilers`.

In emulation mode, you will need to install `qemu-user-static-binfmt` on your host system to it can execute binaries inside the container. A native static tool chain can be bound inside a container with host config `&arch/<architecture>/&bind-toolchain`.

## Examples

Simply running `pat-aur` will build a package from current directory, similar to running `makepkg`.

Check and install updates on host machine:

```bash
pat-aur us:
```

Check and install updates on host machine and remote machine `my-other-machine`:

```bash
pat-aur us::.,my-other-machine
```

Build a package from the AUR for the host machine:

```bash
pat-aur b:czkawka-gui
```

Build and install packages from the AUR for remote machine `my-other-machine1` and `my-other-machine2`:

```bash
pat-aur bi:komikku,sirikali:my-other-machine1,my-other-machine2
```

Build from a PKGBUILD file in a directory:

```bash
pat-aur m:/directory/of/PKGBUILD
```

Create a container with all dependencies preinstalled for building a package for a target (assuming x86_64):

```bash
pat-aur container pkg 'my-other-machine' /tmp/java21-openjfx-container java21-openjfx
pat-aur --container-paths=x86_64:/tmp/java21-openjfx-container b:java21-openjfx:my-other-machine
```

Quickly bootstrap and run an application inside a container:

```bash
pat-aur bootstrap-run -g /tmp/firefox 'pipewire-pulse,firefox' firefox
```

Sometimes a package need to be rebuilt and installed as part of an update. This command will rebuild ffmpeg6.1 and installed it with the updates:

```bash
pat-aur us: bif:ffmpeg6.1
```

## Make commands

Commands must be issued to `pat-aur` to build packages. The general format is:

`pat-aur make <commands...>`

where commands are:

`[<command>:]<package/dir>,...[:<target>,...].`

`make` can be omitted if an argument contain a `:`, it will be interpreted as a command. Consult the manual page and the examples above
for details about each commands.

## Why build in clean containers?

1. Predictable build environment.

There are many problems when building packages outside of a clean container:

*  Make dependencies can sometimes conflict with system packages and such packages would have to be constantly uninstalled and reinstalled.
*  Versioning problems: You have package A that depends on package B of version 1. You want to build and install version 2 of B and then build version 2 of A. You can't install B unless you uninstall A first.
*  Finding the wrong library to link to or the wrong compiler or compiling tool.
*  Linking to extra libraries not intended by the author of the pkgbuild.

Overall, it is impossible to support every combination of configuration and packages installed when maintaining a package on the AUR. Building in a container ensure that the build environment is exactly as intended.

2. Security.

While it is not a completely fool proof solution, isolating the build environment in a container limits what a rogue pkgbuild could do. It does not replace manually reviewing build files, but it is not always easy to spot hidden nefarious code.

## Motivations

I find current tools to be lacking. I wanted to be able to build in clean containers without requiring root access. I wanted to be able to configure builds for specific packages all into a simple configuration file. I wanted the extra security of building in a container with restricted access to my system. I wanted to use my big workstation machine to build optimized packages for my other devices. I wanted to parallelize all aspects of the build system.
