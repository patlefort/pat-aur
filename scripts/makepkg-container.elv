#!/usr/bin/env elvish

use flag
use str
use re
use os

use pat-aur/build b
use pat-aur/utils u
use pat-aur/client
use pat-aur/config c
use pat-aur/container ct

var flag-specs = [
	[&long= 'help' &description= 'Show this help.']
	[&short= 'j' &long= 'jobs' &default= (u:env-or-default 'PATAUR_NB_SUBJOBS' $nil) &arg-required= $true &description= 'Maximum number of sub-jobs allowed.']
	[&long= 'report' &default= $nil &arg-required= $true &description= 'Report what packages were built in this file.']
	[&long= 'build-opts-file' &arg-required= $true &description= 'File containing build options in JSON format.']
	[&short= 'q' &long= 'quiet' &description= 'Silence build output.']
]
var flags commands = (u:parse-args $args $flag-specs)

if $flags['help'] {
	u:show-flags-help $flag-specs []
	exit
}

client:run &silent=$flags['quiet'] {
	if (u:empty $commands) {
		fail 'No package directory given.'
	}

	if (not (os:exists $flags['build-opts-file'])) {
		fail 'Option file `'$flags['build-opts-file']'` not found.'
	}

	var build-opts = (from-json < $flags['build-opts-file'])

	u:fail-if-nil $build-opts['root-path'] 'No root-path given.'

	#pprint $commands >&2
	#pprint $parsed-flags >&2
	#pprint $flags >&2
	#pprint $build-opts >&2

	var arch-config = (c:get-arch-config (cat $build-opts['root-path']/'arch' | one))
	#pprint $arch-config

	ct:load-container $build-opts['root-path'] ^
			&bootstrap=$false &arch-config=$arch-config &read-arch-config-opts=$false ^
		{|container path|
			$container[lock] &excl=$false {|container|
				if (not (has-key $build-opts['srcinfo'] 'pkgbase')) { fail 'Could not determine pkgbase of package.' }

				var container-config = $arch-config
				u:with-key $c:conf['pkg-config'] $build-opts['srcinfo']['pkgbase'] {|pkg-config|
					set container-config = (c:read-container-opts $container-config $pkg-config)
				}

				var after-hook
				if (not-eq $c:conf['hook-after'] $nil) {
					set after-hook = {|repo-path repo-root key default-install|
						if (not-eq $c:conf['hook-after'] $nil) {
							var args = $build-opts
							set args['repo-path'] = $repo-path
							set args['repo-root'] = $repo-root
							set args['key'] = $key
							set args['install'] = $default-install

							$c:conf['hook-after'] $args
						}
					}
				}

				b:setup-container-layer $container {|layer|
					$layer[lock] &excl=$true {|layer|
						b:build-in-container $commands[0] $layer $build-opts $arch-config $container-config ^
							&jobs=$flags['jobs'] ^
							&report-file=$flags['report'] ^
							&after=$after-hook
					}
				}
			}
		}
}
