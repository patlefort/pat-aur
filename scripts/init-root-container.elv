#!/usr/bin/env elvish

use flag
use path
use os
use str
use re

use pat-aur/build b
use pat-aur/utils u
use pat-aur/client
use pat-aur/config c
use pat-aur/container ct

var flag-specs = [
	[&long='help' &description= 'Show this help.']
	[&short='a' &long='arch' &arg-required=$true &default=$nil &description='Architecture.']
	[&short='r' &long='root' &arg-required=$true &default=$nil &description='Path to root container.']
	[&short='p' &long='path' &arg-required=$true &default=$nil &description='Path to layer.']
]
var flags _ = (u:parse-args $args $flag-specs)

fn show-help {
	u:show-flags-help $flag-specs []
}

if $flags['help'] {
	show-help
	exit
}

client:run {
	var arch-config = (c:get-arch-config $flags['arch'])

	ct:load-container $flags['root'] ^
			&bootstrap=$true &arch-config=$arch-config ^
		{|container path|
			$container[lock] &excl=$false {|container|
				os:mkdir-all &perm=0o755 $flags['path']

				u:scoped ($container[layer] $flags['path']) {|layer|
					u:scoped (u:make (u:ctor-synchronized ($layer['path'])/'lock' $layer)) {|layer|
						$layer[lock] {|layer|
							$layer[save]
							$layer[parents] &include-self=$false | each {|p|
								$p[update] &noconfirm=$true &allow-downgrades=$true
							}
							u:log { b:import-keys $layer $arch-config }
						}
					}
				}
			}
		}
}
